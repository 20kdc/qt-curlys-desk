#include "cdtshiftmaptool.h"
#include <QPainter>
#include <QLabel>
#include <QVBoxLayout>
#include "cdutils.h"

CdtShiftMapTool::CdtShiftMapTool(CdiMapEditor * map) : CdtMapToolBase(map), point2(false) {
	interior = new QLabel("Click the first point of the new map boundaries.");
	QVBoxLayout * vbox = new QVBoxLayout(this);
	vbox->addWidget(interior);
	setLayout(vbox);
}

QRect CdtShiftMapTool::tileBoundRect() {
	if (!point2) {
		return QRect(oldX, oldY, 1, 1);
	} else {
		QRect basis = QRect(oldX, oldY, 1, 1);
		basis = basis.united(QRect(start.x(), start.y(), 1, 1));
		return basis;
	}
}

QMenu * CdtShiftMapTool::mouseUpdate(bool active, int tx, int ty) {
	if (active && !oldActive) {
		if (!point2) {
			start = QPoint(tx, ty);
			point2 = true;
			interior->setText("Click the second point of the new map boundaries.\nThis will resize & shift the map accordingly to fit the new boundaries.");
		} else {
			QRect basis = QRect(tx, ty, 1, 1);
			basis = basis.united(QRect(start.x(), start.y(), 1, 1));
			// perform actual shift
			editor->mdl->shift(basis);
			// clean up
			editor->editTiles();
		}
	}
	return nullptr;
}
