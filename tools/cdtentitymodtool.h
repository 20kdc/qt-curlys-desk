#ifndef CDTENTITYMODTOOL_H
#define CDTENTITYMODTOOL_H

#include <QWidget>
#include <QCheckBox>
#include "cdtmaptoolbase.h"
#include "cdtentitytool.h"
#include "pieces/cdpflageditor.h"

namespace Ui {
class CdtEntityModTool;
}

class cdxEntityMove : public QAction {
	Q_OBJECT
public:
	cdxEntityMove(QObject * par, CdiMapEditor * parent, const QString & str, int idx, int x, int y);

	int targetID, targetX, targetY;
	CdiMapEditor * editor;

public slots:
	void wasTriggered(bool);
};

class cdxEntityDelete : public QAction {
	Q_OBJECT
public:
	cdxEntityDelete(QObject * par, CdiMapEditor * parent, const QString & str, int idx);

	int targetID;
	CdiMapEditor * editor;

public slots:
	void wasTriggered(bool);
};

class CdgEnumGlobalMdl;

class CdtEntityModTool : public CdtEntityTool {
	Q_OBJECT

public:
	explicit CdtEntityModTool(CdiMapEditor * parent, int idx);
	~CdtEntityModTool();

	QMenu * menuForTile(int tx, int ty) override;

	QRect tileBoundRect() override;

private slots:
	void onEdit();
	void flagDetails();
	void valChange(int);

private:
	int targetID;
	QMap<int, int> fromCBtoNPC;
	QMap<int, int> fromNPCtoCB;
	Ui::CdtEntityModTool *ui;
};

#endif // CDTENTITYMODTOOL_H
