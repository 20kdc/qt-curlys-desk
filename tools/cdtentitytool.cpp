#include <QMenu>
#include <QApplication>
#include "cdtentitytool.h"
#include "cdtentitymodtool.h"

CdtEntityTool::CdtEntityTool(CdiMapEditor * _e) : CdtMapToolBase(_e) {

}

CdtEntityTool::~CdtEntityTool() {
}

cdxEntitySelection::cdxEntitySelection(QObject * par, CdiMapEditor * parent, const QIcon & icon, const QString & str, int idx, int x, int y) :
	QAction(par), targetID(idx), targetX(x), targetY(y), editor(parent) {
	setIcon(icon);
	connect(this, SIGNAL(triggered(bool)), this, SLOT(wasTriggered(bool)));
	setText(str);
}

void cdxEntitySelection::wasTriggered(bool) {
	if (targetID == -1) {
		int newID = editor->mdl->state.entities.length();
		editor->stateTransaction([&]() {
			// Adding an entity.
			CdkEntity ce = CdkEntity();
			ce.x = targetX;
			ce.y = targetY;
			ce.chrId = 0;
			ce.flagId = 0;
			ce.eventId = 0;
			ce.bitField = 0;
			editor->mdl->state.entities.append(ce);
		});
		editor->setTool(new CdtEntityModTool(editor, newID));
	} else {
		editor->setTool(new CdtEntityModTool(editor, targetID));
	}
}

QMenu * CdtEntityTool::menuForTile(int tx, int ty) {
	// Done on release because otherwise the release will be inhibited and that breaks things.
	// Search for entities at the target position, and collate them into a modal menu.
	QMenu * modalMenu = new QMenu(nullptr);

	int idx = 0;
	for (const CdkEntity & entity : editor->mdl->state.entities) {
		if ((entity.x == tx) && (entity.y == ty)) {
			QIcon icon;
			CdkEntityDrawInfo inf = editor->mdl->getEntityDrawInfo(entity);
			if (inf.image != nullptr)
				icon = inf.image->copy(inf.bounds);
			modalMenu->addAction(new cdxEntitySelection(modalMenu, editor, icon, "Entity " + QString::number(idx), idx, entity.x, entity.y));
		}
		idx++;
	}
	modalMenu->addAction(new cdxEntitySelection(modalMenu, editor, QIcon(), "New Entity", -1, tx, ty));
	return modalMenu;
}

QMenu * CdtEntityTool::mouseUpdate(bool active, int tx, int ty) {
	if (oldActive && !active)
		return menuForTile(tx, ty);
	return nullptr;
}

QRect CdtEntityTool::tileBoundRect() {
	return QRect(oldX, oldY, 1, 1);
}
