#ifndef CDTSHIFTMAPTOOL_H
#define CDTSHIFTMAPTOOL_H

#include "cdtmaptoolbase.h"

class CdiMapEditor;
class QLabel;

class CdtShiftMapTool : public CdtMapToolBase {
	Q_OBJECT
public:
	CdtShiftMapTool(CdiMapEditor *);
	QMenu * mouseUpdate(bool active, int tx, int ty) override;
	QRect tileBoundRect() override;
	bool point2;
	QPoint start;
	QLabel * interior;
};

#endif // CDTSHIFTMAPTOOL_H
