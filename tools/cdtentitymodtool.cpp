#include "cdtentitytool.h"
#include "cdtentitymodtool.h"
#include "ui_cdtentitymodtool.h"
#include "uis/cdiflageditordialog.h"
#include "models/cdgenumglobalmdl.h"
#include <algorithm>

static CdkEntity * getEntity(CdiMapEditor * editor, int t) {
	if (t < 0)
		return nullptr;
	if (t >= editor->mdl->state.entities.length())
		return nullptr;
	return &(editor->mdl->state.entities[t]);
}

CdtEntityModTool::CdtEntityModTool(CdiMapEditor * parent, int idx) :
	CdtEntityTool(parent),
	targetID(idx),
	ui(new Ui::CdtEntityModTool) {
	ui->setupUi(this);

	const QMap<int, QString> & categoryEnum = parent->enums->enums["npcCategory"];
	QList<int> categoryKeys = categoryEnum.keys();
	std::sort(categoryKeys.begin(), categoryKeys.end());
	// ew ew ew! reorder assorted to last
	if (categoryKeys.length() > 0) {
		if (categoryKeys[0] == 0) {
			categoryKeys.removeFirst();
			categoryKeys.append(0);
		}
	}
	int cbIdx = 0;
	for (int i : categoryKeys) {
		ui->cbType->addItem(QString("~~ ") + categoryEnum[i] + QString(" ~~"));
		cbIdx++;
		int npcIdx = 0;
		for (const CdkNPCTypeInfo & inf : parent->mdl->allNPCTypes) {
			if ((inf.category == i) || ((i == 0) && !categoryEnum.contains(inf.category))) {
				ui->cbType->addItem(inf.icon, inf.name);
				fromCBtoNPC[cbIdx] = npcIdx;
				fromNPCtoCB[npcIdx] = cbIdx;
				cbIdx++;
			}
			npcIdx++;
		}
	}

	ui->gbEntity->setTitle("Entity - " + QString::number(idx));

	onEdit();
	connect(ui->cbType, SIGNAL(currentIndexChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbFlID, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbEvID, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->btnFlagDetails, SIGNAL(clicked()), this, SLOT(flagDetails()));

	connect(parent, SIGNAL(onEdit()), this, SLOT(onEdit()));
}

CdtEntityModTool::~CdtEntityModTool() {
	delete ui;
}

void CdtEntityModTool::onEdit() {
	CdkEntity * ce = getEntity(editor, targetID);
	if (ce) {
		if (fromNPCtoCB.contains(ce->chrId))
			ui->cbType->setCurrentIndex(fromNPCtoCB[ce->chrId]);
		ui->sbFlID->setValue(ce->flagId);
		ui->sbEvID->setValue(ce->eventId);
		ui->tbFlags->setText(QString("%1").arg(ce->bitField, 4, 16, QLatin1Char('0')));
	}
}

void CdtEntityModTool::flagDetails() {
	CdkEntity * ce = getEntity(editor, targetID);
	if (ce) {
		CdiFlagEditorDialog * flagDlg = new CdiFlagEditorDialog(this);
		flagDlg->setValue(ce->bitField);
		int status = flagDlg->exec();
		if (status == QDialog::Accepted) {
			editor->stateTransaction([&]() {
				CdkEntity * ce2 = getEntity(editor, targetID);
				if (ce2)
					ce2->bitField = flagDlg->value();
			});
		}
	}
}

void CdtEntityModTool::valChange(int) {
	CdkEntity * ce = getEntity(editor, targetID);
	if (ce) {
		if (fromCBtoNPC.contains(ui->cbType->currentIndex())) {
			int res = fromCBtoNPC[ui->cbType->currentIndex()];
			if (ce->chrId != res) {
				editor->stateTransaction([&]() {
					ce->chrId = res;
				});
			}
		} else {
			if (fromNPCtoCB.contains(ce->chrId))
				ui->cbType->setCurrentIndex(fromNPCtoCB[ce->chrId]);
		}

		if (ce->flagId != ui->sbFlID->value())
			editor->stateTransaction([&]() {
				ce->flagId = ui->sbFlID->value();
			});

		if (ce->eventId != ui->sbEvID->value())
			editor->stateTransaction([&]() {
				ce->eventId = ui->sbEvID->value();
			});
	}
}

QMenu * CdtEntityModTool::menuForTile(int tx, int ty) {
	QMenu * modalMenu = CdtEntityTool::menuForTile(tx, ty);
	modalMenu->addAction(new cdxEntityMove(modalMenu, editor, "Move Here", targetID, tx, ty));
	modalMenu->addAction(new cdxEntityDelete(modalMenu, editor, "Delete Entity", targetID));
	return modalMenu;
}

QRect CdtEntityModTool::tileBoundRect() {
	CdkEntity * ce = getEntity(editor, targetID);
	if (ce)
		return QRect(ce->x, ce->y, 1, 1);
	return QRect(0, 0, 0, 0);
}

cdxEntityMove::cdxEntityMove(QObject * par, CdiMapEditor * parent, const QString & str, int idx, int x, int y) :
	QAction(par), targetID(idx), targetX(x), targetY(y), editor(parent) {
	connect(this, SIGNAL(triggered(bool)), this, SLOT(wasTriggered(bool)));
	setText(str);
}

void cdxEntityMove::wasTriggered(bool) {
	CdkEntity * ce = getEntity(editor, targetID);
	if (ce) {
		editor->stateTransaction([&]() {
			ce->x = targetX;
			ce->y = targetY;
		});
	}
}

cdxEntityDelete::cdxEntityDelete(QObject * par, CdiMapEditor * parent, const QString & str, int idx) :
	QAction(par), targetID(idx), editor(parent) {
	connect(this, SIGNAL(triggered(bool)), this, SLOT(wasTriggered(bool)));
	setText(str);
}

void cdxEntityDelete::wasTriggered(bool) {
	if (editor->mdl->state.entities.length() > targetID)
		editor->stateTransaction([&]() {
			editor->mdl->state.entities.removeAt(targetID);
		});
	editor->setTool(new CdtEntityTool(editor));
}
