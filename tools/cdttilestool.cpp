#include "tools/cdttilestool.h"
#include "uis/cditileselectionmts.h"
#include "ui_cdttilestool.h"
#include <QVBoxLayout>
#include <QLabel>
#include <QClipboard>

#define CDT_SUBTOOL_DRAW 0
#define CDT_SUBTOOL_RECT 1
#define CDT_SUBTOOL_FILL 2
#define CDT_SUBTOOL_REPLACE 3
#define CDT_SUBTOOL_COPY 4
#define CDT_SUBTOOL_PASTE 5

CdtTilesTool::CdtTilesTool(CdiMapEditor * _z) : CdtMapToolBase(_z), subtool(CDT_SUBTOOL_DRAW), ui(new Ui::CdtTilesTool) {
	ui->setupUi(this);
	selection = new CdiTileSelectionMTS(this);
	selection->setWindowFlag(Qt::WindowType::Tool);
	selection->setAttribute(Qt::WA_ShowWithoutActivating);
	selection->setWindowTitle("Tiles");
	selection->setMdl(_z->mdl);
	connect(ui->btnShowTileset, SIGNAL(clicked()), this, SLOT(showTileset()));
	connect(ui->rbDraw, SIGNAL(toggled(bool)), this, SLOT(updateSubtool(bool)));
	connect(ui->rbRectangle, SIGNAL(toggled(bool)), this, SLOT(updateSubtool(bool)));
	connect(ui->rbFill, SIGNAL(toggled(bool)), this, SLOT(updateSubtool(bool)));
	connect(ui->rbReplace, SIGNAL(toggled(bool)), this, SLOT(updateSubtool(bool)));
	connect(ui->rbCopy, SIGNAL(toggled(bool)), this, SLOT(updateSubtool(bool)));
	connect(ui->rbPaste, SIGNAL(toggled(bool)), this, SLOT(updateSubtool(bool)));
}

CdtTilesTool::~CdtTilesTool() {
	delete selection;
	delete ui;
}

void CdtTilesTool::showTileset() {
	selection->show();
}

void CdtTilesTool::updateSubtool(bool) {
	if (ui->rbDraw->isChecked())
		subtool = CDT_SUBTOOL_DRAW;
	else if (ui->rbRectangle->isChecked())
		subtool = CDT_SUBTOOL_RECT;
	else if (ui->rbFill->isChecked())
		subtool = CDT_SUBTOOL_FILL;
	else if (ui->rbReplace->isChecked())
		subtool = CDT_SUBTOOL_REPLACE;
	else if (ui->rbCopy->isChecked())
		subtool = CDT_SUBTOOL_COPY;
	else if (ui->rbPaste->isChecked())
		subtool = CDT_SUBTOOL_PASTE;
}

static void cdttilestool_rect(CdiMapEditor * cde, CdiTileSelection * selection, int x, int y, int w, int h) {
	int sx = selection->sx;
	int sy = selection->sy;
	int sw = selection->sw;
	int sh = selection->sh;
	for (int i = 0; i < w; i++) {
		for (int j = 0; j < h; j++) {
			uint8_t target = (uint8_t) (((i % sw) + sx) + (((j % sh) + sy) * 16));
			int px = i + x;
			int py = j + y;
			if (cde->mdl->tileInBounds(px, py))
				cde->mdl->tile(px, py) = target;
		}
	}
}

class cdxClipboardFragment {
public:
	int w;
	int h;
	QByteArray data;
};

static cdxClipboardFragment cdttilestool_getFragment() {
	QByteArray qba = QByteArray::fromBase64(QGuiApplication::clipboard()->text().toLatin1());
	cdxClipboardFragment frag;
	if (qba.length() < 8)
		return frag;
	int w = cdutils_load_u16(qba.mid(4, 2));
	int h = cdutils_load_u16(qba.mid(6, 2));
	int calcLen = (w * h) + 8;
	if (qba.length() < calcLen)
		return frag;
	frag.w = w;
	frag.h = h;
	frag.data = qba.mid(8, w * h);
	return frag;
}

QMenu * CdtTilesTool::mouseUpdate(bool active, int tx, int ty) {
	if (active && !oldActive) {
		lockX = tx;
		lockY = ty;
	}
	QRect rect = QRect(tx, ty, 1, 1).united(QRect(lockX, lockY, 1, 1));
	if (subtool == CDT_SUBTOOL_DRAW) {
		if (active && !oldActive)
			editor->stateTransactionStart();
		if (active) {
			editor->stateTransactionStepStart();
			cdttilestool_rect(editor, selection, tx, ty, selection->sw, selection->sh);
			editor->stateTransactionStepEnd();
		}
		if (!active && oldActive)
			editor->stateTransactionEnd();
	} else if (subtool == CDT_SUBTOOL_RECT) {
		if (!active && oldActive) {
			editor->stateTransaction([&]() {
				cdttilestool_rect(editor, selection, rect.x(), rect.y(), rect.width(), rect.height());
			});
		}
	} else if (subtool == CDT_SUBTOOL_REPLACE) {
		if (active && !oldActive) {
			if (editor->mdl->tileInBounds(tx, ty)) {
				editor->stateTransaction([&]() {
					uint8_t tile = editor->mdl->tile(tx, ty);
					for (int i = 0; i < (int) editor->mdl->w(); i++) {
						for (int j = 0; j < (int) editor->mdl->h(); j++) {
							if (editor->mdl->tile(i, j) == tile) {
								int subX = selection->sx + cdutils_cellmod(i - tx, selection->sw);
								int subY = selection->sy + cdutils_cellmod(j - ty, selection->sh);
								uint8_t newTile = subX + (subY * 16);
								editor->mdl->tile(i, j) = newTile;
							}
						}
					}
				});
			}
		}
	} else if (subtool == CDT_SUBTOOL_COPY) {
		if (!active && oldActive) {
			QByteArray qba = QByteArray(8 + (rect.width() * rect.height()), 0);
			qba[0] = 'P';
			qba[1] = 'X';
			qba[2] = 'M';
			qba[3] = 0x10;
			qba[4] = rect.width() & 0xFF;
			qba[5] = (rect.width() >> 8) & 0xFF;
			qba[6] = rect.height() & 0xFF;
			qba[7] = (rect.height() >> 8) & 0xFF;
			int idx = 8;
			for (int j = 0; j < rect.height(); j++) {
				for (int i = 0; i < rect.width(); i++) {
					int px = rect.x() + i;
					int py = rect.y() + j;
					if (editor->mdl->tileInBounds(px, py)) {
						qba[idx++] = editor->mdl->tile(px, py);
					} else {
						qba[idx++] = 0;
					}
				}
			}
			QGuiApplication::clipboard()->setText(QString::fromUtf8(qba.toBase64()));
		}
	} else if (subtool == CDT_SUBTOOL_PASTE) {
		if (active && !oldActive) {
			editor->stateTransaction([&]() {
				cdxClipboardFragment frag = cdttilestool_getFragment();
				for (int i = 0; i < frag.w; i++) {
					for (int j = 0; j < frag.h; j++) {
						uint8_t tile = frag.data[i + (j * frag.w)];
						int x = oldX + i;
						int y = oldY + j;
						if (editor->mdl->tileInBounds(x, y))
							editor->mdl->tile(x, y) = tile;
					}
				}
			});
		}
	}
	return nullptr;
}

QRect CdtTilesTool::tileBoundRect() {
	if ((subtool == CDT_SUBTOOL_RECT) || (subtool == CDT_SUBTOOL_COPY)) {
		if (oldActive)
			return QRect(oldX, oldY, 1, 1).united(QRect(lockX, lockY, 1, 1));
	} else if (subtool == CDT_SUBTOOL_PASTE) {
		cdxClipboardFragment frag = cdttilestool_getFragment();
		return QRect(oldX, oldY, frag.w, frag.h);
	}
	return QRect(oldX, oldY, selection->sw, selection->sh);
}
