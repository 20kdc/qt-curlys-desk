#ifndef CDTTILESTOOL_H
#define CDTTILESTOOL_H

#include "tools/cdtmaptoolbase.h"
#include "uis/cditileselectionmts.h"
#include "cdutils.h"

namespace Ui {
class CdtTilesTool;
}

class CdtTilesTool : public CdtMapToolBase {
	Q_OBJECT
public:
	CdtTilesTool(CdiMapEditor * editor);
	~CdtTilesTool();

	QMenu * mouseUpdate(bool active, int tx, int ty) override;
	QRect tileBoundRect() override;

	CdiTileSelectionMTS * selection;
	int lockX, lockY;
	int subtool;

private:
	Ui::CdtTilesTool *ui;

private slots:
	void showTileset();
	void updateSubtool(bool);
};

#endif // CDTTILESTOOL_H
