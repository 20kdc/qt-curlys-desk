#include "cdtmaptoolbase.h"

CdtMapToolBase::CdtMapToolBase(CdiMapEditor * parent) : QWidget(parent), oldActive(false), oldX(0), oldY(0), editor(parent) {
}

CdtMapToolBase::~CdtMapToolBase() {

}

QMenu * CdtMapToolBase::mouseUpdate(bool, int, int) {
	return nullptr;
}

QRect CdtMapToolBase::tileBoundRect() {
	return QRect(0, 0, 0, 0);
}

void CdtMapToolBase::paintMap(QPainter &) {

}
