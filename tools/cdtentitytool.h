#ifndef CDTENTITYTOOL_H
#define CDTENTITYTOOL_H

#include "cdtmaptoolbase.h"
#include <QAction>
#include <QMenu>

class cdxEntitySelection : public QAction {
	Q_OBJECT
public:
	cdxEntitySelection(QObject * par, CdiMapEditor * parent, const QIcon & icon, const QString & str, int idx, int x, int y);

	int targetID, targetX, targetY;
	CdiMapEditor * editor;

public slots:
	void wasTriggered(bool);
};

class CdtEntityTool : public CdtMapToolBase {
	Q_OBJECT
public:
	CdtEntityTool(CdiMapEditor * editor);
	~CdtEntityTool();

	virtual QMenu * menuForTile(int tx, int ty);

	QMenu * mouseUpdate(bool active, int tx, int ty) override;
	QRect tileBoundRect() override;

};

#endif // CDTENTITYTOOL_H
