#ifndef CDTMAPTOOLBASE_H
#define CDTMAPTOOLBASE_H

#include <QWidget>
#include <QRect>

#include "uis/cdimapeditor.h"

class QMenu;

class CdtMapToolBase : public QWidget {
	Q_OBJECT
public:
	CdtMapToolBase(CdiMapEditor * parent);
	virtual ~CdtMapToolBase() override;

protected:
	virtual QMenu * mouseUpdate(bool active, int tx, int ty);

	bool oldActive;
	int oldX, oldY;

	CdiMapEditor * editor;

public:
	virtual QRect tileBoundRect();
	virtual void paintMap(QPainter &);

	inline QMenu * inMouseUpdate(bool active, int tx, int ty) {
		QMenu * menu = mouseUpdate(active, tx, ty);
		oldActive = active;
		oldX = tx;
		oldY = ty;
		return menu;
	}
};

#endif // CDTMAPTOOLBASE_H
