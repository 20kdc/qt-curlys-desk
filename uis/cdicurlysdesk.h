#ifndef CDICURLYSDESK_H
#define CDICURLYSDESK_H

#include <QWidget>

#include "models/cdkgamemdl.h"
#include "uis/cdimapeditor.h"
#include "pieces/cdptsceditor.h"
#include "cdexcept.h"

namespace Ui {
class CdiCurlysDesk;
}

class QVBoxLayout;
class CdkBaseVisibleMdl;
class QListWidgetItem;

class CdiCurlysDesk : public QWidget {
	Q_OBJECT

public:
	explicit CdiCurlysDesk(CdkGameMdl * model);
	~CdiCurlysDesk();
	void updateStageList();

	CdkGameMdl * gameMdl;
	inline int currentMapIndex() {
		return currentMapIdx;
	}

	virtual void closeEvent(QCloseEvent * ev) override;

	void save(CdExceptionTracker &);

public slots:
	void causeSave();
	void causeSaveTest();
	void causeAbout();
	void causeNPCTBLReport();
	void stageSelected(QListWidgetItem *);
	void tscSelected(QListWidgetItem *);
	void stageViewCheckboxToggled(bool);

	void editCurrentStageTbl();
	void causeImportMapInfo();
	void causeExportMapInfo();
	void deleteCurrentStage();
	void addNewStage();

	void cleanupMapEditor(int);

private:
	Ui::CdiCurlysDesk * ui;
	QList<int> stageMap;
	QList<CdkBaseVisibleMdl *> visibleMdls;
	int currentMapIdx;
	CdiMapEditor * currentMapEditor;
};

#endif // CDICURLYSDESK_H
