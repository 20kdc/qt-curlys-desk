#include <QFileDialog>
#include <QMessageBox>

#include "uis/cdimapeditor.h"
#include "ui_cdimapeditor.h"

#include "uis/cdicurlysdesk.h"
#include "uis/cditileselection.h"
#include "models/cdkgamemdl.h"
#include "models/cdkbasemapmdl.h"

#include "tools/cdtmaptoolbase.h"
#include "tools/cdttilestool.h"
#include "tools/cdtentitytool.h"
#include "tools/cdtentitymodtool.h"
#include "tools/cdtshiftmaptool.h"

#include "pieces/cdptsceditor.h"
#include "pieces/cdpcollisioneditor.h"

#include "models/language/cdkbaselanguagemdl.h"

#include "cdifindiddialog.h"

CdiMapEditor::CdiMapEditor(CdiCurlysDesk * parent, CdgEnumGlobalMdl * enu) :
	QDialog(parent),
	enums(enu),
	ui(new Ui::CdiMapEditor),
	inTransaction(false) {

	int midx = parent->currentMapIndex();
	mdl = parent->gameMdl->openMap(midx);
	CdkBaseLanguageMdl * langMdl = CdkBaseLanguageMdl::attemptAcquireByName(parent->gameMdl, "tsc");
	tscEditor = new CdpTSCEditor(this, parent->gameMdl->gameCodec, langMdl, mdl->boundMdl->getPath(mdl->boundMdl->mtscPre + parent->gameMdl->stages[midx].mapId + mdl->boundMdl->mtscPost));
	currentTool = new CdtTilesTool(this);
	ui->setupUi(this);
	ui->toolHolder->layout()->addWidget(currentTool);

	connect(ui->actionHeader, SIGNAL(triggered()), parent, SLOT(editCurrentStageTbl()));
	connect(ui->actionShift_Map, SIGNAL(triggered()), this, SLOT(editShiftMap()));
	connect(ui->actionExport_Map_Info, SIGNAL(triggered()), parent, SLOT(causeExportMapInfo()));
	connect(ui->actionImport_Map_Info, SIGNAL(triggered()), parent, SLOT(causeImportMapInfo()));
	connect(ui->actionEditScript, SIGNAL(triggered()), this, SLOT(editTSC()));
	connect(ui->actionEditTilesetCollision, SIGNAL(triggered()), this, SLOT(editCEd()));
	connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(causeSave()));
	connect(ui->actionFindEv, SIGNAL(triggered()), this, SLOT(causeFindEv()));
	connect(ui->actionFindId, SIGNAL(triggered()), this, SLOT(causeFindId()));

	connect(ui->actionUndo, SIGNAL(triggered()), this, SLOT(causeUndo()));
	connect(ui->actionRedo, SIGNAL(triggered()), this, SLOT(causeRedo()));

	connect(ui->action1x, SIGNAL(triggered()), this, SLOT(setZoom1()));
	connect(ui->action2x, SIGNAL(triggered()), this, SLOT(setZoom2()));
	connect(ui->action3x, SIGNAL(triggered()), this, SLOT(setZoom3()));
	connect(ui->action4x, SIGNAL(triggered()), this, SLOT(setZoom4()));

	connect(ui->rbTiles, SIGNAL(clicked()), this, SLOT(editTiles()));
	connect(ui->rbEntities, SIGNAL(clicked()), this, SLOT(editEntities()));

	connect(ui->cbEntities, SIGNAL(toggled(bool)), this, SLOT(updateViewOptions(bool)));
	connect(ui->cbTileGrid, SIGNAL(toggled(bool)), this, SLOT(updateViewOptions(bool)));
	connect(ui->cbBackTiles, SIGNAL(toggled(bool)), this, SLOT(updateViewOptions(bool)));
	connect(ui->cbTileTypes, SIGNAL(toggled(bool)), this, SLOT(updateViewOptions(bool)));
	connect(ui->cbBackground, SIGNAL(toggled(bool)), this, SLOT(updateViewOptions(bool)));
	connect(ui->cbEntityText, SIGNAL(toggled(bool)), this, SLOT(updateViewOptions(bool)));
	connect(ui->cbFrontTiles, SIGNAL(toggled(bool)), this, SLOT(updateViewOptions(bool)));
	connect(ui->cbEntitySprites, SIGNAL(toggled(bool)), this, SLOT(updateViewOptions(bool)));
	ui->mapArea->zoom = parent->gameMdl->scale;
	updateViewOptions(false);
}

void CdiMapEditor::save(CdExceptionTracker & ex) {
	mdl->save(ex);
	tscEditor->save(ex);
}

void CdiMapEditor::causeSave() {
	CdExceptionTracker ex("map save");
	save(ex);
	mdl->boundMdl->terminateTracker(ex, this);
}

// undo/redo-critical {
void CdiMapEditor::stateTransaction(std::function<void()> transaction) {
	stateTransactionStart();
	stateTransactionStepStart();
	transaction();
	stateTransactionStepEnd();
	stateTransactionEnd();
}

void CdiMapEditor::stateTransactionStart() {
	// forces reset since this is a clear transaction start
	inTransaction = false;
}

void CdiMapEditor::stateTransactionStepStart() {
	if (!inTransaction) {
		inTransaction = true;
		redoList.clear();
		undoList.append(mdl->state);
		ui->actionUndo->setEnabled(true);
		ui->actionRedo->setEnabled(false);
	}
}

void CdiMapEditor::stateTransactionStepEnd() {
	emit onEdit();
}

void CdiMapEditor::stateTransactionEnd() {
	// forces automatic transaction start in case steps get applied outside of a transaction
	inTransaction = false;
}

void CdiMapEditor::causeUndo() {
	if (undoList.length() == 0)
		return;
	// causes future actions to start a new transaction to prevent fucking it all up
	inTransaction = false;
	//
	redoList.append(mdl->state);
	mdl->state = undoList.last();
	undoList.removeLast();
	ui->actionUndo->setEnabled(undoList.length() > 0);
	ui->actionRedo->setEnabled(true);
	emit onEdit();
}

void CdiMapEditor::causeRedo() {
	if (redoList.length() == 0)
		return;
	// causes future actions to start a new transaction to prevent fucking it all up
	inTransaction = false;
	//
	undoList.append(mdl->state);
	mdl->state = redoList.last();
	redoList.removeLast();
	ui->actionUndo->setEnabled(true);
	ui->actionRedo->setEnabled(redoList.length() > 0);
	emit onEdit();
}
// }

void CdiMapEditor::causeFindId() {
	CdiFindIdDialog * findIdDialog = new CdiFindIdDialog(this, "Internal ID");
	int val;
	if (findIdDialog->parseResult(findIdDialog->exec(), &val)) {
		int idx = 0;
		bool success = false;
		for (const CdkEntity & ent : mdl->state.entities) {
			if (idx == val) {
				setTool(new CdtEntityModTool(this, idx));
				ui->mapArea->coc(ent.x, ent.y);
				success = true;
				break;
			}
			idx++;
		}
		if (!success) {
			QMessageBox::information(this, "Failed", "Couldn't find entity with that internal ID.");
		}
	}
	delete findIdDialog;
}

void CdiMapEditor::causeFindEv() {
	CdiFindIdDialog * findIdDialog = new CdiFindIdDialog(this, "Event ID");
	int val;
	if (findIdDialog->parseResult(findIdDialog->exec(), &val)) {
		int idx = 0;
		bool success = false;
		for (const CdkEntity & ent : mdl->state.entities) {
			if (ent.eventId == val) {
				setTool(new CdtEntityModTool(this, idx));
				ui->mapArea->coc(ent.x, ent.y);
				success = true;
				break;
			}
			idx++;
		}
		if (!success) {
			QMessageBox::information(this, "Failed", "Couldn't find entity with that event ID.");
		}
	}
	delete findIdDialog;
}

void CdiMapEditor::setZoom1() {
	ui->mapArea->zoom = 1;
	updateViewOptions(false);
}

void CdiMapEditor::setZoom2() {
	ui->mapArea->zoom = 2;
	updateViewOptions(false);
}

void CdiMapEditor::setZoom3() {
	ui->mapArea->zoom = 3;
	updateViewOptions(false);
}

void CdiMapEditor::setZoom4() {
	ui->mapArea->zoom = 4;
	updateViewOptions(false);
}

void CdiMapEditor::updateViewOptions(bool) {
	ui->action1x->setChecked(ui->mapArea->zoom == 1);
	ui->action2x->setChecked(ui->mapArea->zoom == 2);
	ui->action3x->setChecked(ui->mapArea->zoom == 3);
	ui->action4x->setChecked(ui->mapArea->zoom == 4);
	ui->mapArea->viewEntities = ui->cbEntities->isChecked();
	ui->mapArea->viewTileGrid = ui->cbTileGrid->isChecked();
	ui->mapArea->viewBackTiles = ui->cbBackTiles->isChecked();
	ui->mapArea->viewTileTypes = ui->cbTileTypes->isChecked();
	ui->mapArea->viewBackground = ui->cbBackground->isChecked();
	ui->mapArea->viewEntityInd = ui->cbEntityText->isChecked();
	ui->mapArea->viewFrontTiles = ui->cbFrontTiles->isChecked();
	ui->mapArea->viewEntitySprites = ui->cbEntitySprites->isChecked();
	ui->mapArea->repaint();
}

void CdiMapEditor::setStatusText(const QString & statusText) {
	ui->statusBar->showMessage(statusText);
}

void CdiMapEditor::setTool(CdtMapToolBase * tool) {
	if (currentTool != nullptr) {
		ui->toolHolder->layout()->removeWidget(currentTool);
		currentTool->deleteLater();
	}
	currentTool = tool;
	ui->toolHolder->layout()->addWidget(currentTool);
}

void CdiMapEditor::editTSC() {
	tscEditor->show();
}

void CdiMapEditor::editTiles() {
	setTool(new CdtTilesTool(this));
}

void CdiMapEditor::editEntities() {
	setTool(new CdtEntityTool(this));
}

void CdiMapEditor::editShiftMap() {
	setTool(new CdtShiftMapTool(this));
}

void CdiMapEditor::editCEd() {
	CdpCollisionEditor * cce = new CdpCollisionEditor(this, mdl);
	cce->exec();
	delete cce;
}

CdiMapEditor::~CdiMapEditor() {
	// currentTool *should* be bound to this.
	delete ui;
	delete mdl;
}
