#ifndef CDIABOUT_H
#define CDIABOUT_H

#include <QDialog>

namespace Ui {
class CdiAbout;
}

class CdiAbout : public QDialog {
	Q_OBJECT

public:
	explicit CdiAbout(QWidget *parent = nullptr);
	~CdiAbout();

private:
	Ui::CdiAbout *ui;
};

#endif // CDIABOUT_H
