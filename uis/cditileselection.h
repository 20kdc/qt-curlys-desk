#ifndef CDITILESELECTION_H
#define CDITILESELECTION_H

#include <QWidget>
#include <QPaintEvent>

class CdkBaseMapMdl;

class CdiTileSelection : public QWidget {
	Q_OBJECT
public:
	explicit CdiTileSelection(QWidget * parent);

	void setTileSize(int i, int scaled);

	virtual QPixmap * openTS() = 0;
	virtual void closeTS(QPixmap * ts) = 0;

	void paintEvent(QPaintEvent * qpd) override;
	void mousePressEvent(QMouseEvent * ev) override;
	void mouseMoveEvent(QMouseEvent * ev) override;
	void mouseReleaseEvent(QMouseEvent * ev) override;

	inline void fixSize() { setTileSize(tileSize, tileSizeScaled); }

	inline int index() {
		return sx + (sy * 16);
	}
	inline void setIndex(int i) {
		sx = i & 0xF;
		sy = i >> 4;
		sw = sh = 1;
		repaint();
	}

	int tileSize = 16;
	int tileSizeScaled = 16;
	int sx = 0, sy = 0, sw = 1, sh = 1;
	bool dragging = false;
	bool canWide = true;

signals:
	void changedVal();

public slots:
};

#endif // CDITILESELECTION_H
