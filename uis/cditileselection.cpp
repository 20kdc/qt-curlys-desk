#include <QPainter>
#include "cdutils.h"

#include "uis/cditileselection.h"
#include "uis/cdimapeditor.h"

CdiTileSelection::CdiTileSelection(QWidget * parent) : QWidget(parent) {

}

void CdiTileSelection::setTileSize(int ts, int tss) {
	tileSize = ts;
	tileSizeScaled = tss;
	setMinimumSize(tileSizeScaled * 16, tileSizeScaled * 16);
	setMaximumSize(tileSizeScaled * 16, tileSizeScaled * 16);
}

void CdiTileSelection::paintEvent(QPaintEvent * qpe) {
	QPainter painter(this);
	QSize sz = size();
	painter.fillRect(0, 0, sz.width(), sz.height(), CDUTILS_BACKGROUND_COLOUR);
	QPixmap * qi = openTS();
	if (qi) {
		painter.drawPixmap(0, 0, (qi->width() * tileSizeScaled) / tileSize, (qi->height() * tileSizeScaled) / tileSize, *qi);
		closeTS(qi);
	}
	painter.setPen(CDUTILS_SELECTION_COLOUR);
	painter.drawRect(sx * tileSizeScaled, sy * tileSizeScaled, (sw * tileSizeScaled) - 1, (sh * tileSizeScaled) - 1);
	qpe->accept();
}

#define CDI_TS_CALCER \
	int xd = cdutils_celldiv(event->x(), tileSizeScaled); \
	int yd = cdutils_celldiv(event->y(), tileSizeScaled); \
	if ((xd < 0) || (xd >= 16)) \
		return; \
	if ((yd < 0) || (yd >= 16)) \
		return;


void CdiTileSelection::mousePressEvent(QMouseEvent * event) {
	if (event->button() != Qt::MouseButton::LeftButton)
		return;
	CDI_TS_CALCER
	sx = xd;
	sy = yd;
	sw = 1;
	sh = 1;
	dragging = true;
	changedVal();
	repaint();
}

void CdiTileSelection::mouseMoveEvent(QMouseEvent * event) {
	if (!canWide)
		return;
	if (dragging) {
		CDI_TS_CALCER
		sw = (xd - sx) + 1;
		sh = (yd - sy) + 1;
		if (sw < 1)
			sw = 1;
		if (sh < 1)
			sh = 1;
		int mxw = 16 - sx;
		int mxh = 16 - sy;
		if (sw > mxw)
			sw = mxw;
		if (sh > mxh)
			sh = mxh;
		changedVal();
		repaint();
	}
}

void CdiTileSelection::mouseReleaseEvent(QMouseEvent * event) {
	if (event->button() != Qt::MouseButton::LeftButton)
		return;
}
