#include <QMessageBox>
#include <QPushButton>
#include <QProcess>
#include <QFileDialog>
#include "uis/cdistbleditor.h"
#include "uis/cdicurlysdesk.h"
#include "ui_cdicurlysdesk.h"
#include "models/cdkbasevisiblemdl.h"
#include "models/cdgnpctblglobalmdl.h"
#include "cdcategory.h"
#include "cdicurlysdesk.h"
#include "cdiabout.h"
#include "models/cdgenumglobalmdl.h"

CdiCurlysDesk::CdiCurlysDesk(CdkGameMdl * mdl) :
	QWidget(nullptr),
	gameMdl(mdl),
	ui(new Ui::CdiCurlysDesk),
	currentMapIdx(-1),
	currentMapEditor(nullptr) {
	ui->setupUi(this);

	setWindowTitle("Curly's Desk");

	updateStageList();
	if (!mdl->stageModel->supportsMNA2())
		ui->cbStblName2->setDisabled(true);

	QList<int> sizes;
	sizes.push_back(200);
	sizes.push_back(200);
	ui->theOtherFrame->setSizes(sizes);

	connect(ui->cbStblName2, SIGNAL(toggled(bool)), this, SLOT(stageViewCheckboxToggled(bool)));
	connect(ui->cbStblUnused, SIGNAL(toggled(bool)), this, SLOT(stageViewCheckboxToggled(bool)));
	connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(causeSave()));
	connect(ui->actionSaveTest, SIGNAL(triggered()), this, SLOT(causeSaveTest()));
	connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(causeAbout()));
	connect(ui->actionNPCTBL, SIGNAL(triggered()), this, SLOT(causeNPCTBLReport()));
	connect(ui->lstStbl, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(stageSelected(QListWidgetItem *)));
	connect(ui->btnStblAdd, SIGNAL(clicked()), this, SLOT(addNewStage()));
	connect(ui->btnStblDel, SIGNAL(clicked()), this, SLOT(deleteCurrentStage()));
	connect(ui->lstTSC, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(tscSelected(QListWidgetItem *)));

	for (CdkBaseGlobalMdl * tsc : mdl->gModels) {
		CdkBaseVisibleMdl * cast = dynamic_cast<CdkBaseVisibleMdl *>(tsc);
		if (cast == nullptr)
			continue;
		ui->lstTSC->addItem(tsc->objectName());
		visibleMdls.append(cast);
	}
}

void CdiCurlysDesk::tscSelected(QListWidgetItem *) {
	visibleMdls[ui->lstTSC->currentRow()]->show();
}

void CdiCurlysDesk::closeEvent(QCloseEvent * ev) {
	if (QMessageBox::question(this, "Confirm exit?", "Data will be saved regardless.") == QMessageBox::Yes) {
		causeSave();
		QWidget::closeEvent(ev);
	} else {
		ev->ignore();
	}
}

void CdiCurlysDesk::updateStageList() {
	int oldMapIdx = currentMapIdx;
	currentMapIdx = -1;

	// Need to do this first, as the clear causes a stageSelected.
	// This conveniently cleans up the map editor.
	ui->lstStbl->setCurrentRow(-1);
	cleanupMapEditor(-1);

	ui->lstStbl->clear();
	stageMap.clear();

	int x = 0;
	for (const CdfStage & cs : gameMdl->stages) {
		if ((cs.mapId.length() != 0) || (ui->cbStblUnused->checkState() == Qt::CheckState::Checked)) {
			QString base = (ui->cbStblName2->checkState() == Qt::CheckState::Checked) ? cs.mnaSecondary : cs.mnaPrimary;
			ui->lstStbl->addItem(QString::number(x) + ": " + base + " (" + cs.mapId + ")");
			stageMap.append(x);
			if (x == oldMapIdx) {
				ui->lstStbl->setCurrentRow(ui->lstStbl->count() - 1);
				stageSelected(nullptr);
			}
		}
		x++;
	}
}

void CdiCurlysDesk::stageSelected(QListWidgetItem *) {
	cleanupMapEditor(0);
	int r = ui->lstStbl->currentRow();
	if (r == -1) {
		currentMapIdx = -1;
	} else {
		currentMapIdx = stageMap[r];
		CdExceptionTracker ex("Editing new stage");
		CdgEnumGlobalMdl * enums = gameMdl->findDependency<CdgEnumGlobalMdl>(ex, "CdiStblEditor");
		if (gameMdl->terminateTracker(ex, this))
			return;
		currentMapEditor = new CdiMapEditor(this, enums);
		connect(currentMapEditor, SIGNAL(finished(int)), this, SLOT(cleanupMapEditor(int)));
		currentMapEditor->show();
	}
}

void CdiCurlysDesk::cleanupMapEditor(int) {
	CdiMapEditor * cme = currentMapEditor;
	if (cme != nullptr) {
		CdExceptionTracker ex("Unable to save for cleanup.");
		cme->save(ex);
		gameMdl->terminateTracker(ex, this);
		// This is *critical* because this function can be called by a map editor that is cleaning itself up.
		cme->deleteLater();
		currentMapEditor = nullptr;
	}
}

void CdiCurlysDesk::deleteCurrentStage() {
	if (currentMapIdx == -1)
		return;
	// CONFIRM?
	if (currentMapIdx == gameMdl->stages.length() - 1) {
		gameMdl->stages.removeLast();
	} else {
		gameMdl->stages[currentMapIdx].mapId = "";
		gameMdl->stages[currentMapIdx].mnaPrimary = "";
	}
	updateStageList();
}

void CdiCurlysDesk::addNewStage() {
	int x = 0;
	for (const CdfStage & cs : gameMdl->stages) {
		if (cs.mapId.length() == 0)
			break;
		x++;
	}
	if (x == gameMdl->stages.length()) {
		gameMdl->stages.append(CdfStage());
	} else {
		gameMdl->stages[x] = CdfStage();
	}
	currentMapIdx = x;
	updateStageList();
	editCurrentStageTbl();
}

void CdiCurlysDesk::editCurrentStageTbl() {
	if (currentMapIdx != -1) {
		CdExceptionTracker ex("Editing current stage tbl");
		CdgEnumGlobalMdl * enums = gameMdl->findDependency<CdgEnumGlobalMdl>(ex, "CdiStblEditor");
		if (gameMdl->terminateTracker(ex, this))
			return;
		CdiStblEditor * subDialog = new CdiStblEditor(nullptr, enums, gameMdl->stageModel, gameMdl->stages[currentMapIdx]);
		if (subDialog->exec() == QDialog::Accepted) {
			// Stuff happened!
			gameMdl->stages[currentMapIdx] = subDialog->result;
			updateStageList();
		}
		delete subDialog;
	}
}

void CdiCurlysDesk::causeExportMapInfo() {
	if (currentMapIdx != -1) {
		QString filename = QFileDialog::getSaveFileName(this, "Export Map Info", gameMdl->getPath("."), "Map Information (*.cemap)");
		QFile file(filename);
		if (file.open(QFile::WriteOnly)) {
			CduBinaryW writer(&file, gameMdl->gameCodec);
			gameMdl->stages[currentMapIdx].write(writer, CDFSTAGE_FORMAT_CS_BASE);
		} else {
			QMessageBox::warning(this, "Error", "Unable to write file");
		}
	}
}

void CdiCurlysDesk::causeImportMapInfo() {
	if (currentMapIdx != -1) {
		QString filename = QFileDialog::getOpenFileName(this, "Import Map Info", gameMdl->getPath("."), "Map Information (*.cemap)");
		QFile file(filename);
		if (file.open(QFile::ReadOnly)) {
			CduBinaryR reader(&file, gameMdl->gameCodec);
			gameMdl->stages[currentMapIdx].read(reader, CDFSTAGE_FORMAT_CS_BASE);
			updateStageList();
		} else {
			QMessageBox::warning(this, "Error", "Unable to read file");
		}
	}
}

void CdiCurlysDesk::causeSave() {
	CdExceptionTracker tracker("save");
	save(tracker);
	if (tracker.check())
		QMessageBox::warning(this, "Error", tracker.errorText);
}

void CdiCurlysDesk::save(CdExceptionTracker & ex) {
	gameMdl->save(ex);
	if (currentMapEditor)
		currentMapEditor->save(ex);
}

void CdiCurlysDesk::causeSaveTest() {
	CdExceptionTracker ex("save & test");
	gameMdl->save(ex);
	if (currentMapEditor)
		currentMapEditor->save(ex);
	QStringList rest = gameMdl->gameRunArgs;
	if (rest.length() == 0)
		ex.report("No game run arguments");
	if (ex.check()) {
		QMessageBox::warning(this, "Error", ex.errorText);
	} else {
		QString first = rest[0];
		rest.removeFirst();
		QProcess::startDetached(first, rest, gameMdl->getPath("."));
	}
}

void CdiCurlysDesk::causeAbout() {
	CdiAbout * ca = new CdiAbout();
	ca->exec();
	delete ca;
}

void CdiCurlysDesk::causeNPCTBLReport() {
	QDialog * qd = new QDialog(this);
	qd->setWindowTitle("NPCTBL Report");
	QTextEdit * qte = new QTextEdit(qd);
	// compile
	CdExceptionTracker ex("causeNPCTBLReport");
	CdgNPCTBLGlobalMdl * npctblMdl = gameMdl->findDependency<CdgNPCTBLGlobalMdl>(ex, "causeNPCTBLReport");
	if (gameMdl->terminateTracker(ex, this))
		return;

	QStringList final;

	final.append("Categorized & Sorted NPC names");
	final.append("------------------------------");
	final.append("");

	{
		QStringList categories = cdcategory_names();
		int categoryIdx = 0;
		for (const QString & category : categories) {
			final.append(QString(" ") + category);
			final.append(QString(" ") + (QString("-").repeated(category.length())));
			final.append("");
			QStringList sortedNames;
			int id = 0;
			for (CdgNPCTBLEntry & qs : npctblMdl->npcs) {
				if (qs.category == categoryIdx)
					sortedNames.append(QString("  ") + qs.name + QString(" - ") + QString::number(id));
				id++;
			}
			sortedNames.sort();
			final.append(sortedNames);
			final.append("");
			categoryIdx++;
		}
	}

	qte->setText(final.join("\n"));
	QVBoxLayout * vbox = new QVBoxLayout(qd);
	vbox->addWidget(qte);
	qd->setLayout(vbox);
	// done
	qd->exec();
	delete qd;
}

void CdiCurlysDesk::stageViewCheckboxToggled(bool b) {
	Q_UNUSED(b);
	updateStageList();
}

CdiCurlysDesk::~CdiCurlysDesk() {
	delete ui;
}
