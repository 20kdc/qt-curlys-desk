#include "cdifindiddialog.h"
#include "ui_cdifindiddialog.h"

CdiFindIdDialog::CdiFindIdDialog(QWidget *parent, const QString & text) :
	QDialog(parent),
	ui(new Ui::CdiFindIdDialog) {
	ui->setupUi(this);
	ui->label->setText(text);
}

CdiFindIdDialog::~CdiFindIdDialog() {
	delete ui;
}

bool CdiFindIdDialog::parseResult(int a, int * b) {
	if (a == QDialog::Accepted) {
		bool ok;
		*b = ui->lineEdit->text().toInt(&ok);
		return ok;
	}
	return false;
}
