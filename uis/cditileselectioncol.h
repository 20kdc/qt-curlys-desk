#ifndef CDITILESELECTIONCOL_H
#define CDITILESELECTIONCOL_H

#include "uis/cditileselection.h"

class CdiTileSelectionCol : public CdiTileSelection {
	Q_OBJECT
public:
	CdiTileSelectionCol(QWidget * parent);

	virtual QPixmap * openTS() override;
	virtual void closeTS(QPixmap * ts) override;

private:
	QPixmap localPixmap;
};

#endif // CDITILESELECTIONCOL_H
