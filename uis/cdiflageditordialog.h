#ifndef CDIFLAGEDITORDIALOG_H
#define CDIFLAGEDITORDIALOG_H

#include <QDialog>

namespace Ui {
class CdiFlagEditorDialog;
}

class CdiFlagEditorDialog : public QDialog
{
	Q_OBJECT

public:
	explicit CdiFlagEditorDialog(QWidget *parent = nullptr);
	~CdiFlagEditorDialog();
	uint16_t value();
	void setValue(uint16_t);
private:
	Ui::CdiFlagEditorDialog *ui;
private slots:
	void textChanged(const QString & hex);
	void valueChanged();
};

#endif // CDIFLAGEDITORDIALOG_H
