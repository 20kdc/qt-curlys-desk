#ifndef CDIMAPEDITOR_H
#define CDIMAPEDITOR_H

#include <QWidget>
#include <functional>

#include "models/cdkgamemdl.h"

namespace Ui {
class CdiMapEditor;
}

class CdiCurlysDesk;
class CdiTileSelection;
class CdtMapToolBase;
class CdpTSCEditor;
class CdgEnumGlobalMdl;

class CdiMapEditor : public QDialog {
	Q_OBJECT

public:
	explicit CdiMapEditor(CdiCurlysDesk * parent, CdgEnumGlobalMdl * enu);
	~CdiMapEditor();

	CdkBaseMapMdl * mdl;
	CdpTSCEditor * tscEditor;
	CdgEnumGlobalMdl * enums;

	QList<CdkMapState> undoList;
	QList<CdkMapState> redoList;

	void save(CdExceptionTracker & ex);

	// Ownership is transferred to CdiMapEditor.
	void setTool(CdtMapToolBase * tool);
	inline CdtMapToolBase * tool() {
		return currentTool;
	}

	void setStatusText(const QString & statusText);

	void stateTransaction(std::function<void()> content);
	void stateTransactionStart();
	void stateTransactionStepStart();
	void stateTransactionStepEnd();
	void stateTransactionEnd();

signals:
	// DO NOT CALL THIS (use stateTransaction)
	void onEdit();

public slots:
	void editTSC();
	void editTiles();
	void editEntities();
	void editCEd();
	void editShiftMap();
	void causeSave();
	void causeRedo();
	void causeUndo();
	void causeFindId();
	void causeFindEv();
	void setZoom1();
	void setZoom2();
	void setZoom3();
	void setZoom4();

private slots:
	void updateViewOptions(bool);

private:
	Ui::CdiMapEditor * ui;
	CdtMapToolBase * currentTool;
	bool inTransaction;
};

#endif // CDIMAPEDITOR_H
