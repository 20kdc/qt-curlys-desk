#ifndef CDISTBLEDITOR_H
#define CDISTBLEDITOR_H

#include <QDialog>
#include "formats/cdfstage.h"

namespace Ui {
class CdiStblEditor;
}

class CdgEnumGlobalMdl;
class CdkBaseStageMdl;

class CdiStblEditor : public QDialog {
	Q_OBJECT

public:
	explicit CdiStblEditor(QWidget * parent, CdgEnumGlobalMdl * enums, CdkBaseStageMdl * ref, const CdfStage & old);
	~CdiStblEditor();

	void accept() override;

	CdfStage result;
	CdgEnumGlobalMdl * enumMdl;
private:
	Ui::CdiStblEditor * ui;
};

#endif // CDISTBLEDITOR_H
