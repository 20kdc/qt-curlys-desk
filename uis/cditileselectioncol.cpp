#include "models/cdkbasemapmdl.h"
#include "models/cdkgamemdl.h"
#include "uis/cditileselectioncol.h"
#include "pieces/cdpcollisionimage.h"

CdiTileSelectionCol::CdiTileSelectionCol(QWidget * parent) : CdiTileSelection(parent) {
	localPixmap = cdpcollisionimage_generate();
}

QPixmap * CdiTileSelectionCol::openTS() {
	return &localPixmap;
}

void CdiTileSelectionCol::closeTS(QPixmap *) {
}
