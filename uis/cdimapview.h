#ifndef CDIMAPVIEW_H
#define CDIMAPVIEW_H

#include <QWidget>

class CdiMapEditor;

class CdiMapView : public QWidget {
	Q_OBJECT
public:
	explicit CdiMapView(QWidget * parent);

	virtual void paintEvent(QPaintEvent * qpd) override;
	virtual void mousePressEvent(QMouseEvent * ev) override;
	virtual void mouseMoveEvent(QMouseEvent * ev) override;
	virtual void mouseReleaseEvent(QMouseEvent * ev) override;

	void coc(int x, int y);

	QPoint camera, dragCameraOfs;
	bool dragCamera = false;
	bool dragWrite = false;

	bool viewEntities = true;
	bool viewTileGrid = false;
	bool viewBackTiles = true;
	bool viewTileTypes = false;
	bool viewBackground = true;
	bool viewEntityInd = true;
	bool viewFrontTiles = true;
	bool viewEntitySprites = true;
	int zoom = 1;

	QPixmap collisionPixmap;

	CdiMapEditor * editor;

signals:

public slots:
private:
	QPoint hoverTile;
	void calcHoverTile(QMouseEvent * qme);
};

#endif // CDIMAPVIEW_H
