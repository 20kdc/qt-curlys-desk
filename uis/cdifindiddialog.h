#ifndef CDIFINDIDDIALOG_H
#define CDIFINDIDDIALOG_H

#include <QDialog>

namespace Ui {
class CdiFindIdDialog;
}

class CdiFindIdDialog : public QDialog {
	Q_OBJECT

public:
	explicit CdiFindIdDialog(QWidget *parent, const QString & text);
	~CdiFindIdDialog();
	bool parseResult(int a, int * b);

private:
	Ui::CdiFindIdDialog *ui;
};

#endif // CDIFINDIDDIALOG_H
