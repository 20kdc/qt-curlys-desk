#ifndef CDITILESELECTIONMTS_H
#define CDITILESELECTIONMTS_H

#include "uis/cditileselection.h"

class CdiTileSelectionMTS : public CdiTileSelection {
	Q_OBJECT
public:
	CdiTileSelectionMTS(QWidget * parent);

	QPixmap * openTS() override;
	void closeTS(QPixmap * ts) override;

	void setMdl(CdkBaseMapMdl * mdl);
private:
	CdkBaseMapMdl * mdl;
};

#endif // CDITILESELECTIONMTS_H
