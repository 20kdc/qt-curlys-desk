#include "uis/cdimapview.h"
#include "uis/cdimapeditor.h"
#include "uis/cditileselection.h"
#include "tools/cdtmaptoolbase.h"
#include "pieces/cdpcollisionimage.h"
#include "cdutils.h"

#include <QPaintEvent>
#include <QPainter>
#include <QMenu>

CdiMapView::CdiMapView(QWidget * parent) : QWidget(parent), camera(0, 0) {
	// oh, now this is awful
	editor = dynamic_cast<CdiMapEditor *>(parent->parent());
	CdkBaseMapMdl * mdl = editor->mdl;
	int ts = mdl->boundMdl->tileSize;
	int tss = ts * zoom;
	camera = QPoint((mdl->w() * tss) / 2, (mdl->h() * tss) / 2);
	hoverTile = QPoint(0, 0);
	collisionPixmap = cdpcollisionimage_generate();
	this->setMouseTracking(true);
	connect(editor, SIGNAL(onEdit()), this, SLOT(repaint()));
}

#define CDIMAPVIEW_CONVRECT(r) QRect(r.x() * tss, r.y() * tss, (r.width() * tss) - 1, (r.height() * tss) - 1)

void CdiMapView::paintEvent(QPaintEvent * qpe) {
	QPainter painter(this);
	QSize sz = size();

	// Background
	painter.fillRect(0, 0, sz.width(), sz.height(), CDUTILS_BACKGROUND_COLOUR);
	CdkBaseMapMdl * mdl = editor->mdl;

	if (viewBackground && !mdl->background.isNull()) {
		int bkgWidth = mdl->background.width() * zoom;
		int bkgHeight = mdl->background.height() * zoom;
		int ofsX = 0;
		int ofsY = 0;
		bool inhibitStandardRendering = false;
		if (mdl->backgroundType == 1) {
			ofsX = camera.x() / (zoom * 2);
			ofsY = camera.y() / (zoom * 2);
		} else if (mdl->backgroundType == 2) {
			ofsX = camera.x() / zoom;
			ofsY = camera.y() / zoom;
		} else if ((mdl->backgroundType == 6) || (mdl->backgroundType == 7)) {
			inhibitStandardRendering = true;
			int heightForFullWidth = (sz.width() * mdl->background.height()) / mdl->background.width();
			int realSplit = (heightForFullWidth * 88) / mdl->background.height();
			// top
			painter.drawPixmap(0, 0, sz.width(), realSplit, mdl->background, 0, 0, mdl->background.width(), 88);
			// extended top
			painter.drawPixmap(0, realSplit, sz.width(), sz.height() - realSplit, mdl->background, 0, 87, mdl->background.width(), 1);
			// lower clouds
			int lowerHeightSc = 152 * zoom;
			for (int i = 0; i < cdutils_celldivceil(sz.width(), bkgWidth); i++)
				painter.drawPixmap(i * bkgWidth, sz.height() - lowerHeightSc, bkgWidth, lowerHeightSc, mdl->backgroundCK, 0, 88, mdl->background.width(), 152);
		} else if (mdl->backgroundType != 0) {
			inhibitStandardRendering = true;
		}
		if (!inhibitStandardRendering) {
			ofsX = cdutils_cellmod(ofsX * zoom, bkgWidth);
			ofsY = cdutils_cellmod(ofsY * zoom, bkgHeight);
			for (int i = 0; i < cdutils_celldivceil(sz.width() + ofsX, bkgWidth); i++)
				for (int j = 0; j < cdutils_celldivceil(sz.height() + ofsY, bkgHeight); j++)
					painter.drawPixmap((i * bkgWidth) - ofsX, (j * bkgHeight) - ofsY, bkgWidth, bkgHeight, mdl->background, 0, 0, mdl->background.width(), mdl->background.height());
		}
	}

	// World
	painter.save();
	auto cameraTL = QPoint(camera.x() - (sz.width() / 2), camera.y() - (sz.height() / 2));
	auto cameraBR = cameraTL + QPoint(sz.width(), sz.height());
	auto cameraBounds = QRect(cameraTL, cameraBR);

	painter.translate(-cameraTL);

	int ts = mdl->boundMdl->tileSize;
	int tss = ts * zoom;
	auto cameraTileBounds = QRect(QPoint(cdutils_celldiv(cameraTL.x(), tss), cdutils_celldiv(cameraTL.y(), tss)), QPoint(cdutils_celldiv(cameraBR.x(), tss), cdutils_celldiv(cameraBR.y(), tss)));

	for (int j = cameraTileBounds.top(); j <= cameraTileBounds.bottom(); j++) {
		for (int i = cameraTileBounds.left(); i <= cameraTileBounds.right(); i++) {
			if (i < 0 || i >= (int) mdl->w())
				continue;
			if (j < 0 || j >= (int) mdl->h())
				continue;

			QPixmap * tileset = &editor->mdl->tileset;
			uint8_t tgt = mdl->tile(i, j);
			int tsFinal = ts;
			uint8_t collision = mdl->collisionMap[tgt];
			if (viewTileTypes) {
				tileset = &collisionPixmap;
				tgt = collision;
				tsFinal = 16;
			} else {
				bool fg = (collision == 0x05) || (collision >= 0x40);
				if (fg && !viewFrontTiles)
					continue;
				if (!fg && !viewBackTiles)
					continue;
			}
			int tsx = tgt & 0xF;
			int tsy = tgt >> 4;
			painter.drawPixmap(i * tss, j * tss, tss, tss, *tileset, tsx * tsFinal, tsy * tsFinal, tsFinal, tsFinal);
		}
	}
	if (viewTileGrid) {
		painter.setPen(CDUTILS_TILE_COLOUR);
		for (int i = cameraTileBounds.top(); i <= cameraTileBounds.bottom(); i++)
			painter.drawLine(cameraTL.x(), i * tss, cameraBR.x(), i * tss);
		for (int i = cameraTileBounds.left(); i <= cameraTileBounds.right(); i++)
			painter.drawLine(i * tss, cameraTL.y(), i * tss, cameraBR.y());
	}
	for (const CdkEntity & entity : mdl->state.entities) {
		QRect entityBounds = CDIMAPVIEW_CONVRECT(QRect(entity.x, entity.y, 1, 1));
		if (!cameraBounds.intersects(entityBounds))
			continue;
		CdkEntityDrawInfo edi = mdl->getEntityDrawInfo(entity);
		if (viewEntitySprites) {
			if (edi.image != nullptr) {
				QPoint topLeft = entityBounds.topLeft() + (edi.shift * zoom);
				QSize sz = edi.bounds.size() * zoom;
				painter.drawPixmap(topLeft.x(), topLeft.y(), sz.width(), sz.height(), *edi.image, edi.bounds.x(), edi.bounds.y(), edi.bounds.width(), edi.bounds.height());
			}
		}
		if (viewEntities) {
			// draw rect over entity
			painter.setPen(CDUTILS_ENTITY_COLOUR);
			painter.drawRect(entityBounds);
		}
		if (viewEntityInd) {
			if (edi.indicator.width() != 0) {
				painter.setPen(CDUTILS_INDICATOR_COLOUR);
				painter.drawRect(entityBounds.x() + (edi.indicator.x() * zoom), entityBounds.y() + (edi.indicator.y() * zoom), (edi.indicator.width() * zoom) - 1, (edi.indicator.height() * zoom) - 1);
			}
		}
	}
	CdtMapToolBase * mapTool = editor->tool();

	QRect select = CDIMAPVIEW_CONVRECT(mapTool->tileBoundRect());
	select.adjust(-1, -1, 1, 1);
	painter.setPen(CDUTILS_SELECTION_COLOUR);
	painter.drawRect(select);

	painter.setPen(CDUTILS_BORDER_COLOUR);
	select = CDIMAPVIEW_CONVRECT(QRect(0, 0, mdl->w(), mdl->h()));
	select.adjust(-1, -1, 1, 1);
	painter.drawRect(select);

	mapTool->paintMap(painter);

	painter.restore();
	qpe->accept();

}

void CdiMapView::coc(int x, int y) {
	int ts = editor->mdl->boundMdl->tileSize;
	int tss = ts * zoom;
	camera = QPoint(x * tss, y * tss);
	repaint();
}

void CdiMapView::calcHoverTile(QMouseEvent * event) {
	QSize sz = size();
	int tlX = camera.x() - (sz.width() / 2);
	int tlY = camera.y() - (sz.height() / 2);
	int tss = editor->mdl->boundMdl->tileSize * zoom;
	hoverTile = QPoint(cdutils_celldiv(event->x() + tlX, tss), cdutils_celldiv(event->y() + tlY, tss));
	QMenu * menu = editor->tool()->inMouseUpdate(dragWrite, hoverTile.x(), hoverTile.y());
	if (menu != nullptr) {
		menu->move(mapToGlobal(event->pos()));
		menu->exec();
		delete menu;
	}
	editor->setStatusText(QString::number(hoverTile.x()) + ", " + QString::number(hoverTile.y()));
	repaint();
}

void CdiMapView::mousePressEvent(QMouseEvent * event) {
	if (event->button() == Qt::MouseButton::RightButton) {
		dragCamera = true;
		dragCameraOfs = event->pos();
	} else if (event->button() == Qt::MouseButton::LeftButton) {
		dragWrite = true;
	}
	calcHoverTile(event);
}

void CdiMapView::mouseMoveEvent(QMouseEvent * event) {
	if (dragCamera) {
		camera -= event->pos() - dragCameraOfs;
		dragCameraOfs = event->pos();
		repaint();
	}
	calcHoverTile(event);
}

void CdiMapView::mouseReleaseEvent(QMouseEvent * event) {
	if (event->button() == Qt::MouseButton::RightButton)
		dragCamera = false;
	else if (event->button() == Qt::MouseButton::LeftButton)
		dragWrite = false;
	calcHoverTile(event);
}
