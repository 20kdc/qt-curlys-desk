#include "cdiflageditordialog.h"
#include "ui_cdiflageditordialog.h"

CdiFlagEditorDialog::CdiFlagEditorDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::CdiFlagEditorDialog) {
	ui->setupUi(this);
	connect(ui->tbFlags, SIGNAL(textChanged(const QString &)), this, SLOT(textChanged(const QString &)));
	connect(ui->flagEditor, SIGNAL(valueChanged()), this, SLOT(valueChanged()));
}

uint16_t CdiFlagEditorDialog::value() {
	return ui->flagEditor->value();
}

void CdiFlagEditorDialog::setValue(uint16_t value) {
	// this will carry into main flag box
	ui->tbFlags->setText(QString("%1").arg(value, 4, 16, QLatin1Char('0')));
}

void CdiFlagEditorDialog::textChanged(const QString & hex) {
	bool ok;
	int newVal = hex.toInt(&ok, 16);
	if (ok)
		ui->flagEditor->setValue(newVal);
}

void CdiFlagEditorDialog::valueChanged() {
	setValue(ui->flagEditor->value());
}

CdiFlagEditorDialog::~CdiFlagEditorDialog() {
	delete ui;
}
