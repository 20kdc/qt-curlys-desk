#include "cdistbleditor.h"
#include "ui_cdistbleditor.h"
#include "models/cdkgamemdl.h"
#include "models/cdgenumglobalmdl.h"

CdiStblEditor::CdiStblEditor(QWidget *parent, CdgEnumGlobalMdl * enums, CdkBaseStageMdl * ref, const CdfStage & stage) : QDialog(parent), ui(new Ui::CdiStblEditor) {
	ui->setupUi(this);
	enumMdl = enums;

	ui->tbBkg->setText(stage.bkg);
	ui->tbMapId->setText(stage.mapId);
	enumMdl->initCB(ui->cbScrollType, "scrollType", stage.bkgType);
	ui->tbTileset->setText(stage.tileset);
	enumMdl->initCB(ui->cbBoss, "boss", stage.bossType);
	ui->tbNS1->setText(stage.npc1Tile);
	ui->tbNS2->setText(stage.npc2Tile);
	ui->tbMNA1->setText(stage.mnaPrimary);
	ui->tbMNA2->setText(stage.mnaSecondary);

	ui->tbMNA2->setEnabled(ref->supportsMNA2());
}

void CdiStblEditor::accept() {
	result.bkg = ui->tbBkg->text();
	result.mapId = ui->tbMapId->text();
	result.bkgType = enumMdl->getCB(ui->cbScrollType);
	result.tileset = ui->tbTileset->text();
	result.bossType = enumMdl->getCB(ui->cbBoss);
	result.npc1Tile = ui->tbNS1->text();
	result.npc2Tile = ui->tbNS2->text();
	result.mnaPrimary = ui->tbMNA1->text();
	result.mnaSecondary = ui->tbMNA2->text();
	QDialog::accept();
}

CdiStblEditor::~CdiStblEditor() {
	delete ui;
}
