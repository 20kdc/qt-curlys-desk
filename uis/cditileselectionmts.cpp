#include "models/cdkbasemapmdl.h"
#include "models/cdkgamemdl.h"
#include "uis/cditileselectionmts.h"

CdiTileSelectionMTS::CdiTileSelectionMTS(QWidget * parent) : CdiTileSelection(parent) {
}

void CdiTileSelectionMTS::setMdl(CdkBaseMapMdl * _m) {
	setTileSize(_m->boundMdl->tileSize, _m->boundMdl->tileSize * _m->boundMdl->scale);
	mdl = _m;
}

QPixmap * CdiTileSelectionMTS::openTS() {
	if (!mdl)
		return nullptr;
	return &(mdl->tileset);
}

void CdiTileSelectionMTS::closeTS(QPixmap *) {
}
