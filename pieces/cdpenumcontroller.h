#ifndef CDPENUMCONTROLLER_H
#define CDPENUMCONTROLLER_H

#include <QObject>

class QComboBox;
class CdkGameMdl;
class CdgEnumGlobalMdl;

class CdpEnumController : public QObject {
	Q_OBJECT
public:
	CdpEnumController(QComboBox * parent, CdkGameMdl * mdl);
	CdpEnumController(QComboBox * parent, CdkGameMdl * mdl, const QString & enumType) : CdpEnumController(parent, mdl) { clear(enumType); }

	void clear(const QString & enumType);
	int getValue() { return storedVal; }
	void setValue(int value);

signals:
	void valueChanged();
	void valueChanged(int);

private slots:
	void cbTextChanged(const QString &);

private:
	int storedVal;
	QComboBox * comboBox;
	CdgEnumGlobalMdl * enumMdl;
};

#endif // CDPENUMCONTROLLER_H
