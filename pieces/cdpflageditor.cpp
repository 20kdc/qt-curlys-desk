#include "cdpflageditor.h"
#include "ui_cdpflageditor.h"

CdpFlagEditor::CdpFlagEditor(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::CdpFlagEditor) {
	ui->setupUi(this);
	boxes[0] = ui->cb0; boxes[1] = ui->cb1; boxes[2] = ui->cb2; boxes[3] = ui->cb3;
	boxes[4] = ui->cb4; boxes[5] = ui->cb5; boxes[6] = ui->cb6; boxes[7] = ui->cb7;
	boxes[8] = ui->cb8; boxes[9] = ui->cb9; boxes[10] = ui->cb10; boxes[11] = ui->cb11;
	boxes[12] = ui->cb12; boxes[13] = ui->cb13; boxes[14] = ui->cb14; boxes[15] = ui->cb15;

	for (int i = 0; i < 16; i++)
		connect(boxes[i], SIGNAL(toggled(bool)), this, SLOT(flagChange(bool)));
}

CdpFlagEditor::~CdpFlagEditor() {
	delete ui;
}

void CdpFlagEditor::setValue(uint16_t value) {
	inhibit = true;
	for (int i = 0; i < 16; i++)
		boxes[i]->setChecked(value & (1 << i));
	inhibit = false;
}

uint16_t CdpFlagEditor::value() {
	uint16_t bitField = 0;
	for (int i = 0; i < 16; i++)
		if (boxes[i]->checkState() == Qt::CheckState::Checked)
			bitField |= 1 << i;
	return bitField;
}

void CdpFlagEditor::flagChange(bool) {
	if (!inhibit)
		emit valueChanged();
}
