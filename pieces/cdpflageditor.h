#ifndef CDPFLAGEDITOR_H
#define CDPFLAGEDITOR_H

#include <QWidget>

namespace Ui {
class CdpFlagEditor;
}

class QCheckBox;

class CdpFlagEditor : public QWidget {
	Q_OBJECT

public:
	explicit CdpFlagEditor(QWidget *parent = nullptr);
	~CdpFlagEditor();
	void setValue(uint16_t value);
	uint16_t value();

signals:
	void valueChanged();

private slots:
	void flagChange(bool);

private:
	Ui::CdpFlagEditor *ui;
	QCheckBox * boxes[16];
	bool inhibit = false;
};

#endif // CDPFLAGEDITOR_H
