#ifndef CDPTSCNUMERICS_H
#define CDPTSCNUMERICS_H

#include <QWidget>

namespace Ui {
class CdpTSCNumerics;
}

class QTextEdit;
class CdgTSCLanguageMdl;
class CdgEnumGlobalMdl;

class CdpTSCNumerics : public QWidget {
	Q_OBJECT

public:
	explicit CdpTSCNumerics(QWidget * parent, QTextEdit * editor, CdgTSCLanguageMdl * mdl);
	~CdpTSCNumerics();

private:
	CdgEnumGlobalMdl * enumMdl;
	QStringList listOfCommands;
	QTextEdit * editor;
	CdgTSCLanguageMdl * model;
	bool hasLock = false;
	int lockPosition = 0;
	bool inhibit = false;
	Ui::CdpTSCNumerics *ui;
private slots:
	void numberChanged(const QString & text);
	void tscChanged(const QString & text);
	void autoTrackTSC();
	void showTSCCommandHelp(int);
	void enumNumberChanged(int);
};

#endif // CDPTSCNUMERICS_H
