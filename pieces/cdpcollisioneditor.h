#ifndef CDPCOLLISIONEDITOR_H
#define CDPCOLLISIONEDITOR_H

#include <QDialog>
#include "models/cdkbasemapmdl.h"

namespace Ui {
class CdpCollisionEditor;
}

class CdpCollisionEditor : public QDialog {
	Q_OBJECT

public:
	explicit CdpCollisionEditor(QWidget * parent, CdkBaseMapMdl * mdl);
	~CdpCollisionEditor();

private slots:
	void keyChanged();
	void valChanged();

private:
	Ui::CdpCollisionEditor * ui;
	CdkBaseMapMdl * mdl;

};

#endif // CDPCOLLISIONEDITOR_H
