#include "cdpcollisionimage.h"

#include <QPainter>

#include "assets/tiles.xpm"

#define TS_R1(a) cond |= (tileId & a) != 0;
#define TS_R2(a, b) cond |= (tileId & a) != b;
#define TS_R3(a, b) cond |= (tileId >= a) && (tileId <= b);
#define TS_RE(a) cond |= tileId == a;
#define TS_D(off1, off2, on1, on2) \
	if (cond) painter.drawImage(baseX, baseY, buf, on1, on2, 16, 16); else painter.drawImage(baseX, baseY, buf, off1, off2, 16, 16); cond = false;
#define TS_O(on1, on2) \
	if (cond) painter.drawImage(baseX, baseY, buf, on1, on2, 16, 16); cond = false;
#define TS_X(off1, off2) \
	if (!cond) painter.drawImage(baseX, baseY, buf, off1, off2, 16, 16); cond = false;

static void cditileselectioncol_drawImage(QPainter & painter, QImage & buf, int baseX, int baseY, int tileId) {
	bool cond = false;
	// blank
	TS_R2(0, 1) TS_D(16, 48, 16, 32)
	// nodraw
	TS_R3(0x20, 0x3F) TS_O(16,  0)
	// water
	TS_RE(0x02) TS_R3(0x60, 0x62) TS_R3(0x70, 0x77) TS_R3(0xA0, 0xA3) TS_O(64, 32)
	// star
	TS_RE(0x43) TS_O( 0, 64)
	// 50Sl
	TS_RE(0x50) TS_RE(0x70) TS_O(32, 32)
	// 51Sl
	TS_RE(0x51) TS_RE(0x71) TS_O(48, 32)
	// 52Sl
	TS_RE(0x52) TS_RE(0x72) TS_O(32, 48)
	// 53Sl
	TS_RE(0x53) TS_RE(0x73) TS_O(48, 48)
	// 54Sl
	TS_RE(0x54) TS_RE(0x74) TS_O(32, 16)
	// 55Sl
	TS_RE(0x55) TS_RE(0x75) TS_O(48, 16)
	// 56Sl
	TS_RE(0x56) TS_RE(0x76) TS_O(32,  0)
	// 57Sl
	TS_RE(0x57) TS_RE(0x77) TS_O(48,  0)
	// Solid
	TS_RE(0x41) TS_RE(0x61) TS_O(16, 48)
	// Foreground
	TS_R1(0xC0) TS_O(48, 64)
	// No Bult
	TS_RE(0x05) TS_O(48, 64)
	// No NPC (and +)
	TS_R3(0x03, 0x04) TS_RE(0x44) TS_O(64, 16)
	TS_RE(0x64) TS_O(64, 48)
	// No PLYR
	TS_RE(0x46) TS_O(32, 64)
	// Spike
	TS_RE(0x42) TS_RE(0x62) TS_O(64, 0)
	// Arrow : Left
	TS_RE(0x80) TS_RE(0xA0) TS_O(0, 48)
	// Arrow : Up
	TS_RE(0x81) TS_RE(0xA1) TS_O(0, 32)
	// Arrow : Right
	TS_RE(0x82) TS_RE(0xA2) TS_O(0, 16)
	// Arrow : Down
	TS_RE(0x83) TS_RE(0xA3) TS_O(16, 16)
}

QPixmap cdpcollisionimage_generate() {
	// Create the tileset image.
	QImage localImage = QImage(256, 256, QImage::Format_ARGB32);
	QImage backup_buf = QImage(tiles_xpm);
	{
		QPainter qp(&localImage);
		for (int i = 0; i < 16; i++)
			for (int j = 0; j < 16; j++)
				cditileselectioncol_drawImage(qp, backup_buf, i * 16, j * 16, (i + (j * 16)));
	}
	QPixmap localPixmap;
	localPixmap.convertFromImage(localImage);
	return localPixmap;
}
