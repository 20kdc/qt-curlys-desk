#ifndef CDPNPCTBLENTRYVIEWER_H
#define CDPNPCTBLENTRYVIEWER_H

#include <QWidget>
#include "models/cdgnpctblglobalmdl.h"

class CdpNPCTBLEntryViewer : public QWidget
{
	Q_OBJECT
public:
	explicit CdpNPCTBLEntryViewer(QWidget *parent = nullptr);

	// NOTE: Pixmap is copied, not borrowed.
	void setEntry(const CdgNPCTBLEntry & entry, QPixmap * sheet);
	void paintEvent(QPaintEvent *event) override;

private:
	CdgNPCTBLEntry entry;
	QPixmap sheet;
};

#endif // CDPNPCTBLENTRYVIEWER_H
