#include <QComboBox>
#include "cdpenumcontroller.h"
#include "models/cdkgamemdl.h"
#include "models/cdgenumglobalmdl.h"

CdpEnumController::CdpEnumController(QComboBox * p, CdkGameMdl * mdl) : QObject(p), comboBox(p) {
	enumMdl = mdl->findOptDependency<CdgEnumGlobalMdl>();
	p->setEditable(true);
	p->setCurrentText("0");
	connect(p, SIGNAL(currentTextChanged(const QString &)), this, SLOT(cbTextChanged(const QString &)));
	storedVal = 0;
}

void CdpEnumController::clear(const QString & enumType) {
	comboBox->clear();
	if (enumMdl) {
		if (enumMdl->enums.contains(enumType)) {
			auto enm = enums[key];
			QList<int> sortedKeys = enm.keys();
			std::sort(sortedKeys.begin(), sortedKeys.end());
			for (int val : sortedKeys)
				target->addItem(QString::number(val) + QString(" - ") + enm[val]);
		}
	}
	comboBox->clearEditText();
}

void CdpEnumController::cbTextChanged(const QString & text) {
	bool ok;
	int val = text.split(" ")[0].toInt(&ok);
	if (ok) {
		if (storedVal == val) {
			// editing text of existing entry
		} else {
			setValue(val);
		}
	}
}

void CdpEnumController::setValue(int value) {
	storedVal = value;
	comboBox->setCurrentIndex(-1);
	comboBox->setCurrentText(QString::number(value));
	emit valueChanged();
	emit valueChanged(value);
}
