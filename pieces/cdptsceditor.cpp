#include <QFile>
#include <QTextCodec>
#include "cdptsceditor.h"
#include "ui_cdptsceditor.h"
#include "models/language/cdkbaselanguagemdl.h"

static void tscrypt(QByteArray & qba, bool encrypt) {
	if (qba.length() == 0)
		return;
	int ql = qba.length();
	int key = qba[ql / 2];
	if (key == 0)
		key = -7;
	for (int i = 0; i < ql; i++) {
		if (i == (ql / 2))
			continue;
		if (encrypt) {
			qba[i] = (char) (qba[i] + key);
		} else {
			qba[i] = (char) (qba[i] - key);
		}
	}
}

CdpTSCEditor::CdpTSCEditor(QWidget * parent, QTextCodec * qtc, CdkBaseLanguageMdl * mdl, QString fn) :
	QDialog(parent),
	ui(new Ui::CdpTSCEditor),
	filename(fn),
	codec(qtc) {
	ui->setupUi(this);
	setWindowTitle(filename);

	QFile src(filename);
	if (src.open(QFile::ReadOnly)) {
		QByteArray qba = src.readAll();
		tscrypt(qba, false);
		ui->textEdit->setText(codec->toUnicode(qba));
		src.close();
	}

	// CE uses one of these fonts. I think.
	QFont font("Lucida Sans Unicode");
	font.setStyleHint(QFont::TypeWriter);
	ui->textEdit->setFont(font);

	if (mdl)
		ui->horizontalLayout->addWidget(mdl->provideServices(ui->textEdit));
}

CdpTSCEditor::~CdpTSCEditor() {
	delete ui;
}

void CdpTSCEditor::save(CdExceptionTracker & ex) {
	QFile src(filename);
	if (src.open(QFile::WriteOnly)) {
		QByteArray qba = codec->fromUnicode(ui->textEdit->toPlainText());
		tscrypt(qba, true);
		src.write(qba);
		src.close();
	} else {
		CD_THROW("Unable to save TSC file.");
	}
}
