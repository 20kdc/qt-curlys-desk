#include "cdpcollisioneditor.h"
#include "ui_cdpcollisioneditor.h"
#include "models/cdkgamemdl.h"

CdpCollisionEditor::CdpCollisionEditor(QWidget * parent, CdkBaseMapMdl * _m) :
	QDialog(parent),
	ui(new Ui::CdpCollisionEditor),
	mdl(_m) {
	ui->setupUi(this);
	ui->tsKey->canWide = false;
	ui->tsCollision->canWide = false;
	ui->tsKey->setMdl(_m);
	connect(ui->tsKey, SIGNAL(changedVal()), this, SLOT(keyChanged()));
	connect(ui->tsCollision, SIGNAL(changedVal()), this, SLOT(valChanged()));
	ui->tsCollision->setTileSize(16, _m->boundMdl->tileSize * _m->boundMdl->scale);
	ui->tsKey->changedVal();
	ui->tsCollision->fixSize();
}

void CdpCollisionEditor::keyChanged() {
	ui->tsCollision->setIndex(mdl->collisionMap[ui->tsKey->index()]);
}
void CdpCollisionEditor::valChanged() {
	mdl->collisionMap[ui->tsKey->index()] = (uint8_t) ui->tsCollision->index();
}

CdpCollisionEditor::~CdpCollisionEditor() {
	delete ui;
}
