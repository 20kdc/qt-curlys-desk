#include "cdptscnumerics.h"
#include "models/language/cdgtsclanguagemdl.h"
#include "models/cdgenumglobalmdl.h"
#include <QTextEdit>
#include <QTextBlock>
#include "models/cdkgamemdl.h"
#include <algorithm>
#include "ui_cdptscnumerics.h"

CdpTSCNumerics::CdpTSCNumerics(QWidget * parent, QTextEdit * ed, CdgTSCLanguageMdl * mdl) : QWidget(parent),
	editor(ed), model(mdl), ui(new Ui::CdpTSCNumerics) {
	ui->setupUi(this);
	enumMdl = mdl->boundMdl->findOptDependency<CdgEnumGlobalMdl>();

	connect(ed, SIGNAL(cursorPositionChanged()), this, SLOT(autoTrackTSC()));
	connect(ui->number, SIGNAL(textChanged(const QString &)), this, SLOT(numberChanged(const QString &)));
	connect(ui->tsc, SIGNAL(textChanged(const QString &)), this, SLOT(tscChanged(const QString &)));
	connect(ui->cbEnum, SIGNAL(currentIndexChanged(int)), this, SLOT(enumNumberChanged(int)));

	QStringList lst;
	for (const CdgTSCLanguageCommand & cmd : mdl->commands)
		lst.append(cmd.name);
	std::sort(lst.begin(), lst.end());

	for (const QString & key : lst) {
		const CdgTSCLanguageCommand & cmd = mdl->commands[key];
		listOfCommands.append(key);
		ui->listWidget->addItem(QLatin1String("<") + cmd.name + QLatin1String(" - ") + cmd.longName);
	}

	connect(ui->listWidget, SIGNAL(currentRowChanged(int)), this, SLOT(showTSCCommandHelp(int)));
}

static QString cdptscnumerics_solve(int i, bool * ok) {
	*ok = false;
	if ((i >= 0) && (i <= 9999)) {
		*ok = true;
		return QString("%1").arg(i, 4, 10, QLatin1Char('0'));
	}
	// Alright, time for plan B.
	// The theoretically acceptable range is from " " to "~".
	// That's 32 to 126, or 95 characters.
	// The lowest number possible here is -17776.
	// So base everything around that; if the number is presently 0, that means it has to go up by 17776.
	// Then increase from highest to lowest as necessary.
	i += 17776;
	char workspace[4];
	workspace[0] = ' ';
	workspace[1] = ' ';
	workspace[2] = ' ';
	workspace[3] = ' ';
	// i is now the "remaining stuff to deal with".
	// Note that the digits are measured according to their theoretical maximum capabilities.
	while ((workspace[0] != '~') && (i >= 8659)) {
		workspace[0]++;
		i -= 1000;
	}
	while ((workspace[1] != '~') && (i >= 859)) {
		workspace[1]++;
		i -= 100;
	}
	while ((workspace[2] != '~') && (i >= 79)) {
		workspace[2]++;
		i -= 10;
	}
	while ((workspace[3] != '~') && (i >= 1)) {
		workspace[3]++;
		i -= 1;
	}
	*ok = i == 0;
	return QLatin1String(workspace, 4);
}

void CdpTSCNumerics::numberChanged(const QString & text) {
	if (inhibit)
		return;
	ui->tsc->setVisible(true);
	bool ok = false;
	int i = text.toInt(&ok);
	if (!ok)
		return;
	inhibit = true;
	QString res = cdptscnumerics_solve(i, &ok);
	ui->tsc->setText(res);
	if (ok) {
		if (hasLock) {
			QString doc = editor->toPlainText();
			doc = doc.mid(0, lockPosition) + res + doc.mid(lockPosition + 4);
			inhibit = true;
			editor->setPlainText(doc);
			editor->textCursor().setPosition(lockPosition);
			inhibit = false;
		}
	}
	inhibit = false;
	ui->tsc->setVisible(ok);
}

void CdpTSCNumerics::tscChanged(const QString & text) {
	if (inhibit)
		return;
	// it would be nice to actually use that codec information here but w/e
	QByteArray stuff = text.toLatin1();
	if (stuff.length() != 4)
		return;
	int a = stuff[0] - '0';
	int b = stuff[1] - '0';
	int c = stuff[2] - '0';
	int d = stuff[3] - '0';
	a *= 1000;
	b *= 100;
	c *= 10;
	int res = a + b + c + d;
	inhibit = true;
	ui->number->setText(QString::number(res));
	inhibit = false;
}

void CdpTSCNumerics::autoTrackTSC() {
	if (inhibit)
		return;
	hasLock = false;
	inhibit = true;
	ui->cbEnum->setEnabled(false);
	ui->cbEnum->clear();
	inhibit = false;
	QTextCursor textCursor = editor->textCursor();
	int posInBlock = textCursor.positionInBlock();
	QTextBlock block = textCursor.block();
	QString blockText = block.text();
	CdgTSCCompletionData data = model->provideCompletionData(blockText);
	for (const CdgTSCCompletionNumberDatum & datum : data.numbers) {
		if (blockText.length() - datum.start < 4)
			continue;
		if ((posInBlock >= datum.start) && (posInBlock < (datum.start + 4))) {
			// deliberately allow events to do the rest
			ui->tsc->setText(blockText.mid(datum.start, 4));
			ui->tsc->setVisible(true);
			hasLock = true;
			lockPosition = block.position() + datum.start;
			if (datum.enumType != "") {
				if (enumMdl) {
					if (enumMdl->enums.contains(datum.enumType)) {
						inhibit = true;
						enumMdl->initCB(ui->cbEnum, datum.enumType);
						ui->cbEnum->setEnabled(true);
						enumMdl->setCB(ui->cbEnum, datum.enumType, ui->number->text().toInt());
						inhibit = false;
					}
				}
			}
			break;
		}
	}
	for (const CdgTSCCompletionCommandDatum & datum : data.commands) {
		if ((posInBlock >= datum.start) && (posInBlock < (datum.start + datum.fmtText.length()))) {
			QString fmtText;
			int relLockPos = lockPosition - (block.position() + datum.start);
			if (hasLock && (relLockPos >= 0) && ((relLockPos + 3) < datum.fmtText.length())) {
				QString a = datum.fmtText.mid(0, relLockPos);
				QString b = datum.fmtText.mid(relLockPos, 4);
				QString c = datum.fmtText.mid(relLockPos + 4);
				fmtText = a.toHtmlEscaped() + "<b>" + b.toHtmlEscaped() + "</b>" + c.toHtmlEscaped();
			} else {
				fmtText = datum.fmtText.toHtmlEscaped();
			}
			ui->textBrowser->setHtml(QString("<span style=\"font-family: monospace;\">") + fmtText + QString("</span><hr/>") + datum.description);
		}
	}
}

void CdpTSCNumerics::enumNumberChanged(int) {
	if (inhibit)
		return;
	if (enumMdl)
		ui->number->setText(QString::number(enumMdl->getCB(ui->cbEnum)));
}

void CdpTSCNumerics::showTSCCommandHelp(int num) {
	if (num < 0)
		return;
	const CdgTSCLanguageCommand & cmd = model->commands[listOfCommands[num]];
	QString fmtText = (QLatin1String("<") + cmd.name + cmd.parameterText).toHtmlEscaped();
	ui->textBrowser->setHtml(QString("<span style=\"font-family: monospace;\">") + fmtText + QString("</span><hr/>") + cmd.description);
}

CdpTSCNumerics::~CdpTSCNumerics() {
	delete ui;
}
