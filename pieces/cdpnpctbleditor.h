#ifndef CDPNPCTBLEDITOR_H
#define CDPNPCTBLEDITOR_H

#include <QDialog>
#include "models/cdgnpctblglobalmdl.h"

namespace Ui {
class CdpNPCTBLEditor;
}

class CdgNPCTBLGlobalMdl;
class CdgEnumGlobalMdl;
class CdpEnumController;

class CdpNPCTBLEditor : public QDialog {
	Q_OBJECT

public:
	CdpNPCTBLEditor(QWidget * parent, CdgEnumGlobalMdl * enu, CdgNPCTBLGlobalMdl * mdl);
	~CdpNPCTBLEditor();

	void accept() override;

	CdgNPCTBLGlobalMdl * target;
	QList<CdgNPCTBLEntry> localDataCopy;

public slots:
	void resizeNPCTable();
	void npcChange(int);
	void valChange(int);
	void valChange(const QString &);
	void valChange();

private:
	bool inhibit;
	Ui::CdpNPCTBLEditor *ui;
	CdgEnumGlobalMdl * enums;
	CdpEnumController * cbSurface;
	CdpEnumController * cbDeath;
	CdpEnumController * cbHurt;
	CdpEnumController * cbSize;
	CdpEnumController * cbCat;
	void regenNPCList();
	void updateVisualizer(const CdgNPCTBLEntry & npc);
};

#endif // CDPNPCTBLEDITOR_H
