#ifndef CDPTSCEDITOR_H
#define CDPTSCEDITOR_H

#include <QDialog>
#include <QSyntaxHighlighter>
#include <QTextEdit>
#include "cdexcept.h"

namespace Ui {
class CdpTSCEditor;
}

class CdkBaseLanguageMdl;

class CdpTSCEditor : public QDialog {
	Q_OBJECT

public:
	// Language model may be null.
	explicit CdpTSCEditor(QWidget * parent, QTextCodec * codec, CdkBaseLanguageMdl * mdl, QString target);
	~CdpTSCEditor();

	void save(CdExceptionTracker & ex);

private:
	Ui::CdpTSCEditor * ui;
	QString filename;
	QTextCodec * codec;
};

#endif // CDPTSCEDITOR_H
