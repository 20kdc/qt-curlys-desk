#include "cdpnpctblentryviewer.h"
#include <QPainter>
#include <QPaintEvent>
#include "cdutils.h"

CdpNPCTBLEntryViewer::CdpNPCTBLEntryViewer(QWidget *parent) : QWidget(parent) {

}

void CdpNPCTBLEntryViewer::setEntry(const CdgNPCTBLEntry & ent, QPixmap * s) {
	entry = ent;
	sheet = *s;
	repaint();
}

void CdpNPCTBLEntryViewer::paintEvent(QPaintEvent * event) {
	QPainter painter(this);
	QSize sz = size();

	// Background
	painter.fillRect(0, 0, sz.width(), sz.height(), CDUTILS_BACKGROUND_COLOUR);

	painter.translate(sz.width() / 2, sz.height() / 2);

	QRect imageRect = QRect(-entry.displayL, -entry.displayU, entry.cfgA.spriteBounds.width(), entry.cfgA.spriteBounds.height());

	if (!sheet.isNull())
		painter.drawPixmap(imageRect, sheet, entry.cfgA.spriteBounds);

	painter.setPen(CDUTILS_IMAGE_COLOUR);
	painter.drawRect(QRect(imageRect.x(), imageRect.y(), imageRect.width() - 1, imageRect.height() - 1));

	painter.setPen(CDUTILS_DISPLAY_COLOUR);
	painter.drawRect(QRect(-entry.displayL, -entry.displayU, entry.displayR + entry.displayL - 1, entry.displayD + entry.displayU - 1));

	painter.setPen(CDUTILS_HIT_COLOUR);
	painter.drawRect(QRect(-entry.hitL, -entry.hitU, entry.hitR + entry.hitL - 1, entry.hitD + entry.hitU - 1));

	event->accept();
}
