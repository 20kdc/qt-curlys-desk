#include "cdpnpctbleditor.h"
#include "ui_cdpnpctbleditor.h"
#include "models/cdkgamemdl.h"
#include "models/cdgenumglobalmdl.h"
#include "cdpenumcontroller.h"

CdpNPCTBLEditor::CdpNPCTBLEditor(QWidget *parent, CdgEnumGlobalMdl * enu, CdgNPCTBLGlobalMdl * targ) : QDialog(parent), target(targ), inhibit(false), ui(new Ui::CdpNPCTBLEditor) {
	ui->setupUi(this);
	enums = enu;

	cbSurface = new CdpEnumController(ui->cbSurface, targ->boundMdl, "surface");
	cbHurt = new CdpEnumController(ui->cbHurt, targ->boundMdl, "sound");
	cbDeath = new CdpEnumController(ui->cbDeath, targ->boundMdl, "sound");
	cbSize = new CdpEnumController(ui->cbSize, targ->boundMdl, "size");
	cbCat = new CdpEnumController(ui->cbCat, targ->boundMdl, "npcCategory");

	localDataCopy = target->npcs;
	regenNPCList();
	npcChange(0);

	connect(ui->cbNPC, SIGNAL(currentIndexChanged(int)), this, SLOT(npcChange(int)));

	connect(cbSurface, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(cbHurt, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(cbDeath, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(cbSize, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(cbCat, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));

	connect(ui->sbHealth, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbExp, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbAttack, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));

	connect(ui->sbHitL, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbHitU, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbHitR, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbHitD, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));

	connect(ui->sbDisplayL, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbDisplayU, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbDisplayR, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbDisplayD, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));

	connect(ui->sbEditorLa, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbEditorUa, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbEditorRa, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbEditorDa, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbEditorXa, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbEditorYa, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));

	connect(ui->sbEditorL, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbEditorU, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbEditorR, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbEditorD, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbEditorX, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));
	connect(ui->sbEditorY, SIGNAL(valueChanged(int)), this, SLOT(valChange(int)));

	connect(ui->tbName, SIGNAL(textChanged(const QString &)), this, SLOT(valChange(const QString &)));
	connect(ui->tbTileset, SIGNAL(textChanged(const QString &)), this, SLOT(valChange(const QString &)));
	connect(ui->tbDescription, SIGNAL(textChanged()), this, SLOT(valChange()));

	connect(ui->flags, SIGNAL(valueChanged()), this, SLOT(valChange()));
}

void CdpNPCTBLEditor::accept() {
	target->npcs = localDataCopy;
	QDialog::accept();
}

void CdpNPCTBLEditor::regenNPCList() {
	ui->cbNPC->clear();
	int idx = 0;
	for (CdgNPCTBLEntry & npc : localDataCopy) {
		// NPCs here don't get icons because it'd be annoying.
		ui->cbNPC->addItem(QString::number(idx++) + QString(" - ") + npc.name);
	}
	ui->sbNPCCount->setValue(idx);
}

void CdpNPCTBLEditor::resizeNPCTable() {
	int targetValue =  ui->sbNPCCount->value();
	while (localDataCopy.size() > targetValue)
		localDataCopy.removeLast();
	while (localDataCopy.size() < targetValue)
		localDataCopy.append(CdgNPCTBLEntry());
	regenNPCList();
	npcChange(0);
}

void CdpNPCTBLEditor::updateVisualizer(const CdgNPCTBLEntry & npc) {
	QPixmap pxm;
	pxm = target->boundMdl->loadPBM(target->bkgPre + npc.sheet, true);
	ui->visualizer->setEntry(npc, &pxm);
}

void CdpNPCTBLEditor::npcChange(int) {
	const CdgNPCTBLEntry & npc = localDataCopy[ui->cbNPC->currentIndex()];

	inhibit = true;

	cbSurface->setValue(npc.surfaceId);
	cbHurt->setValue(npc.hurtSoundId);
	cbDeath->setValue(npc.deathSoundId);
	cbSize->setValue(npc.sizeId);
	cbCat->setValue(npc.category);

	ui->sbHealth->setValue(npc.health);
	ui->sbExp->setValue(npc.exp);
	ui->sbAttack->setValue(npc.damage);

	ui->sbHitL->setValue(npc.hitL);
	ui->sbHitU->setValue(npc.hitU);
	ui->sbHitR->setValue(npc.hitR);
	ui->sbHitD->setValue(npc.hitD);

	ui->sbDisplayL->setValue(npc.displayL);
	ui->sbDisplayU->setValue(npc.displayU);
	ui->sbDisplayR->setValue(npc.displayR);
	ui->sbDisplayD->setValue(npc.displayD);

	ui->sbEditorLa->setValue(npc.cfgA.spriteBounds.x());
	ui->sbEditorUa->setValue(npc.cfgA.spriteBounds.y());
	ui->sbEditorRa->setValue(npc.cfgA.spriteBounds.x() + npc.cfgA.spriteBounds.width());
	ui->sbEditorDa->setValue(npc.cfgA.spriteBounds.y() + npc.cfgA.spriteBounds.height());
	ui->sbEditorXa->setValue(npc.cfgA.nudge.x());
	ui->sbEditorYa->setValue(npc.cfgA.nudge.y());

	ui->sbEditorL->setValue(npc.cfgB.spriteBounds.x());
	ui->sbEditorU->setValue(npc.cfgB.spriteBounds.y());
	ui->sbEditorR->setValue(npc.cfgB.spriteBounds.x() + npc.cfgB.spriteBounds.width());
	ui->sbEditorD->setValue(npc.cfgB.spriteBounds.y() + npc.cfgB.spriteBounds.height());
	ui->sbEditorX->setValue(npc.cfgB.nudge.x());
	ui->sbEditorY->setValue(npc.cfgB.nudge.y());

	ui->tbName->setText(npc.name);
	ui->tbTileset->setText(npc.sheet);
	ui->tbDescription->setPlainText(npc.description);

	ui->flags->setValue(npc.flags);

	updateVisualizer(npc);
	inhibit = false;
}

void CdpNPCTBLEditor::valChange(int) {
	if (inhibit)
		return;
	CdgNPCTBLEntry & npc = localDataCopy[ui->cbNPC->currentIndex()];
	npc.extModified = true;

	npc.surfaceId = cbSurface->getValue();
	npc.hurtSoundId = cbHurt->getValue();
	npc.deathSoundId = cbDeath->getValue();
	npc.sizeId = cbSize->getValue();
	npc.category = cbCat->getValue();

	npc.health = ui->sbHealth->value();
	npc.exp = ui->sbExp->value();
	npc.damage = ui->sbAttack->value();

	npc.hitL = ui->sbHitL->value();
	npc.hitU = ui->sbHitU->value();
	npc.hitR = ui->sbHitR->value();
	npc.hitD = ui->sbHitD->value();

	npc.displayL = ui->sbDisplayL->value();
	npc.displayU = ui->sbDisplayU->value();
	npc.displayR = ui->sbDisplayR->value();
	npc.displayD = ui->sbDisplayD->value();

	npc.cfgA.spriteBounds = QRect(ui->sbEditorLa->value(), ui->sbEditorUa->value(), ui->sbEditorRa->value() - ui->sbEditorLa->value(), ui->sbEditorDa->value() - ui->sbEditorUa->value());
	npc.cfgA.nudge = QPoint(ui->sbEditorXa->value(), ui->sbEditorYa->value());

	npc.cfgB.spriteBounds = QRect(ui->sbEditorL->value(), ui->sbEditorU->value(), ui->sbEditorR->value() - ui->sbEditorL->value(), ui->sbEditorD->value() - ui->sbEditorU->value());
	npc.cfgB.nudge = QPoint(ui->sbEditorX->value(), ui->sbEditorY->value());

	updateVisualizer(npc);
}

void CdpNPCTBLEditor::valChange(const QString &) {
	if (inhibit)
		return;
	CdgNPCTBLEntry & npc = localDataCopy[ui->cbNPC->currentIndex()];
	npc.extModified = true;
	npc.name = ui->tbName->text();
	npc.sheet = ui->tbTileset->text();

	updateVisualizer(npc);
}

void CdpNPCTBLEditor::valChange() {
	if (inhibit)
		return;
	CdgNPCTBLEntry & npc = localDataCopy[ui->cbNPC->currentIndex()];
	npc.extModified = true;
	npc.description = ui->tbDescription->toPlainText();
	npc.flags = ui->flags->value();

	updateVisualizer(npc);
}

CdpNPCTBLEditor::~CdpNPCTBLEditor() {
	delete ui;
}
