#ifndef CDREGISTRY_H
#define CDREGISTRY_H

#include <QMap>

// We can't rely on specific construction order.
// We can rely on the contents of this class being zero on initialization.
// And we can rely on Qt being available.
template <typename R> struct CdRegistry {
private:
	QMap<QString, R> * map = nullptr;
	void ensureInit() {
		if (!map)
			map = new QMap<QString, R>();
	}
public:
	R get(const QString & key) {
		ensureInit();
		return (*map)[key];
	}
	// Returns whatever you put in solely for the fancy construction
	R put(const QString & key, R value) {
		ensureInit();
		(*map)[key] = value;
		return value;
	}
};

#endif // CDREGISTRY_H
