#ifndef CDUTILS_H
#define CDUTILS_H

#include <stdint.h>
#include <QFile>
#include <QByteArray>
#include <QTextCodec>
#include <QtDebug>

#define CDUTILS_BACKGROUND_COLOUR QColor(0, 0, 0)
#define CDUTILS_ENTITY_COLOUR QColor(0, 255, 0, 128)
#define CDUTILS_TILE_COLOUR QColor(128, 128, 128)
#define CDUTILS_INDICATOR_COLOUR QColor(255, 255, 0, 128)
#define CDUTILS_SELECTION_COLOUR QColor(128, 0, 128, 128)
#define CDUTILS_BORDER_COLOUR QColor(255, 255, 0, 255)
// NPC
#define CDUTILS_IMAGE_COLOUR QColor(0, 0, 255, 128)
#define CDUTILS_DISPLAY_COLOUR QColor(0, 255, 0, 128)
#define CDUTILS_HIT_COLOUR QColor(255, 0, 0, 128)

class CduBinary {
public:
	bool read;
	void u32(quint32 & num) { handle((char *) &num, 4, 4); }
	void u16(quint16 & num) { handle((char *) &num, 2, 2); }
	void u8(quint8 & num) { handle((char *) &num, 1, 1); }
	void u8cast(quint32 & num) { handle((char *) &num, 4, 1); }

	virtual void fixstr(QString & str, int sz) = 0;
	// Const if and only if a CduBinaryR.
	virtual void handle(char * data, size_t length, size_t fileLength) = 0;
};

class CduBinaryR : public CduBinary {
public:
	CduBinaryR(QIODevice * ba, QTextCodec * cod) : base(ba), codec(cod) { read = true; }
	QIODevice * base;
	QTextCodec * codec;
	void handle(char * data, size_t length, size_t fileLength) override;
	void fixstr(QString & str, int sz) override;
};

class CduBinaryW : public CduBinary {
public:
	CduBinaryW(QIODevice * ba, QTextCodec * cod) : base(ba), codec(cod) { read = false; }
	QIODevice * base;
	QTextCodec * codec;
	void handle(char * data, size_t length, size_t fileLength) override;
	void fixstr(QString & str, int sz) override;
};

// IO Functions...
void cdutils_save_fixstr(QIODevice * file, QTextCodec * cod, const QString & text, int sz);
QString cdutils_load_fixstr(QTextCodec * cod, const QByteArray & data);

void cdutils_save_u32(QIODevice * file, uint32_t num);
#define cdutils_save_i32(file, num) cdutils_save_u32(file, (uint32_t) (num))
void cdutils_save_u16(QIODevice * file, uint16_t num);
#define cdutils_save_i16(file, num) cdutils_save_u16(file, (uint16_t) (num))
void cdutils_save_u8(QIODevice * file, uint8_t num);
#define cdutils_save_i8(file, num) cdutils_save_u8(file, (uint8_t) (num))

uint32_t cdutils_load_u32(const QByteArray & data);
#define cdutils_load_i32(data) ((int32_t) cdutils_load_u32(data))
uint16_t cdutils_load_u16(const QByteArray & data);
#define cdutils_load_i16(data) ((int16_t) cdutils_load_u16(data))

inline uint32_t cdutils_load_u32(QIODevice * file) {
	return cdutils_load_u32(file->read(4));
}

inline uint16_t cdutils_load_u16(QIODevice * file) {
	return cdutils_load_u16(file->read(2));
}

inline uint8_t cdutils_load_u8(QIODevice * file) {
	return file->read(1)[0];
}

inline int cdutils_celldiv(int i, int d) {
	if (i < 0) {
		// Outer -1 makes it so that -1 goes from -15 to 15
		// The +1 then shifts that to -16 to 14.
		return ((i + 1) / d) - 1;
	}
	return i / d;
}
inline int cdutils_celldivceil(int i, int d) {
	return cdutils_celldiv(i + (d - 1), d);
}
inline int cdutils_cellmod(int i, int d) {
	if (i < 0)
		return i - (cdutils_celldiv(i, d) * d);
	return i % d;
}

#endif // CDUTILS_H
