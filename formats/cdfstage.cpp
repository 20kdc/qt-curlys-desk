#include "cdfstage.h"
#include "cdutils.h"

void CdfStage::read(CduBinary & codec, int format) {
	if (format == CDFSTAGE_FORMAT_CS_BASE) {
		// 0xC8 bytes
		// THIS IS USED IN EXE WRITING
		codec.fixstr(tileset, 0x20);
		codec.fixstr(mapId, 0x20);
		codec.u32(bkgType);
		codec.fixstr(bkg, 0x20);
		codec.fixstr(npc1Tile, 0x20);
		codec.fixstr(npc2Tile, 0x20);
		codec.u8(bossType);
		codec.fixstr(mnaPrimary, 0x23);
		if (codec.read)
			mnaSecondary = mnaPrimary;
	} else if (format == CDFSTAGE_FORMAT_CSE2_EX) {
		// 0xE5 bytes
		codec.fixstr(tileset, 0x20);
		codec.fixstr(mapId, 0x20);
		codec.u32(bkgType);
		codec.fixstr(bkg, 0x20);
		codec.fixstr(npc1Tile, 0x20);
		codec.fixstr(npc2Tile, 0x20);
		codec.u8(bossType);
		codec.fixstr(mnaPrimary, 0x20);
		codec.fixstr(mnaSecondary, 0x20);
	} else if (format == CDFSTAGE_FORMAT_MRMAP) {
		// 0x74 bytes
		codec.fixstr(tileset, 0x10);
		codec.fixstr(mapId, 0x10);
		codec.u8cast(bkgType);
		codec.fixstr(bkg, 0x10);
		codec.fixstr(npc1Tile, 0x10);
		codec.fixstr(npc2Tile, 0x10);
		codec.u8(bossType);
		codec.fixstr(mnaPrimary, 0x22);
		if (codec.read)
			mnaSecondary = mnaPrimary;
	}
}
