#ifndef CDFSTAGE_H
#define CDFSTAGE_H

#include <QString>
#include "cdutils.h"

// These constants match up to values in CdgExtStageMdl!
// Please keep that in mind.
#define CDFSTAGE_FORMAT_UNKNOWN 0
#define CDFSTAGE_FORMAT_CS_BASE 1
// Despite the name, doukutsu-rs says this is actually CS+ & Booster's Lab format
// Wonder if this is why CSE2 got DMCA'd
#define CDFSTAGE_FORMAT_CSE2_EX 2
// The best part is, this might not even be real mrmap, it's doukutsu-rs mrmap (or maybe cse2e?) which MIGHT be different (???)
// there seems to be inconsistencies in the available documentation
// I'm trusting what's in doukutsu-rs data for now
#define CDFSTAGE_FORMAT_MRMAP   3

// This is a stage table entry.
class CdfStage {
public:
	// In CSE2 order.
	QString tileset = "Cave";
	QString mapId = "__FIXME__";
	quint32 bkgType = 0;
	QString bkg = "bkBlue";
	QString npc1Tile = "Guest";
	QString npc2Tile = "0";
	quint8 bossType = 0;
	QString mnaPrimary = "Map";
	QString mnaSecondary = "Map";

	void read(CduBinary &, int format);
	void write(CduBinaryW & cb, int format) const {
		// This is intentional!
		((CdfStage *) this)->read(cb, format);
	}
};

#endif // CDFSTAGE_H
