#ifndef CDEXCEPT_H
#define CDEXCEPT_H

#include <QString>
#include <stdio.h>
#include <stdlib.h>

class CdExceptionTracker {
public:
	CdExceptionTracker(const char * loc) : location(loc) {
	}
	CdExceptionTracker(const char * loc, CdExceptionTracker & par) : location(loc), parent(&par) {
	}

	void report(const QString & base) {
		if (!error) {
			error = true;
			errorText = base;
		} else {
			errorText += QString("\n") + base;
		}
	}

	bool __attribute__((warn_unused_result)) check() {
		armed = false;
		return error;
	}

	bool __attribute__((warn_unused_result)) poll() {
		return error;
	}

	~CdExceptionTracker() {
		if (parent) {
			if (error)
				parent->report(QLatin1String(location) + QString("\n ") + errorText.replace("\n", "\n "));
		} else {
			if (armed) {
				puts("PANIC: CdExceptionTracker was destroyed without defusal.");
				printf("Location: %s\n", location);
				exit(1);
			}
		}
	}

	const char * location;
	QString errorText;
private:
	bool error = false;
	bool armed = true;
	CdExceptionTracker * parent = nullptr;
};

#define CD_THROW(text) {ex.report(text); return;}
#define CD_POLL {if (ex.poll()) return;}
#define CD_DERIVE_EXCEPTIONS CdExceptionTracker ex(typeid(this).name(), exB)
#endif // CDEXCEPT_H
