QT += widgets
CONFIG += c++11 console
CONFIG -= app_bundle

# IF YOU GET INCLUDE ISSUES WITH STDDEF ON UBUNTU, PLEASE SEE: https://bugs.launchpad.net/ubuntu/+source/qtcreator/+bug/1910551

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    formats/cdfstage.cpp \
        main.cpp \
    models/cdgenumglobalmdl.cpp \
    models/language/cdgcreditslanguagemdl.cpp \
    models/language/cdgtsclanguagemdl.cpp \
    models/language/cdgtsclanguagemdl_data.cpp \
    models/language/cdkbaselanguagemdl.cpp \
    models/exe/cdeexeconstants.cpp \
    models/cdgexeglobalmdl.cpp \
    models/cdgnpctblglobalmdl.cpp \
    models/cdgnpctblglobalmdl_data.cpp \
    models/cdkbasemdl.cpp \
    models/cdkbasestagemdl.cpp \
    models/cdkbasevisiblemdl.cpp \
    models/exe/cdestageupdate.cpp \
    pieces/cdpcollisionimage.cpp \
    pieces/cdpenumcontroller.cpp \
    pieces/cdpflageditor.cpp \
    pieces/cdpnpctbleditor.cpp \
    pieces/cdpnpctblentryviewer.cpp \
    pieces/cdptscnumerics.cpp \
    tools/cdtshiftmaptool.cpp \
    uis/cdiabout.cpp \
    uis/cdicurlysdesk.cpp \
    models/cdkgamemdl.cpp \
    models/cdgextstagemdl.cpp \
    uis/cdifindiddialog.cpp \
    uis/cdiflageditordialog.cpp \
    uis/cdimapeditor.cpp \
    uis/cdistbleditor.cpp \
    uis/cdimapview.cpp \
    models/cdgcsmapmdl.cpp \
    models/cdkbasemapmdl.cpp \
    cdutils.cpp \
    uis/cditileselection.cpp \
    tools/cdtmaptoolbase.cpp \
    tools/cdttilestool.cpp \
    tools/cdtentitytool.cpp \
    tools/cdtentitymodtool.cpp \
    pieces/cdptsceditor.cpp \
    uis/cditileselectionmts.cpp \
    uis/cditileselectioncol.cpp \
    pieces/cdpcollisioneditor.cpp \
    models/cdkbaseglobalmdl.cpp \
    models/cdgtscglobalmdl.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    cdcategory.h \
    cdexcept.h \
    cdregistry.h \
    cdpnpctblentryviewer.h \
    cdpflageditor.h \
    cdptscnumerics.h \
    formats/cdfstage.h \
    models/cdgenumglobalmdl.h \
    models/language/cdgcreditslanguagemdl.h \
    models/language/cdgtsclanguagemdl.h \
    models/language/cdkbaselanguagemdl.h \
    models/exe/cdeexeconstants.h \
    models/cdgexeglobalmdl.h \
    models/cdgnpctblglobalmdl.h \
    models/cdkbasemdl.h \
    models/cdkbasestagemdl.h \
    models/cdkbasevisiblemdl.h \
    models/exe/cdestageupdate.h \
    pieces/cdpcollisionimage.h \
    pieces/cdpenumcontroller.h \
    pieces/cdpflageditor.h \
    pieces/cdpnpctbleditor.h \
    pieces/cdpnpctblentryviewer.h \
    pieces/cdptscnumerics.h \
    tools/cdtshiftmaptool.h \
    uis/cdiabout.h \
    uis/cdicurlysdesk.h \
    models/cdkgamemdl.h \
    models/cdgextstagemdl.h \
    uis/cdifindiddialog.h \
    uis/cdiflageditordialog.h \
    uis/cdimapeditor.h \
    uis/cdistbleditor.h \
    uis/cdimapview.h \
    models/cdgcsmapmdl.h \
    models/cdkbasemapmdl.h \
    cdutils.h \
    uis/cditileselection.h \
    tools/cdtmaptoolbase.h \
    tools/cdttilestool.h \
    cdimapview.h \
    tools/cdtentitytool.h \
    tools/cdtentitymodtool.h \
    pieces/cdptsceditor.h \
    uis/cditileselectionmts.h \
    uis/cditileselectioncol.h \
    pieces/cdpcollisioneditor.h \
    models/cdkbaseglobalmdl.h \
    models/cdgtscglobalmdl.h

FORMS += \
    pieces/cdpflageditor.ui \
    pieces/cdpnpctbleditor.ui \
    pieces/cdptscnumerics.ui \
    uis/cdiabout.ui \
    uis/cdicurlysdesk.ui \
    uis/cdifindiddialog.ui \
    uis/cdiflageditordialog.ui \
    uis/cdimapeditor.ui \
    uis/cdistbleditor.ui \
    tools/cdttilestool.ui \
    tools/cdtentitymodtool.ui \
    pieces/cdptsceditor.ui \
    pieces/cdpcollisioneditor.ui

# QExe into build tree

INCLUDEPATH += thirdparty/QExe

SOURCES += \
    thirdparty/QExe/qexe.cpp \
    thirdparty/QExe/qexecoffheader.cpp \
    thirdparty/QExe/qexedosstub.cpp \
    thirdparty/QExe/qexeoptionalheader.cpp \
    thirdparty/QExe/qexersrcentry.cpp \
    thirdparty/QExe/qexersrcmanager.cpp \
    thirdparty/QExe/qexesection.cpp \
    thirdparty/QExe/qexesectionmanager.cpp

HEADERS += \
    thirdparty/QExe/QExe_global.h \
    thirdparty/QExe/qexe.h \
    thirdparty/QExe/qexecoffheader.h \
    thirdparty/QExe/qexedosstub.h \
    thirdparty/QExe/qexeerrorinfo.h \
    thirdparty/QExe/qexeoptionalheader.h \
    thirdparty/QExe/qexersrcentry.h \
    thirdparty/QExe/qexersrcmanager.h \
    thirdparty/QExe/qexesection.h \
    thirdparty/QExe/qexesectionmanager.h \
    thirdparty/QExe/typedef_version.h

