#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QTextCodec>
#include <QImage>
#include <QRgb>
#include <QVector>
#include <QDir>
#include <QMessageBox>
#include <QtDebug>
#include "models/cdkgamemdl.h"
#include "models/cdgextstagemdl.h"
#include "models/cdkbasestagemdl.h"
#include "models/cdgcsmapmdl.h"
#include "models/cdgnpctblglobalmdl.h"
#include "models/cdgtscglobalmdl.h"
#include "models/cdgexeglobalmdl.h"
#include "models/cdgenumglobalmdl.h"
#include "models/language/cdgtsclanguagemdl.h"
#include "models/language/cdgcreditslanguagemdl.h"

CdRegistry<cdkglobal_loader_t> cdkGlobalRegistry;

static void cdkgamemdl_cleanup(CdkGameMdl * obj) {
	obj->stageModel = nullptr;
	int l = obj->gModels.length();
	for (int i = 0; i < l; i++)
		delete obj->gModels[i];
	obj->gModels.clear();
}

CdkGameMdl::CdkGameMdl(CdExceptionTracker & ex, const QString & prefix, const QString & executable) {
	fullPfx = prefix;
	QFile config(getPath("cdkgamemdl.json"));
	// As 'new' occurs, catch blocks must ensure that any otherwise leaked objects are deleted.
	if (config.open(QFile::ReadOnly)) {
		QJsonDocument document = QJsonDocument::fromJson(config.readAll());
		config.close();
		QJsonObject jobj = document.object();
		// -- Codec --
		if (jobj["gameCodec"].type() != QJsonValue::String)
			throw QString("No gameCodec specified.");
		gameCodec = QTextCodec::codecForName(jobj["gameCodec"].toString().toUtf8());
		if (!gameCodec)
			CD_THROW("gameCodec was invalid.");
		// -- Stages --

		pxaPre = jobj["pxaPre"].toString();
		pxaPost = jobj["pxaPost"].toString();
		pxmPre = jobj["pxmPre"].toString();
		pxmPost = jobj["pxmPost"].toString();
		pxePre = jobj["pxePre"].toString();
		pxePost = jobj["pxePost"].toString();

		mtscPre = jobj["mtscPre"].toString();
		mtscPost = jobj["mtscPost"].toString();

		prtPre = jobj["prtPre"].toString();
		pbmPost = jobj["pbmPost"].toString();

		pbmFmt = jobj["pbmFmt"].toString();

		QJsonArray gameRunArgsJ = jobj["gameRunArgs"].toArray();
		for (QJsonValueRef ref : gameRunArgsJ)
			gameRunArgs.append(ref.toString());

		QJsonArray tsc = jobj["globals"].toArray();
		for (QJsonValueRef ref : tsc) {
			if (!ref.isObject())
				CD_THROW("Elements of 'globals' array must be objects.");
			QJsonObject qjo = ref.toObject();
			auto type = qjo["type"].toString();
			auto loader = cdkGlobalRegistry.get(type);
			if (loader != nullptr) {
				CdkBaseGlobalMdl * global = nullptr;
				try {
					global = loader(this, qjo);
				} catch (QString qs) {
					ex.report(QString("During global '%1': %2").arg(type).arg(qs));
				} catch (...) {
					ex.report(QString("Unknown exception during global '%1': %2").arg(type));
				}
				if (global != nullptr) {
					gModels.append(global);
					auto asStageMdl = dynamic_cast<CdkBaseStageMdl *>(global);
					if (asStageMdl != nullptr) {
						if (stageModel != nullptr)
							ex.report("Attempted to install >1 stage model");
						stageModel = asStageMdl;
					}
				}
			} else {
				ex.report("Unknown global type \"" + type + "\".");
			}
		}

		tileSize = jobj["tileSize"].toInt();
		scale = jobj["scale"].toInt();
	} else {
		try {
			// Set things up assuming standard Cave Story. Autodetect any obvious deviations.
			gameCodec = QTextCodec::codecForName("Shift-JIS");

			// Generic enum model that really doesn't need removing
			gModels.append(new CdgEnumGlobalMdl(this, QJsonObject()));

			// First of all, prepare the EXE global model if any.
			CdgEXEGlobalMdl * exeMdl = nullptr;
			QString doukutsuEXEPath = getPath("Doukutsu.exe");
			QString doukutsuBaseEXEPath = getPath("Doukutsu.base.exe");
			if (QFile::exists(doukutsuEXEPath)) {
				// Doukutsu exe mode.
				// Start by making a backup if there isn't one already (which would suggest the target has already been modified).
				if (!QFile::exists(doukutsuBaseEXEPath))
					QFile::copy(doukutsuEXEPath, doukutsuBaseEXEPath);
				// Continue by setting up the EXE model.
				gModels.append(exeMdl = new CdgEXEGlobalMdl(this, "Doukutsu.exe", "Doukutsu.base.exe"));
			}

			// Prepare run
			gameRunArgs.append(executable);
#ifndef WINDOWS
			if (executable.endsWith(".exe")) {
				gameRunArgs.clear();
				gameRunArgs.append("wine");
				gameRunArgs.append(executable);
			}
#endif

			gModels.append(new CdgNPCTBLGlobalMdl(this, "data/npc.tbl", QJsonObject()));

			// Logic here is similar to doukutsu-rs, just cleaner
			// Also note, it doesn't support NXEngine stage.dat
			// Why? Because NXEngine uses a sort of "compressed" format with internal tables
			if (QFile::exists(getPath("data/stage.tbl"))) {
				// CS+
				stageModel = new CdgExtStageMdl(this, "data/stage.tbl", CDFSTAGE_FORMAT_CSE2_EX);
			} else if (QFile::exists(getPath("data/stage.sect"))) {
				// CS extraction
				stageModel = new CdgExtStageMdl(this, "data/stage.sect", CDFSTAGE_FORMAT_CS_BASE);
			} else if (QFile::exists(getPath("data/mrmap.bin"))) {
				// mrmap.bin
				// supposed to be Noxid's engine, but doukutsu-rs uses this as well.
				stageModel = new CdgExtStageMdl(this, "data/mrmap.bin", CDFSTAGE_FORMAT_MRMAP);
			} else {
				// Need to extract.
				if (exeMdl == nullptr)
					throw QString("There is no stage.tbl and no Doukutsu.exe!");
				exeMdl->extractStages(getPath("data/stage.sect"));
				stageModel = new CdgExtStageMdl(this, "data/stage.sect", CDFSTAGE_FORMAT_CS_BASE);
			}
			gModels.append(stageModel);

			pxaPre = "data/Stage/";
			pxaPost = ".pxa";
			pxmPre = "data/Stage/";
			pxmPost = ".pxm";
			pxePre = "data/Stage/";
			pxePost = ".pxe";

			mtscPre = "data/Stage/";
			mtscPost = ".tsc";

			prtPre = "data/Stage/Prt";
			pbmPost = ".pbm";

			pbmFmt = "BMP";

			// Check for .pbm -> .bmp or .png
			if (QFile::exists(getPath("data/Loading.bmp"))) {
				pbmPost = ".bmp";
			} else if (QFile::exists(getPath("data/Loading.png"))) {
				pbmPost = ".png";
				pbmFmt = "PNG";
			}

			gModels.append(new CdgTSCLanguageMdl(this, QJsonObject()));
			gModels.append(new CdgCreditsLanguageMdl(this));

			gModels.append(new CdgTSCGlobalMdl(this, "data/Head.tsc", "tsc"));
			gModels.append(new CdgTSCGlobalMdl(this, "data/StageSelect.tsc", "tsc"));
			gModels.append(new CdgTSCGlobalMdl(this, "data/ArmsItem.tsc", "tsc"));
			gModels.append(new CdgTSCGlobalMdl(this, "data/Credit.tsc", "credits"));

			tileSize = 16;
			// Attempt to determine game resolution. 'Face.pbm' is expected to have a width of 288.
			// If it's 576, then this is 2x-modded.
			QPixmap qi = loadPBM("data/Face", false);
			if (qi.width() == 576) {
				tileSize = 32;
				scale = 1;
			}
		} catch (QString qs) {
			CD_THROW(QString("During init: %1").arg(qs));
		} catch (...) {
			CD_THROW("Unknown exception during init");
		}
	}
	// Final init
	if (stageModel == nullptr)
		CD_THROW("There's no stage model.");
	stages = stageModel->load(ex);
}

CdkGameMdl::~CdkGameMdl() {
	cdkgamemdl_cleanup(this);
}

CdkBaseMapMdl * CdkGameMdl::openMap(int idx) {
	return new CdgCSMapMdl(this, idx);
}

QPixmap CdkGameMdl::loadPBM(const QString & id, bool transparent) {
	QByteArray qba = pbmFmt.toUtf8();
	QString fullPath = getPath(id + pbmPost);
	QImage qi = QImage(fullPath, qba);
	if (qi.isNull())
		qWarning() << "missing image" << id << "as" << fullPath;
	if (transparent) {
		QImage img = QImage(qi.size(), QImage::Format_ARGB32);
		for (int i = 0; i < img.size().width(); i++) {
			for (int j = 0; j < img.size().height(); j++) {
				QColor c = qi.pixel(i, j);
				if ((c.red() == 0) && (c.green() == 0) && (c.blue() == 0)) {
					img.setPixelColor(i, j, QColor(0, 0, 0, 0));
				} else {
					img.setPixelColor(i, j, c);
				}
			}
		}
		qi = img;
		if (qi.colorCount() > 0)
			qi.setColor(0, qRgba(0, 0, 0, 0));
	}
	QPixmap result;
	result.convertFromImage(qi);
	return result;
}

QString CdkGameMdl::getPath(const QString & path) {
	// Case insensitivity
	QString soFar = fullPfx;
	bool needsAppendSlash = false;
	for (QString component : path.split("/")) {
		if (needsAppendSlash)
			soFar += "/";
		needsAppendSlash = true;
		QString result = soFar + component;
		if (!QFile::exists(result)) {
			QString folded = component.toCaseFolded();
			QDir dir(soFar);
			for (QString qs : dir.entryList()) {
				if (qs.toCaseFolded() == folded) {
					result = soFar + qs;
					break;
				}
			}
		}
		soFar = result;
	}
	return soFar;
}

void CdkGameMdl::save(CdExceptionTracker & ex) {
	QJsonObject jobj;
	{
		jobj["gameCodec"] = QString::fromUtf8(gameCodec->name());
		jobj["pxaPre"] = pxaPre;
		jobj["pxaPost"] = pxaPost;
		jobj["pxmPre"] = pxmPre;
		jobj["pxmPost"] = pxmPost;
		jobj["pxePre"] = pxePre;
		jobj["pxePost"] = pxePost;

		jobj["mtscPre"] = mtscPre;
		jobj["mtscPost"] = mtscPost;

		jobj["prtPre"] = prtPre;
		jobj["pbmPost"] = pbmPost;

		jobj["pbmFmt"] = pbmFmt;

		QJsonArray qjaArgs;
		for (const QString & qs : gameRunArgs)
			qjaArgs.append(qs);
		jobj["gameRunArgs"] = qjaArgs;

		QJsonArray qja;
		for (CdkBaseGlobalMdl * qs : gModels)
			qja.append(qs->storeCfg());
		jobj["globals"] = qja;

		jobj["tileSize"] = tileSize;
		jobj["scale"] = scale;
	}
	QFile config(getPath("cdkgamemdl.json"));
	if (config.open(QFile::WriteOnly)) {
		QJsonDocument document;
		document.setObject(jobj);
		config.write(document.toJson());
		config.close();
	} else {
		throw QString("Unable to save CdkGameMdl JSON");
	}
	int l = gModels.length();
	for (int i = 0; i < l; i++)
		gModels[i]->saveGlobal(ex);
}

bool CdkGameMdl::terminateTracker(CdExceptionTracker & ex, QWidget * parent) {
	if (ex.check()) {
		QMessageBox::information(parent, "Error", QLatin1String(ex.location) + QLatin1String("\n") + ex.errorText);
		return true;
	}
	return false;
}
