#include "cdgextstagemdl.h"
#include "cdutils.h"
#include "cdkgamemdl.h"

CDKGAMEMDL_REGISTER_GLOBAL(stblUnknown) {
	if (data["file"].type() != QJsonValue::String)
		throw QString("No stageMdl->file specified.");
	return new CdgExtStageMdl(mdl, data["file"].toString(), CDFSTAGE_FORMAT_UNKNOWN);
}

CDKGAMEMDL_REGISTER_GLOBAL(stblCSBase) {
	if (data["file"].type() != QJsonValue::String)
		throw QString("No stageMdl->file specified.");
	return new CdgExtStageMdl(mdl, data["file"].toString(), CDFSTAGE_FORMAT_CS_BASE);
}

CDKGAMEMDL_REGISTER_GLOBAL(stblCSE2EX) {
	if (data["file"].type() != QJsonValue::String)
		throw QString("No stageMdl->file specified.");
	return new CdgExtStageMdl(mdl, data["file"].toString(), CDFSTAGE_FORMAT_CSE2_EX);
}

CDKGAMEMDL_REGISTER_GLOBAL(stblMRMAP) {
	if (data["file"].type() != QJsonValue::String)
		throw QString("No stageMdl->file specified.");
	return new CdgExtStageMdl(mdl, data["file"].toString(), CDFSTAGE_FORMAT_MRMAP);
}

CdgExtStageMdl::CdgExtStageMdl(CdkGameMdl * gameMdl, const QString & filename, int mode) : CdkBaseStageMdl(gameMdl), stbl(filename) {
	codec = gameMdl->gameCodec;
	fileAccess = new QFile(gameMdl->getPath(filename));
	if (mode == CDFSTAGE_FORMAT_UNKNOWN) {
		if (fileAccess->open(QFile::ReadOnly)) {
			QByteArray qba = fileAccess->readAll();
			int l = qba.length();
			bool ori = (l % 0xC5) == 0;
			bool cse = (l % 0xE5) == 0;
			if (ori && cse) {
				if (l >= (0xE5 * 2)) {
					// A less reliable method is to check field alignment.
					// Not all fields in the file are 20-byte, and removing/adding a 20-byte field will shift the position of the non-20-byte fields.
					// The byte at 0x41 is a part of bkType (0x40-0x43), but bkType has a very fixed range of values.
					// None of these values are above 255 or negative, but for ASM safety reasons 255 is considered on-alignment.
					char tst1 = qba[0xC5 + 0x41];
					char tst2 = qba[0xE5 + 0x41];
					ori = (tst1 == -1) || (tst1 == 0);
					cse = (tst2 == -1) || (tst2 == 0);
				}
			}
			if (cse) {
				// If both are valid, assume CSE as it's the most likely.
				mode = CDFSTAGE_FORMAT_CSE2_EX;
			} else if (ori) {
				mode = CDFSTAGE_FORMAT_CS_BASE;
			}
			fileAccess->close();
		}
	}
	this->mode = mode;
}
CdgExtStageMdl::~CdgExtStageMdl() {
	// The passed QTextCodec is not owned here.
	delete fileAccess;
}

bool CdgExtStageMdl::supportsMNA2() {
	return mode == CDFSTAGE_FORMAT_CSE2_EX;
}

QJsonObject CdgExtStageMdl::storeCfg() {
	QJsonObject cfg;
	if (mode == CDFSTAGE_FORMAT_UNKNOWN)
		cfg["type"] = "stblUnknown";
	else if (mode == CDFSTAGE_FORMAT_CS_BASE)
		cfg["type"] = "stblCSBase";
	else if (mode == CDFSTAGE_FORMAT_CSE2_EX)
		cfg["type"] = "stblCSE2EX";
	else if (mode == CDFSTAGE_FORMAT_MRMAP)
		cfg["type"] = "stblMRMAP";
	else
		cfg["type"] = "stblUNKNOWN_PLEASE_REPORT_THIS_AS_A_BUG_" + QString::number(mode);
	cfg["file"] = stbl;
	return cfg;
}

QList<CdfStage> CdgExtStageMdl::load(CdExceptionTracker & ex) {
	QList<CdfStage> stage;
	if (mode == CDFSTAGE_FORMAT_UNKNOWN) {
		ex.report("Unable to detect mode.");
		return stage;
	}
	if (fileAccess->open(QFile::ReadOnly)) {
		CduBinaryR reader(fileAccess, boundMdl->gameCodec);
		while (!fileAccess->atEnd()) {
			CdfStage cs;
			cs.read(reader, mode);
			stage.append(cs);
		}
		fileAccess->close();
	} else {
		ex.report("Unable to load stage table.");
	}
	return stage;
}

void CdgExtStageMdl::saveGlobal(CdExceptionTracker & ex) {
	if (mode == CDFSTAGE_FORMAT_UNKNOWN)
		CD_THROW("Unknown stage format.");
	if (!fileAccess->open(QFile::WriteOnly))
		CD_THROW("Unable to open stage table");

	CduBinaryW writer(fileAccess, boundMdl->gameCodec);
	for (const CdfStage & cs : boundMdl->stages)
		cs.write(writer, mode);

	fileAccess->close();
}
