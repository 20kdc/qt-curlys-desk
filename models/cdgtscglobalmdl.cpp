#include "models/cdgtscglobalmdl.h"
#include "models/cdkgamemdl.h"
#include "pieces/cdptsceditor.h"
#include "models/language/cdkbaselanguagemdl.h"

CDKGAMEMDL_REGISTER_GLOBAL(textFile) {
	if (data["file"].type() != QJsonValue::String)
		throw QString("No TSC file specified in entry.");
	if (data["language"].type() != QJsonValue::String)
		throw QString("No language specified in entry.");
	return new CdgTSCGlobalMdl(mdl, data["file"].toString(),  data["language"].toString());
}

CdgTSCGlobalMdl::CdgTSCGlobalMdl(CdkGameMdl * mdl, const QString & fn, const QString & ln) : CdkBaseVisibleMdl(mdl), filename(fn), language(ln), editor(nullptr) {
	setObjectName(fn);
}

void CdgTSCGlobalMdl::saveGlobal(CdExceptionTracker & ex) {
	if (editor)
		editor->save(ex);
}

QJsonObject CdgTSCGlobalMdl::storeCfg() {
	QJsonObject cfg;
	cfg["type"] = "textFile";
	cfg["language"] = language;
	cfg["file"] = filename;
	return cfg;
}

void CdgTSCGlobalMdl::show() {
	if (!editor) {
		CdkBaseLanguageMdl * baseMdl = CdkBaseLanguageMdl::attemptAcquireByName(boundMdl, language);
		editor = new CdpTSCEditor(boundMdl->theOneTrueRoot, boundMdl->gameCodec, baseMdl, boundMdl->getPath(filename));
	}
	editor->show();
}
