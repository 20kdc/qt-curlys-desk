#include "cdgenumglobalmdl.h"
#include "cdkgamemdl.h"
#include "cdcategory.h"
#include <algorithm>

CDKGAMEMDL_REGISTER_GLOBAL(enums) {
	return new CdgEnumGlobalMdl(mdl, data["enums"].toObject());
}

QMap<int, QString> & cdgenumglobalmdl_initializeBaseEnum(CdgEnumGlobalMdl * mdl, const QString & key) {
	mdl->enums[key] = QMap<int, QString>();
	return mdl->enums[key];
}

CdgEnumGlobalMdl::CdgEnumGlobalMdl(CdkGameMdl * base, const QJsonObject & evals) : CdkBaseGlobalMdl(base) {
	// -- Base Enums --
	QMap<int, QString> & eScrollTypes = cdgenumglobalmdl_initializeBaseEnum(this, "scrollType");
	eScrollTypes[0] = "Screen-Stationary Repeated";
	eScrollTypes[1] = "Parallax 0.5x Repeated";
	eScrollTypes[2] = "Level-Stationary Repeated";
	eScrollTypes[3] = "Water";
	eScrollTypes[4] = "Black";
	eScrollTypes[5] = "Autoscroll";
	eScrollTypes[6] = "Outer Wall (Balcony but pushes pickups west)";
	eScrollTypes[7] = "Balcony (Split into sections @ Y 88, 123, 146, 176)";
	QMap<int, QString> & eSurfaces = cdgenumglobalmdl_initializeBaseEnum(this, "surface");
	eSurfaces[0] = "data/Title";
	eSurfaces[1] = "data/pixel";
	eSurfaces[2] = "MAP_TILESET";
	eSurfaces[6] = "data/Fade";
	eSurfaces[8] = "data/ItemImage";
	eSurfaces[11] = "data/Arms";
	eSurfaces[12] = "data/ArmsImage";
	eSurfaces[14] = "data/StageImage";
	eSurfaces[15] = "data/Loading";
	eSurfaces[16] = "data/MyChar";
	eSurfaces[17] = "data/Bullet";
	eSurfaces[19] = "data/Caret";
	eSurfaces[20] = "data/Npc/NpcSym";
	eSurfaces[21] = "MAP_NPC1";
	eSurfaces[22] = "MAP_NPC2";
	eSurfaces[23] = "data/Npc/NpcRegu";
	eSurfaces[26] = "data/TextBox";
	eSurfaces[27] = "data/Face";
	eSurfaces[28] = "MAP_BACKGROUND";
	QMap<int, QString> & eBoss = cdgenumglobalmdl_initializeBaseEnum(this, "boss");
	eBoss[0] = "No Boss";
	eBoss[1] = "Sand Zone / Omega";
	eBoss[2] = "Gum / Balfrog";
	eBoss[3] = "Labyrinth W / Monster X";
	eBoss[4] = "Core / Core";
	eBoss[5] = "Main Artery / Ironhead";
	eBoss[6] = "Egg Observation Room? / The Twins";
	eBoss[7] = "Black Space / Undead Core";
	eBoss[8] = "Sacred Ground - B3 / Heavy Press";
	eBoss[9] = "Seal Chamber / Ballos";
	QMap<int, QString> & eSounds = cdgenumglobalmdl_initializeBaseEnum(this, "sound");
	eSounds[0] = "No Sound";
	QMap<int, QString> & eSize = cdgenumglobalmdl_initializeBaseEnum(this, "size");
	eSize[0] = "Nothing";
	eSize[1] = "Small Cloud";
	eSize[2] = "Medium Cloud";
	eSize[3] = "Large Cloud";
	QMap<int, QString> & eDirection = cdgenumglobalmdl_initializeBaseEnum(this, "direction");
	eDirection[0] = "Left";
	eDirection[1] = "Up";
	eDirection[2] = "Right";
	eDirection[3] = "Down";
	eDirection[4] = "Towards Player";
	QMap<int, QString> & eFadeMode = cdgenumglobalmdl_initializeBaseEnum(this, "fadeMode");
	eFadeMode[0] = "Left";
	eFadeMode[1] = "Up";
	eFadeMode[2] = "Right";
	eFadeMode[3] = "Down";
	eFadeMode[4] = "Centre";
	QMap<int, QString> & eMusic = cdgenumglobalmdl_initializeBaseEnum(this, "music");
	eMusic[ 0] = "xxxx - None";
	eMusic[ 1] = "wanpaku - Mischievous Robot";
	eMusic[ 2] = "anzen - Safety";
	eMusic[ 3] = "gameover - Game Over";
	eMusic[ 4] = "gravity - Gravity";
	eMusic[ 5] = "weed - On To Grasstown";
	eMusic[ 6] = "mdown2 - Meltdown 2";
	eMusic[ 7] = "fireeye - Eyes Of Flame";
	eMusic[ 8] = "vivi - Gestation";
	eMusic[ 9] = "mura - Mimiga Town";
	eMusic[10] = "fanfale1 - Got Item";
	eMusic[11] = "ginsuke - Balrog's Theme";
	eMusic[12] = "cemetery - Cemetery";
	eMusic[13] = "plant - Plant";
	eMusic[14] = "kodou - Pulse";
	eMusic[15] = "fanfale3 - Victory";
	eMusic[16] = "fanfale2 - Get Heart Tank";
	eMusic[17] = "dr - Tyrant";
	eMusic[18] = "escape - Run!";
	eMusic[19] = "jenka - Jenka 1";
	eMusic[20] = "maze - Labyrinth Fight";
	eMusic[21] = "access - Access";
	eMusic[22] = "ironh - Oppression";
	eMusic[23] = "grand - Geothermal";
	eMusic[24] = "curly - Cave Story";
	eMusic[25] = "oside - Moonsong";
	eMusic[26] = "requiem - Hero's End";
	eMusic[27] = "wanpak2 - Scorching Back";
	eMusic[28] = "quiet - Quiet";
	eMusic[29] = "lastcave - Last Cave";
	eMusic[30] = "balcony - Balcony";
	eMusic[31] = "lastbtl - Charge";
	eMusic[32] = "lastbt3 - Last Battle";
	eMusic[33] = "ending - The Way Back Home";
	eMusic[34] = "zonbie - Zombie";
	eMusic[35] = "bdown - Break Down";
	eMusic[36] = "hell - Running Hell";
	eMusic[37] = "jenka2 - Jenka 2";
	eMusic[38] = "marine - Living Waterway";
	eMusic[39] = "ballos - Seal Chamber";
	eMusic[40] = "toroko - Toroko's Theme";
	eMusic[41] = "white - White";
	QMap<int, QString> & eFlags = cdgenumglobalmdl_initializeBaseEnum(this, "flags");

	QMap<int, QString> & eNPCCategory = cdgenumglobalmdl_initializeBaseEnum(this, "npcCategory");
	int idx = 0;
	for (const QString & key : cdcategory_names())
		eNPCCategory[idx++] = key;

	// -- Actual Load --
	for (const QString & key : evals.keys()) {
		auto val = evals[key].toObject();
		QMap<int, QString> work;
		for (const QString & key2 : val.keys())
			work[key2.toInt()] = val[key2].toString();
		enums[key] = work;
		enumsLoaded.append(key);
	}
}

void CdgEnumGlobalMdl::saveGlobal(CdExceptionTracker &) {

}

QJsonObject CdgEnumGlobalMdl::storeCfg() {
	QJsonObject meta;
	meta["type"] = "enums";
	QJsonObject ev;
	for (const QString & key : enumsLoaded) {
		auto val = enums[key];
		QJsonObject evals;
		for (int key2 : val.keys())
			evals[QString::number(key2)] = val[key2];
		ev[key] = evals;
	}
	meta["enums"] = ev;
	return meta;
}

void CdgEnumGlobalMdl::initCB(QComboBox * target, const QString & key) {
	target->clear();
	if (enums.contains(key)) {
		auto enm = enums[key];
		QList<int> sortedKeys = enm.keys();
		std::sort(sortedKeys.begin(), sortedKeys.end());
		for (int val : sortedKeys)
			target->addItem(QString::number(val) + QString(" - ") + enm[val]);
	} else {
		target->addItem(QString("0 - NO ENUM ") + key);
	}
	target->setCurrentIndex(0);
}

void CdgEnumGlobalMdl::setCB(QComboBox * target, const QString & key, int value) {
	if (enums.contains(key)) {
		auto enm = enums[key];
		if (!enm.contains(value)) {
			enm[value] = "Unknown enum value (see project file)";
			if (!enumsLoaded.contains(key))
				enumsLoaded.append(key);
			// fall through to plan A
		} else {
			QString pfx = QString::number(value) + QString(" ");
			for (int i = 0; i < target->count(); i++) {
				if (target->itemText(i).startsWith(pfx)) {
					target->setCurrentIndex(i);
					return;
				}
			}
			// shouldn't be possible under sane circumstances, but is possible
			// i.e. if a CB elsewhere causes the enum to have a value added after CB init
		}
	}
	// If we get here, there's no value!
	int idx = target->count();
	target->addItem(QString::number(value) + " - Unknown enum value (see project file)");
	target->setCurrentIndex(idx);
}

int CdgEnumGlobalMdl::getCB(QComboBox * target) {
	return target->currentText().split(" ")[0].toInt();
}
