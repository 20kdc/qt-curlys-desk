#ifndef CDGEXEGLOBALMDL_H
#define CDGEXEGLOBALMDL_H

#include "models/cdkbasevisiblemdl.h"

class QTextCodec;
class QExe;

class CdgEXEGlobalMdl : public CdkBaseVisibleMdl {
public:
	CdgEXEGlobalMdl(CdkGameMdl * mdl, QString filename, QString baseFilename);
	~CdgEXEGlobalMdl();
	void saveGlobal(CdExceptionTracker & ex) override;
	QJsonObject storeCfg() override;
	void show() override;
	// this also throws
	void extractStages(const QString & dest);
	QString filename, baseFilename;
private:
	QExe * baseExe;
};

#endif // CDGEXEGLOBALMDL_H
