#ifndef CDGCSMAPMDL_H
#define CDGCSMAPMDL_H

#include "cdkbasemapmdl.h"
#include "cdkgamemdl.h"

class CdgNPCTBLGlobalMdl;

class CdgCSMapMdl : public CdkBaseMapMdl {
public:
	CdgCSMapMdl(CdkGameMdl * gm, int idx);
	virtual ~CdgCSMapMdl() override;

	void save(CdExceptionTracker & ex) override;
	void saveCollision(CdExceptionTracker & ex) override;
	// Used for moving & deletion.
	void delFiles() override;
	CdkEntityDrawInfo getEntityDrawInfo(const CdkEntity & entity) override;
	QList<QPixmap> surfaces;
	CdkDependency<CdgNPCTBLGlobalMdl> npctbl;

private:
	int mapIdx;
};

#endif // CDGCSMAPMDL_H
