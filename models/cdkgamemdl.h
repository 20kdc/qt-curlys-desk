#ifndef CDKGAMEMDL_H
#define CDKGAMEMDL_H

#include <QString>
#include <QList>
#include <QTextCodec>
#include <QJsonObject>
#include "cdkbasestagemdl.h"
#include "cdkbasemapmdl.h"
#include "cdkbaseglobalmdl.h"
#include "cdregistry.h"

typedef CdkBaseGlobalMdl * (*cdkglobal_loader_t)(CdkGameMdl * mdl, const QJsonObject & data);

// These functions can throw QString
extern CdRegistry<cdkglobal_loader_t> cdkGlobalRegistry;

#define CDKGAMEMDL_REGISTER_GLOBAL(name) \
	static CdkBaseGlobalMdl * cdkgamemdl_create_global_ ## name (CdkGameMdl * mdl, const QJsonObject & data); \
	static auto cdkgamemdl_register_global_ ## name = cdkGlobalRegistry.put(#name, cdkgamemdl_create_global_ ## name); \
	static CdkBaseGlobalMdl * cdkgamemdl_create_global_ ## name (CdkGameMdl * mdl, const QJsonObject & data)

class CdkBaseMapMdl;

class CdkGameMdl {
public:
	// Errors during this leave the object in an invalid state. It must be deconstructed.
	CdkGameMdl(CdExceptionTracker & ex, const QString & prefix, const QString & executable);
	~CdkGameMdl();
	void save(CdExceptionTracker & ex);
	QPixmap loadPBM(const QString & id, bool transparent);
	QString getPath(const QString & path);
	CdkBaseMapMdl * openMap(int idx);
	template <class X> X * findOptDependency() {
		for (CdkBaseGlobalMdl * mdl : gModels) {
			X * res = dynamic_cast<X *>(mdl);
			if (res != nullptr)
				return res;
		}
		return nullptr;
	}
	template <class X> X * findDependency(CdExceptionTracker & ex, const QString & dependent) {
		X * res = findOptDependency<X>();
		if (res)
			return res;
		ex.report(dependent + QString(" could not find ") + QString(typeid(X).name()));
		return nullptr;
	}

	// This is okay even if the object is in an invalid state
	bool terminateTracker(CdExceptionTracker & ex, QWidget * parent);

	// -- Game Data --
	QList<CdfStage> stages;
	// -- Config Game Statics --
	QString pxaPre, pxaPost;
	QString pxmPre, pxmPost;
	QString pxePre, pxePost;

	QString mtscPre, mtscPost;

	QString prtPre, pbmPost;

	QString pbmFmt;

	QStringList gameRunArgs;

	QTextCodec * gameCodec;
	int tileSize;
	int scale = 2;
	// -- Models --
	// must be in next array
	CdkBaseStageMdl * stageModel = nullptr;
	QList<CdkBaseGlobalMdl *> gModels;
	// -- The Root UI Widget For Dialogs --
	QWidget * theOneTrueRoot = nullptr;
private:
	QString fullPfx;
};

#endif // CDKGAMEMDL_H
