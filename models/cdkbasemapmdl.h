#ifndef CDKBASEMAPMDL_H
#define CDKBASEMAPMDL_H

#include <stdint.h>
#include <QPixmap>
#include <QIcon>
#include "cdkbasemdl.h"
#include "cdexcept.h"

class CdkGameMdl;

class CdkEntity {
public:
	// In PXE order.
	int16_t x;
	int16_t y;
	uint16_t flagId;
	uint16_t eventId;
	uint16_t chrId; // some bound checks rely on this being uint!
	uint16_t bitField;
};

class CdkEntityDrawInfo {
public:
	// Note: This is held by the CdkBaseMapMdl and is temporary.
	// If it's null, then this isn't even a valid draw info.
	QPixmap * image = nullptr;
	QRect bounds;
	QPoint shift;
	QRect indicator;
};

class CdkMapState {
public:
	CdkMapState(int w, int h) : w(w), h(h) {
		data = new uint8_t[w * h];
		memset(data, 0, w * h);
	}

	// 'Rule of 3'
	CdkMapState(const CdkMapState & other) : w(other.w), h(other.h) {
		data = new uint8_t[w * h];
		memcpy(data, other.data, w * h);
		entities = other.entities;
	}
	CdkMapState & operator=(const CdkMapState & other) {
		delete[] data;
		w = other.w;
		h = other.h;
		data = new uint8_t[w * h];
		memcpy(data, other.data, w * h);
		entities = other.entities;
		return *this;
	}
	~CdkMapState() {
		delete[] data;
	}

	// Utils
	bool tileInBounds(int x, int y) {
		if ((x < 0) || (y < 0))
			return false;
		if ((((size_t) x) >= w) || (((size_t) y) >= h))
			return false;
		return true;
	}
	uint8_t & tile(int x, int y) {
		return data[x + (y * w)];
	}

	size_t width() {
		return w;
	}
	size_t height() {
		return h;
	}

	uint8_t * data;
	QList<CdkEntity> entities;

private:
	size_t w, h;
};


class CdkNPCTypeInfo {
public:
	QIcon icon;
	int category;
	QString name;
};

class CdkBaseMapMdl : public CdkBaseMdl {
public:
	CdkBaseMapMdl(CdkGameMdl * gm);
	~CdkBaseMapMdl();

	virtual void save(CdExceptionTracker & ex) = 0;
	virtual void saveCollision(CdExceptionTracker & ex) = 0;
	virtual void delFiles() = 0;
	virtual CdkEntityDrawInfo getEntityDrawInfo(const CdkEntity &) {
		// Parameter is entity ID.
		return CdkEntityDrawInfo();
	}

	bool tileInBounds(int x, int y) {
		return state.tileInBounds(x, y);
	}
	size_t w() {
		return state.width();
	}
	size_t h() {
		return state.height();
	}
	uint8_t & tile(int x, int y) {
		return state.tile(x, y);
	}

	void shift(QRect rect);

	CdkMapState state;
	QPixmap tileset;
	QPixmap background;
	QPixmap backgroundCK;
	int backgroundType;
	QList<CdkNPCTypeInfo> allNPCTypes;
	uint8_t collisionMap[256];
};

#endif // CDKBASEMAPMDL_H
