#include "cdgcsmapmdl.h"
#include "cdkgamemdl.h"
#include "cdgnpctblglobalmdl.h"
#include "cdutils.h"

CdgCSMapMdl::CdgCSMapMdl(CdkGameMdl * gm, int idx) : CdkBaseMapMdl(gm), mapIdx(idx) {
	const CdfStage & stage = boundMdl->stages[mapIdx];
	tileset = gm->loadPBM(boundMdl->prtPre + stage.tileset, true);
	background = gm->loadPBM(npctbl->bkgPre + stage.bkg, false);
	backgroundCK = gm->loadPBM(npctbl->bkgPre + stage.bkg, true);
	backgroundType = stage.bkgType;
	for (const QString & surface : npctbl->surfaces) {
		QString realSurface = surface;
		QPixmap base;
		if (surface == "MAP_TILESET") {
			surfaces.append(tileset);
			continue;
		} else if (surface == "MAP_NPC1") {
			realSurface = npctbl->npcPre + stage.npc1Tile;
		} else if (surface == "MAP_NPC2") {
			realSurface = npctbl->npcPre + stage.npc2Tile;
		} else if (surface == "MAP_BACKGROUND") {
			realSurface = npctbl->bkgPre + stage.bkg;
		}
		surfaces.append(gm->loadPBM(realSurface, true));
	}

	int nIdx = 0;
	for (CdgNPCTBLEntry & npc : npctbl->npcs) {
		CdkNPCTypeInfo inf;
		inf.category = npc.category;
		inf.name = QString::number(nIdx++) + ": " + npc.name;
		if (npc.surfaceId < surfaces.length())
			inf.icon = QIcon(surfaces[npc.surfaceId].copy(npc.cfgA.spriteBounds));
		allNPCTypes.append(inf);
	}

	{
		QFile src(boundMdl->getPath(boundMdl->pxmPre + stage.mapId + boundMdl->pxmPost));
		if (src.open(QFile::ReadOnly)) {
			QByteArray qba = src.readAll();
			src.close();
			if (qba.length() >= 8) {
				int w = cdutils_load_u16(qba.mid(4, 2));
				int h = cdutils_load_u16(qba.mid(6, 2));
				CdkMapState newState(w, h);
				if (qba.length() >= (8 + (w * h)))
					memcpy(newState.data, qba.data() + 8, w * h);
				state = newState;
			}
		}
	}
	{
		QFile src(boundMdl->getPath(boundMdl->pxePre + stage.mapId + boundMdl->pxePost));
		if (src.open(QFile::ReadOnly)) {
			QByteArray qba = src.readAll();
			src.close();
			if (qba.length() >= 8) {
				uint32_t entityCount = cdutils_load_u32(qba.mid(4, 4));
				for (uint32_t xd = 0; xd < entityCount; xd++) {
					int base = 8 + (xd * 12);
					CdkEntity entity;
					entity.x = cdutils_load_i16(qba.mid(base, 2));
					entity.y = cdutils_load_i16(qba.mid(base + 2, 2));
					entity.flagId = cdutils_load_u16(qba.mid(base + 4, 2));
					entity.eventId = cdutils_load_u16(qba.mid(base + 6, 2));
					entity.chrId = cdutils_load_u16(qba.mid(base + 8, 2));
					entity.bitField = cdutils_load_u16(qba.mid(base + 10, 2));
					state.entities.append(entity);
				}
			}
		}
	}
	{
		QFile src(boundMdl->getPath(boundMdl->pxaPre + stage.tileset + boundMdl->pxaPost));
		if (src.open(QFile::ReadOnly)) {
			QByteArray qba = src.readAll();
			src.close();
			if (qba.length() >= 256)
				memcpy(collisionMap, qba.data(), 256);
		}
	}
}

CdgCSMapMdl::~CdgCSMapMdl() {

}

void CdgCSMapMdl::save(CdExceptionTracker & ex) {
	if (w() > 65535)
		CD_THROW("Map size out of range");
	if (h() > 65535)
		CD_THROW("Map size out of range");
	const CdfStage & stage = boundMdl->stages[mapIdx];
	{
		QFile src(boundMdl->getPath(boundMdl->pxmPre + stage.mapId + boundMdl->pxmPost));
		if (src.open(QFile::WriteOnly)) {
			src.write("PXM\x10");
			cdutils_save_u16(&src, (uint16_t) w());
			cdutils_save_u16(&src, (uint16_t) h());
			src.write((const char *) state.data, w() * h());
			src.close();
		} else {
			CD_THROW("Unable to open file PXM...");
		}
	}
	{
		QFile src(boundMdl->getPath(boundMdl->pxePre + stage.mapId + boundMdl->pxePost));
		if (src.open(QFile::WriteOnly)) {
			src.write("PXE\x00", 4);
			cdutils_save_u32(&src, (uint32_t) state.entities.length());
			for (const CdkEntity & ce : state.entities) {
				cdutils_save_u16(&src, ce.x);
				cdutils_save_u16(&src, ce.y);
				cdutils_save_u16(&src, ce.flagId);
				cdutils_save_u16(&src, ce.eventId);
				cdutils_save_u16(&src, ce.chrId);
				cdutils_save_u16(&src, ce.bitField);
			}
			src.close();
		} else {
			CD_THROW("Unable to open file PXE...");
		}
	}
}

void CdgCSMapMdl::saveCollision(CdExceptionTracker & ex) {
	const CdfStage & stage = boundMdl->stages[mapIdx];
	QFile src(boundMdl->getPath(boundMdl->pxaPre + stage.tileset + boundMdl->pxaPost));
	if (src.open(QFile::WriteOnly)) {
		src.write((const char *) collisionMap, 256);
		src.close();
	} else {
		CD_THROW("Unable to open PXA...");
	}
}

void CdgCSMapMdl::delFiles() {
	const CdfStage & stage = boundMdl->stages[mapIdx];
	QFile src(boundMdl->getPath(boundMdl->pxmPre + stage.mapId + boundMdl->pxmPost));
	src.remove();
}

CdkEntityDrawInfo CdgCSMapMdl::getEntityDrawInfo(const CdkEntity & entity) {
	CdkEntityDrawInfo info;
	if (entity.chrId < npctbl->npcs.length()) {
		const CdgNPCTBLEntry & entry = npctbl->npcs[entity.chrId];
		if (entry.surfaceId < surfaces.length())
			info.image = &surfaces[entry.surfaceId];
		bool right = entity.bitField & 0x1000;
		const CdgNPCTBLEntryConfig & cfg = right ? entry.cfgB : entry.cfgA;
		// info.bounds = QRect(0, 0, info.image->width(), info.image->height());
		info.shift = -QPoint(right ? entry.displayR : entry.displayL, entry.displayU);
		info.shift += cfg.nudge;
		info.shift += QPoint(8, 8);
		info.bounds = cfg.spriteBounds;
		info.indicator = cfg.indicatorBounds;
		//qInfo() << "AT " << info.shift << " " << info.bounds;
	}
	return info;
}
