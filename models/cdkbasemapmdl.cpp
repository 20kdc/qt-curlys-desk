#include "cdkbasemapmdl.h"

CdkBaseMapMdl::CdkBaseMapMdl(CdkGameMdl * gm) : CdkBaseMdl(gm), state(40, 15) {
	QPixmap ts;
	ts.convertFromImage(QImage(256, 256, QImage::Format_RGBA8888));
	tileset = ts;
	memset(collisionMap, 0, 256);
}

void CdkBaseMapMdl::shift(QRect rect) {
	CdkMapState cms(rect.width(), rect.height());
	int idx = 0;
	for (size_t j = 0; j < cms.height(); j++) {
		for (size_t i = 0; i < cms.width(); i++) {
			int px = i + rect.x();
			int py = j + rect.y();
			if (tileInBounds(px, py)) {
				cms.data[idx++] = tile(px, py);
			} else {
				cms.data[idx++] = 0;
			}
		}
	}
	for (CdkEntity & ce : state.entities) {
		CdkEntity copy = ce;
		copy.x -= rect.x();
		copy.y -= rect.y();
		cms.entities.append(copy);
	}
	state = cms;
}

CdkBaseMapMdl::~CdkBaseMapMdl() {
}
