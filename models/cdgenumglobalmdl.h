#ifndef CDGENUMGLOBALMDL_H
#define CDGENUMGLOBALMDL_H

#include "cdkbaseglobalmdl.h"
#include <QMap>
#include <QComboBox>

class CdgEnumGlobalMdl : public CdkBaseGlobalMdl {
	Q_OBJECT
public:
	CdgEnumGlobalMdl(CdkGameMdl * base, const QJsonObject & env);

	void saveGlobal(CdExceptionTracker & ex) override;
	QJsonObject storeCfg() override;

	QMap<QString, QMap<int, QString>> enums;
	QStringList enumsLoaded;

	void initCB(QComboBox * target, const QString & key);
	inline void initCB(QComboBox * target, const QString & key, int value) {
		initCB(target, key);
		setCB(target, key, value);
	}
	void setCB(QComboBox * target, const QString & key, int value);
	int getCB(QComboBox * target);
};

#endif // CDGENUMGLOBALMDL_H
