#ifndef CDGNPCTBLGLOBALMDL_H
#define CDGNPCTBLGLOBALMDL_H

#include "cdkbasevisiblemdl.h"

class CdgNPCTBLEntryConfig {
public:
	QRect spriteBounds = QRect(0, 0, 16, 16);
	QRect indicatorBounds = QRect(0, 0, 0, 0);
	QPoint nudge = QPoint(0, 0);
};

class CdgNPCTBLEntry {
public:
	// actual npc.tbl data
	uint16_t flags = 0;
	int16_t health = 0;
	uint8_t surfaceId = 0;
	uint8_t deathSoundId = 0;
	uint8_t hurtSoundId = 0;
	uint8_t sizeId = 0;
	int32_t exp = 0;
	int32_t damage = 0;
	int8_t hitL = 0;
	int8_t hitU = 0;
	int8_t hitR = 0;
	int8_t hitD = 0;
	int8_t displayL = 0;
	int8_t displayU = 0;
	int8_t displayR = 0;
	int8_t displayD = 0;
	// extended data
	bool extModified = false;
	QString name = "Unknown NPC";
	QString description = "NPC, information not available";
	int category = 0;
	QString sheet = "Npc/NpcSym";
	CdgNPCTBLEntryConfig cfgA;
	CdgNPCTBLEntryConfig cfgB;
};

extern QJsonObject cdgnpctbl_constantMetadata;

class QFile;

class CdpNPCTBLEditor;

class CdgNPCTBLGlobalMdl : public CdkBaseVisibleMdl {
	Q_OBJECT
public:
	CdgNPCTBLGlobalMdl(CdkGameMdl * gameMdl, const QString & file, const QJsonObject & meta);
	~CdgNPCTBLGlobalMdl();
	void saveGlobal(CdExceptionTracker & ex) override;
	QJsonObject storeCfg() override;
	QList<CdgNPCTBLEntry> npcs;
	QList<QString> surfaces;
	QString bkgPre;
	QString npcPre;
	QString file;
private:
	QFile * fileAccess;
	CdpNPCTBLEditor * editor;
public slots:
	void show() override;
};

#endif // CDGNPCTBLGLOBALMDL_H
