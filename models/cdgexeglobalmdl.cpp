#include "models/cdgexeglobalmdl.h"
#include "models/cdkgamemdl.h"
#include "qexe.h"
#include "models/exe/cdeexeconstants.h"
#include "models/exe/cdestageupdate.h"

CDKGAMEMDL_REGISTER_GLOBAL(exe) {
	return new CdgEXEGlobalMdl(mdl, data["file"].toString(), data["base"].toString());
}

CdgEXEGlobalMdl::CdgEXEGlobalMdl(CdkGameMdl * mdl, QString fn, QString fnb) : CdkBaseVisibleMdl(mdl), filename(fn), baseFilename(fnb) {
	setObjectName(fn);
	QFile file(boundMdl->getPath(baseFilename));
	baseExe = new QExe(this);
	baseExe->setAutoCreateRsrcManager(false);
	if (!file.open(QFile::ReadOnly))
		throw QString("Unable to open EXE");
	QExeErrorInfo info;
	if (!baseExe->read(file, &info))
		throw (QString("Unable to comprehend EXE, error 0x") + QString::number(info.errorID, 16));
}

CdgEXEGlobalMdl::~CdgEXEGlobalMdl() {
}

void CdgEXEGlobalMdl::saveGlobal(CdExceptionTracker & exB) {
	CD_DERIVE_EXCEPTIONS;
	QFile file(boundMdl->getPath(filename));
	if (!file.open(QFile::ReadWrite))
		CD_THROW("Unable to open EXE for writing");

	file.seek(0);
	if (!baseExe->write(file))
		CD_THROW("Failed target base write");

	QExe targetExe;
	targetExe.setAutoCreateRsrcManager(false);
	targetExe.setAutoAddFillerSections(true);

	file.seek(0);
	if (!targetExe.read(file))
		CD_THROW("Failed target read");

	// Perform
	cdestageupdate(ex, &targetExe, boundMdl->gameCodec, boundMdl->stages);
	CD_POLL;

	// Finish

	file.seek(0);
	if (!targetExe.write(file))
		CD_THROW("Failed target final write");
}

void CdgEXEGlobalMdl::show() {
}

QJsonObject CdgEXEGlobalMdl::storeCfg() {
	QJsonObject cfg;
	cfg["type"] = "exe";
	cfg["file"] = filename;
	cfg["base"] = baseFilename;
	return cfg;
}

void CdgEXEGlobalMdl::extractStages(const QString & dest) {
	quint32 actualAddress;
	if (!cdeexe_get(baseExe, cdeexeconstants_mapAddress[0].rva, &actualAddress))
		throw QString("Unable to get stagetbl address");
	quint32 actualCount;
	if (!cdeexe_get(baseExe, cdeexeconstants_mapCount[0].rva, &actualCount))
		throw QString("Unable to get stagetbl count");

	quint32 baseAddress = baseExe->optionalHeader()->imageBase;

	// specific override because it reports 128 when it isn't really 128
	if (actualAddress == CDEEXECONSTANTS_DEFAULT_STAGETBL_VA)
		actualCount = 95;

	size_t actualSize = 0xC8 * actualCount;
	char stages[actualSize];
	if (!cdeexe_get(baseExe, actualAddress - baseAddress, stages, actualSize))
		throw QString("Unable to get stagetbl count");

	QFile stagetbl(dest);
	if (!stagetbl.open(QFile::WriteOnly))
		throw QString("Unable to write stage.sect");
	stagetbl.write(stages, actualSize);
}
