#include "cdeexeconstants.h"
#include "qexe.h"

// The following constants are borrowed from Cave Editor by Wistil.
// As they are merely facts about the game, I assume they do not affect licensing.
// However, credit will still be given.

const CdeEXEConstantPatch cdeexeconstants_mapAddress[] = {
	// This first one must always be an offset 0 one (used in stage extraction)
	{0x00020C2F, 0},
	{0x00020C73, 0},
	{0x00020CB5, 0x20},
	{0x00020CF6, 0x20},
	{0x00020D38, 0x20},
	{0x00020D7A, 0x44},
	{0x00020D9E, 0x40},
	{0x00020DD9, 0x64},
	{0x00020E1C, 0x84},
	{0x00020E6A, 0xA5},
	{0x00020EA8, 0xA4},
	{0, 0}
};
const CdeEXEConstantPatch cdeexeconstants_mapCount[] = {
	// This first one must always be an offset 0 one (used in stage extraction)
	// also this was measured section-relative, so adding 0x1000 is necessary
	{0x00014B24, 0},
	{0, 0}
};

void cdeexe_put(QExe * exe, const CdeEXEConstantPatch * patches, quint32 value) {
	while (patches->rva) {
		quint32 val = value + patches->ofs;
		cdeexe_put(exe, patches->rva, &val);
		patches++;
	}
}
