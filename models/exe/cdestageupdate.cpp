#include "cdestageupdate.h"
#include "cdeexeconstants.h"
#include "formats/cdfstage.h"
#include "cdutils.h"
#include <QDataStream>

static quint32 cdestageupdate_newSection(CdExceptionTracker & ex, QExe * target, const QLatin1String & name, size_t room);
static void cdestageupdate_removeExistingMapSections(QExe * target);
static void cdestageupdate_relocateResources(QByteArray & data, quint32 dirAddr, quint32 ofs);
static void cdestageupdate_injectResources(QExe * target, quint32 rsrcRva, QByteArray & data);
static void cdestageupdate_dumpStages(QBuffer * target, QTextCodec * codec, const QList<CdfStage> & stages);

void cdestageupdate(CdExceptionTracker & exB, QExe * target, QTextCodec * codec, const QList<CdfStage> & stages) {
	CdExceptionTracker ex("cdestageupdate", exB);
	int rsrcIdx = target->sectionManager()->rsrcSectionIndex();
	QByteArray rsrcData;
	quint32 oldRsrcBase;
	if (rsrcIdx != -1) {
		QExeSectionPtr ptr = target->sectionManager()->sectionAt(rsrcIdx);
		rsrcData = ptr.data()->rawData;
		oldRsrcBase = ptr.data()->virtualAddr;
		target->sectionManager()->removeSection(rsrcIdx);
	}
	cdestageupdate_removeExistingMapSections(target);

	quint32 mapBase = cdestageupdate_newSection(ex, target, QLatin1String(".csmap"), stages.length() * 0xC8);
	CD_POLL;
	QBuffer * buffer = target->sectionManager()->setupRVAPoint(mapBase, QIODevice::WriteOnly);
	if (!buffer)
		CD_THROW("Unable to setup write");
	cdestageupdate_dumpStages(buffer, codec, stages);
	delete buffer;

	// Apply patches
	cdeexe_put(target, cdeexeconstants_mapAddress, mapBase + target->optionalHeader()->imageBase);
	cdeexe_put(target, cdeexeconstants_mapCount, stages.length());

	if (rsrcIdx != -1) {
		quint32 rsrcBase = cdestageupdate_newSection(ex, target, QLatin1String(".rsrc"), rsrcData.length());
		CD_POLL;
		cdestageupdate_relocateResources(rsrcData, 0, rsrcBase - oldRsrcBase);
		cdestageupdate_injectResources(target, rsrcBase, rsrcData);
	}
}

quint32 cdestageupdate_newSection(CdExceptionTracker & ex, QExe * target, const QLatin1String & name, size_t room) {
	// Make section at end
	QExeSectionPtr section = target->sectionManager()->createSection(name, QByteArray(room, 0));
	if (!section) {
		ex.report("newSection failed!");
		return 0;
	}
	return section->virtualAddr;
}

void cdestageupdate_removeExistingMapSections(QExe * target) {
	// Remove existing map section.
	QExeSectionPtr mapSection = target->sectionManager()->sectionWithName(QLatin1String(".csmap"));
	if (!mapSection)
		mapSection = target->sectionManager()->sectionWithName(QLatin1String(".swdata"));
	if (mapSection)
		target->sectionManager()->removeSection(mapSection);
}

void cdestageupdate_injectResources(QExe * target, quint32 rsrcRva, QByteArray & data) {
	// Resource injection
	cdeexe_put(target, rsrcRva, data.data(), data.length());
	DataDirectoryPtr targetPair = target->optionalHeader()->dataDirectories[QExeOptionalHeader::ResourceTable];
	targetPair->first = rsrcRva;
}

void cdestageupdate_dumpStages(QBuffer * buffer, QTextCodec * codec, const QList<CdfStage> & stages) {
	CduBinaryW writer(buffer, codec);
	// dump stages
	for (const CdfStage & stage : stages)
		stage.write(writer, CDFSTAGE_FORMAT_CS_BASE);
}

void cdestageupdate_relocateResources(QByteArray & data, quint32 dirAddr, quint32 ofs) {
	// Resource relocation
	QBuffer buffer(&data);
	buffer.open(QIODevice::ReadWrite);
	buffer.seek(dirAddr + 12);
	quint32 entries = cdutils_load_u16(&buffer);
	entries += cdutils_load_u16(&buffer);

	quint32 results[entries];
	for (quint32 i = 0; i < entries; i++) {
		cdutils_load_u32(&buffer);
		results[i] = cdutils_load_u32(&buffer);
	}

	for (quint32 i = 0; i < entries; i++) {
		if (results[i] & 0x80000000U) {
			cdestageupdate_relocateResources(data, results[i] & 0x7FFFFFFF, ofs);
		} else {
			buffer.seek(results[i]);
			quint32 val = cdutils_load_u32(&buffer);
			buffer.seek(results[i]);
			val += ofs;
			cdutils_save_u32(&buffer, val);
		}
	}
}
