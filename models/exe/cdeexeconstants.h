#ifndef CDGEXECONSTANTS_H
#define CDGEXECONSTANTS_H

#include <QList>
#include "qexe.h"

struct CdeEXEConstantPatch {
	uint32_t rva;
	uint32_t ofs;
};

// See .cpp for these.
// They are terminated by {0, 0}.
extern const CdeEXEConstantPatch cdeexeconstants_mapAddress[];
extern const CdeEXEConstantPatch cdeexeconstants_mapCount[];

// Reads data from the EXE.
inline bool cdeexe_get(QExe * exe, quint32 rva, char * target, size_t size) {
	QBuffer * b = exe->sectionManager()->setupRVAPoint(rva, QFile::ReadOnly);
	if (!b)
		return false;
	if (((size_t) (b->size() - b->pos())) < size)
		return false;
	b->read(target, size);
	delete b;
	return true;
}

// Reads a number from the EXE.
template<typename X> bool cdeexe_get(QExe * exe, quint32 rva, X * target) {
	return cdeexe_get(exe, rva, (char *) target, sizeof(X));
}

// Writes data to the EXE.
inline bool cdeexe_put(QExe * exe, quint32 rva, char * target, size_t size) {
	QBuffer * b = exe->sectionManager()->setupRVAPoint(rva, QFile::WriteOnly);
	if (!b)
		return false;
	if (((size_t) (b->size() - b->pos())) < size)
		return false;
	b->write(target, size);
	delete b;
	return true;
}

// Writes a number to the EXE.
template<typename X> bool cdeexe_put(QExe * exe, quint32 rva, X * target) {
	return cdeexe_put(exe, rva, (char *) target, sizeof(X));
}

// Writes to the EXE via a series of patches.
void cdeexe_put(QExe * exe, const CdeEXEConstantPatch * patches, quint32 value);

// The following constants are borrowed from Cave Editor by Wistil.
// As they are merely facts about the game, I assume they do not affect licensing.
// However, credit will still be given.
#define CDEEXECONSTANTS_DEFAULT_STAGETBL_VA 0x4937B0

#endif // CDGEXECONSTANTS_H
