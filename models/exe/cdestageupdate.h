#ifndef CDESTAGEUPDATE_H
#define CDESTAGEUPDATE_H

#include "qexe.h"
#include "cdexcept.h"

class CdfStage;

void cdestageupdate(CdExceptionTracker & ex, QExe * target, QTextCodec * codec, const QList<CdfStage> & stages);

#endif // CDESTAGEUPDATE_H
