#ifndef CDKBASEVISIBLEMDL_H
#define CDKBASEVISIBLEMDL_H

#include "cdkbaseglobalmdl.h"

class CdkBaseVisibleMdl : public CdkBaseGlobalMdl {
	Q_OBJECT
public:
	CdkBaseVisibleMdl(CdkGameMdl *);
public slots:
	virtual void show() = 0;
};

#endif // CDKBASEVISIBLEMDL_H
