#ifndef CDGEXTSTAGEMDL_H
#define CDGEXTSTAGEMDL_H

#include <QString>
#include <QTextCodec>
#include <QFile>

#include "cdkbasestagemdl.h"

class CdgExtStageMdl : public CdkBaseStageMdl {
	Q_OBJECT
public:
	// The passed QTextCodec is not owned here.
	CdgExtStageMdl(CdkGameMdl * gameMdl, const QString & file, int mode);
	~CdgExtStageMdl() override;

	bool supportsMNA2() override;

	QList<CdfStage> load(CdExceptionTracker & ex) override;
	void saveGlobal(CdExceptionTracker & ex) override;
	QJsonObject storeCfg() override;

	int mode;
	const QString stbl;
private:
	QFile * fileAccess;
	QTextCodec * codec;
};

#endif // CDGEXTSTAGEMDL_H
