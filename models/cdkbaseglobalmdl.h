#ifndef CDKBASEGLOBALMDL_H
#define CDKBASEGLOBALMDL_H

#include <QDialog>
#include <QJsonObject>
#include "cdkbasemdl.h"
#include "cdexcept.h"

class CdkBaseGlobalMdl : public CdkBaseMdl {
	Q_OBJECT
public:
	CdkBaseGlobalMdl(CdkGameMdl * mdl);
	virtual void saveGlobal(CdExceptionTracker & ex) = 0;
	virtual QJsonObject storeCfg() = 0;
};

#endif // CDKBASEGLOBALMDL_H
