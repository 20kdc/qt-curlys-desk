#include <QJsonArray>
#include "cdgtsclanguagemdl.h"

static QJsonObject cmd(const QString & longName, const QString & description) {
	QJsonObject obj;
	obj["parameterText"] = "";
	obj["numbers"] = QJsonArray();
	obj["longName"] = longName;
	obj["description"] = description;
	return obj;
}

static QJsonObject cmd(const QString & longName, const QString & parameterText, const QString & parameter1Type, const QString & description) {
	QJsonObject obj;
	obj["parameterText"] = parameterText;
	QJsonArray num;
	num.append(CdgTSCCompletionNumberDatum(0, parameter1Type).save());
	obj["numbers"] = num;
	obj["longName"] = longName;
	obj["description"] = description;
	return obj;
}

static QJsonObject cmd(const QString & longName, const QString & parameterText, const QString & parameter1Type, const QString & parameter2Type, const QString & description) {
	QJsonObject obj;
	obj["parameterText"] = parameterText;
	QJsonArray num;
	num.append(CdgTSCCompletionNumberDatum(0, parameter1Type).save());
	num.append(CdgTSCCompletionNumberDatum(5, parameter2Type).save());
	obj["numbers"] = num;
	obj["longName"] = longName;
	obj["description"] = description;
	return obj;
}

static QJsonObject cmd(const QString & longName, const QString & parameterText, const QString & p1, const QString & p2, const QString & p3, const QString & description) {
	QJsonObject obj;
	obj["parameterText"] = parameterText;
	QJsonArray num;
	num.append(CdgTSCCompletionNumberDatum(0, p1).save());
	num.append(CdgTSCCompletionNumberDatum(5, p2).save());
	num.append(CdgTSCCompletionNumberDatum(10, p3).save());
	obj["numbers"] = num;
	obj["longName"] = longName;
	obj["description"] = description;
	return obj;
}

static QJsonObject cmd(const QString & longName, const QString & parameterText, const QString & p1, const QString & p2, const QString & p3, const QString & p4, const QString & description) {
	QJsonObject obj;
	obj["parameterText"] = parameterText;
	QJsonArray num;
	num.append(CdgTSCCompletionNumberDatum(0, p1).save());
	num.append(CdgTSCCompletionNumberDatum(5, p2).save());
	num.append(CdgTSCCompletionNumberDatum(10, p3).save());
	num.append(CdgTSCCompletionNumberDatum(15, p4).save());
	obj["numbers"] = num;
	obj["longName"] = longName;
	obj["description"] = description;
	return obj;
}

static QJsonObject cdgtsclanguagemdl_tscbuiltin_gen() {
	QJsonObject gen;

	// TODO: Figure out which of these are actually extension stuff.

#define MAPPING_FEATURE "There is a set of 128 'mapping flags' (one per stage), which is assumed to be for a cut feature limiting the capabilities of the Map System.<br/>"

	gen["END"] = cmd(
		"End",
		"Stops the script. As such, automatically returns control to the player."
	);
	gen["LI+"] = cmd(
		"Life +",
		"AAAA", "",
		"Adds A health to the player."
	);
	gen["ML+"] = cmd(
		"Max Life +",
		"AAAA", "",
		"Expands the player's max health by A."
	);
	gen["AE+"] = cmd(
		"Ammo Everything +",
		"Refills all of the player's ammunition in every weapon they have which has ammunition."
	);
	gen["IT+"] = cmd(
		"Item +",
		"AAAA", "item",
		"Gives the player item A, and plays a sound."
	);
	gen["IT-"] = cmd(
		"Item -",
		"AAAA", "item",
		"Removes item A from the player."
	);
	gen["EQ+"] = cmd(
		"Equipment +",
		"AAAA", "equip",
		"Adds equipment A to the player."
	);
	gen["EQ-"] = cmd(
		"Equipment -",
		"AAAA", "equip",
		"Removes equipment A from the player."
	);
	gen["AM+"] = cmd(
		"Ammo/Weapon +",
		"AAAA:BBBB", "weapon", "",
		"Either gives the player weapon A with B ammunition, or adds B ammunition to weapon A."
	);
	gen["AM-"] = cmd(
		"Weapon -",
		"AAAA", "weapon",
		"Takes away weapon A from the player."
	);
	gen["ZAM"] = cmd(
		"Zero Weapon Experience",
		"All weapon levels and experience are reset to zero."
	);
	gen["TAM"] = cmd(
		"Trade Weapon",
		"AAAA:BBBB:CCCC", "weapon", "weapon", "",
		"If weapon A is found, replace it with weapon B, but use weapon A's present ammunition with the cap expanded by C."
	);
	gen["PS+"] = cmd(
		"Add Stage Button",
		"AAAA:BBBB", "", "",
		"Adds stage button A that jumps to label B (in StageSelect.tsc)."
	);
	gen["MP+"] = cmd(
		"Map Flag +",
		"AAAA", "STAGE",
		MAPPING_FEATURE
		"Enables the mapping flag for stage A."
	);
	gen["UNI"] = cmd(
		"Set Player Physics",
		"AAAA", "unit",
		"If A is 1, enables the mode used specifically for Main Artery - if A is 0, disables it."
	);
	gen["STC"] = cmd(
		"Save Time Counter",
		"Writes the Nikumaru Counter time. The player must have the Nikumaru Counter equipped."
	);
	gen["TRA"] = cmd(
		"Travel (Change Map)",
		"AAAA:BBBB:XXXX:YYYY", "STAGE", "LABEL", "X", "Y",
		"Travels to stage A, places player at (X, Y), jumps to label B when in that stage."
	);
	gen["MOV"] = cmd(
		"Move Player",
		"XXXX:YYYY", "X", "Y",
		"Places player at (X, Y)."
	);
	gen["HMC"] = cmd(
		"Hide MyChar",
		"Hides the player."
	);
	gen["SMC"] = cmd(
		"Show MyChar",
		"Shows the player."
	);
	gen["FL+"] = cmd(
		"Flag +",
		"AAAA", "flag",
		"Sets flag A."
	);
	gen["FL-"] = cmd(
		"Flag -",
		"AAAA", "flag",
		"Unsets flag A."
	);
	gen["SK+"] = cmd(
		"Skipflag +",
		"AAAA", "skipflag",
		"Sets skip flag A. Skip flags are reset when the application restarts and ignore save/load."
	);
	gen["SK-"] = cmd(
		"Skipflag -",
		"AAAA", "skipflag",
		"Unsets skip flag A."
	);
	gen["KEY"] = cmd(
		// damage, HUD & input is enabled via game flag 2
		"Lock Input / Hide HUD / Disable Damage / Unfreeze Time",
		"Locks input, hides HUD, stops things damaging each other, disables player looking up, and stops invulnerability frames.<br/>Usually used at the start of a script that's about to show a message box."
	);
	gen["PRI"] = cmd(
		// time is enabled via game flag 1
		"Lock Input / Hide HUD / Disable Damage / Freeze Time",
		"Even more extensive than &lt;KEY, this freezes time. Can be partially cancelled with &lt;KEY or completely cancelled with &lt;FRE."
	);
	gen["FRE"] = cmd(
		"Unlock Input / Show HUD / Enable Damage / Unfreeze Time",
		"This undoes &lt;PRI or &lt;KEY."
	);
	gen["NOD"] = cmd(
		"Wait For Input",
		"Waits for the player to press a button before continuing.<br/>"
		"Usually useful for breaking up large amounts of text."
	);
	gen["CLR"] = cmd(
		"Clear Message Box",
		"Clears the message box."
	);
	gen["MSG"] = cmd(
		"Show / Clear Message Box",
		"Both shows the standard message box and also clears it."
	);
	gen["MS2"] = cmd(
		"Use / Clear Top Invisible Message Box",
		"This is the mode for text at the top of the screen with no message box background."
	);
	gen["MS3"] = cmd(
		"Show / Clear Top Message Box",
		"This is the mode for ordinary text at the top of the screen."
	);
	gen["MS4"] = cmd(
		"Use / Clear Invisible Message Box",
		"This is the mode for text in the standard message box, but without a background."
	);
	gen["WAI"] = cmd(
		"Wait",
		"AAAA", "TICKS",
		"Waits for the specified number of frames (x/50th of a second)."
	);
	gen["WAS"] = cmd(
		"Wait Until Player Hits Ground",
		"Waits until the player hits the ground."
	);
	gen["TUR"] = cmd(
		"Instant Text Display",
		"Causes instant display of text. Note that all clear commands disable this."
	);
	gen["SAT"] = cmd(
		"Instant Text Display (Other)",
		"Apparently another method to cause fast display of text (not the same as &lt;TUR)."
	);
	gen["CAT"] = cmd(
		"Instant Text Display (Other)",
		"The same method of instant text display as &lt;SAT but not &lt;TUR."
	);
	gen["CLO"] = cmd(
		"Close Message Box",
		"Gets rid of the message box."
	);
	gen["EVE"] = cmd(
		"Jump",
		"AAAA", "LABEL",
		"Jumps to label A."
	);
	gen["YNJ"] = cmd(
		"Yes/No Jump",
		"AAAA", "LABEL",
		"Shows a yes/no dialog box. If 'no' is selected, jumps to label A."
	);
	gen["FLJ"] = cmd(
		"Flag Jump",
		"AAAA:BBBB", "flag", "LABEL",
		"If flag A is set, jumps to label B."
	);
	gen["SKJ"] = cmd(
		"Skipflag Jump",
		"AAAA:BBBB", "skipflag", "LABEL",
		"If skipflag A is set, jumps to label B."
	);
	gen["ITJ"] = cmd(
		"Item Jump",
		"AAAA:BBBB", "item", "LABEL",
		"If the player has item A, jumps to label B."
	);
	gen["AMJ"] = cmd(
		"Has Weapon Jump",
		"AAAA:BBBB", "weapon", "LABEL",
		"If player has weapon A, jumps to label B."
	);
	gen["UNJ"] = cmd(
		"Player Physics Jump",
		"AAAA:BBBB", "unit", "LABEL",
		"If player physics == A, jumps to label B."
	);
	gen["ECJ"] = cmd(
		"Entities Alive Jump - Event ID",
		"AAAA:BBBB", "EVENTID", "LABEL",
		"If at least one entity is alive with event ID A, jumps to label B."
	);
	gen["NCJ"] = cmd(
		"Entities Alive Jump - Entity Type",
		"AAAA:BBBB", "NPC", "LABEL",
		"If at least one entity is alive oftype A, jumps to label B."
	);
	gen["MPJ"] = cmd(
		"Map Flag Jump",
		"AAAA", "LABEL",
		MAPPING_FEATURE
		"If the current stage has this flag set, jumps to label A."
	);
	gen["SSS"] = cmd(
		"Play Stream Sound",
		"AAAA", "",
		"Plays a stream sound with pitch specified via A."
	);
	gen["CSS"] = cmd(
		"Cut Stream Sound",
		"Stops the stream or propeller sound."
	);
	gen["SPS"] = cmd(
		"Start Propeller Sound",
		"Starts the propeller sound."
	);
	gen["CPS"] = cmd(
		"Cut Propeller Sound",
		"Stops the stream or propeller sound."
	);
	gen["QUA"] = cmd(
		"Earthquake",
		"AAAA", "TICKS",
		"Causes an earthquake for A ticks."
	);
	gen["FLA"] = cmd(
		"Flash Screen",
		"Flashes the screen. For the love of all kittens that are huggable, don't abuse this."
	);
	gen["FAI"] = cmd(
		"Fade In",
		"AAAA", "fadeMode",
		"Fades in (brings the world into view). Direction is specified in A."
	);
	gen["FAO"] = cmd(
		"Fade Out",
		"AAAA", "fadeMode",
		"Fades out (brings the world out of view). Direction is specified in A."
	);
	gen["MNA"] = cmd(
		"Show Map Name",
		"Shows the map name."
	);
	gen["FOM"] = cmd(
		"Focus MyChar",
		"Sets the camera focus to the player."
	);
	gen["FON"] = cmd(
		"Focus NPC",
		"AAAA:BBBB", "EVENTID", "TICKS",
		"Sets the camera focus to an NPC with event ID A over B ticks."
	);
	gen["FOB"] = cmd(
		"Focus Boss",
		"AAAA:BBBB", "", "TICKS",
		"Sets the camera focus to boss component A over B ticks."
	);
	gen["SOU"] = cmd(
		"Sound",
		"AAAA", "sound",
		"Plays sound A."
	);
	gen["CMU"] = cmd(
		"Set Music",
		"AAAA", "music",
		"Switches to new music track A."
	);
	gen["FMU"] = cmd(
		"Fade Out Music",
		"Fades out the music."
	);
	gen["RMU"] = cmd(
		"Recall Music",
		"Plays whatever music was playing before the current track."
	);
	gen["MLP"] = cmd(
		"Open Map System",
		"Opens the Map System."
	);
	gen["SLP"] = cmd(
		"Open Stage Select / Teleporter Menu",
		"Opens the Stage Select (teleporter menu). This menu will cause a label to be jumped to."
	);
	gen["DNP"] = cmd(
		"Delete Entity & Set Flag",
		"AAAA", "EVENTID",
		"Deletes the entity with event ID A.<br/>The flag corresponding to the entity's flag ID is set."
	);
	gen["DNA"] = cmd(
		"Delete Entities & Set Flags",
		"AAAA", "NPC",
		"Deletes all entities of type A.<br/>The flags corresponding to the entity flag IDs are set."
	);
	gen["BOA"] = cmd(
		"Boss Command",
		"AAAA", "bossCommand",
		"Commands a boss to perform some boss-specific action."
	);
	gen["CNP"] = cmd(
		"Change Entity Type",
		"AAAA:BBBB:CCCC", "EVENTID", "NPC", "direction",
		"Changes entity with event ID A to have type B and direction C.<br/>"
		"Flags are reset.<br/>"
		"This also runs the entity tick function, presumably in order to setup new frame rects."
	);
	gen["ANP"] = cmd(
		"Entity Command",
		"AAAA:BBBB:CCCC", "EVENTID", "", "direction",
		"Sends command B to entity with event ID A (sets act_no) and changes direction to C.<br/>"
		"Commands are specific to the type of entity."
	);
	gen["INP"] = cmd(
		"Change Entity Type & Enable Interact Script",
		"AAAA:BBBB:CCCC", "EVENTID", "NPC", "direction",
		"Changes entity with event ID A to have type B and direction C.<br/>"
		"Flags are reset, but the Interact Script flag is always enabled.<br/>"
		"This also runs the entity tick function, presumably in order to setup new frame rects."
	);
	gen["SNP"] = cmd(
		"Create Entity",
		"AAAA:XXXX:YYYY:BBBB", "EVENTID", "X", "Y", "NPC",
		"Creates an entity with event ID A, type D, at position (X, Y)."
	);
	gen["MNP"] = cmd(
		"Move Entity",
		"AAAA:XXXX:YYYY:BBBB", "EVENTID", "X", "Y", "direction",
		"Moves entity with event ID A to position (X, Y) and sets direction to B."
	);
	gen["SMP"] = cmd(
		"Subtract 1 From Tile Number",
		"XXXX:YYYY", "X", "Y",
		"Subtracts 1 from the tile number at the given place. Keep in mind that the tile number will wrap to 255 if it is brought under 0."
	);
	gen["CMP"] = cmd(
		"Change Tile Number",
		"XXXX:YYYY:AAAA", "X", "Y", "",
		"Sets tile at (X, Y) to A. This generates smoke."
	);
	gen["BSL"] = cmd(
		"Boss Lifebar",
		"AAAA", "EVENTID",
		"Shows a life bar for event ID A.<br/>"
		"If A is 0, sets the life to the health of boss NPC 0."
	);
	gen["MYD"] = cmd(
		"MyChar Direction",
		"AAAA", "direction",
		"Sets player direction to A."
	);
	gen["MYB"] = cmd(
		"MyChar Backstep",
		"AAAA", "EVENTID",
		"If A is 0, player looks left but hops right.<br/>"
		"If A is 2, player looks right but hops left.<br/>"
		"If A is neither, then it is an event ID, and player hops away from that.<br/>"
		"Can be combined with an opposing &lt;MYD to produce a forward hop."
	);
	gen["MM0"] = cmd(
		"MyChar X Motion 0",
		"Halts all player X motion."
	);
	gen["INI"] = cmd(
		"Initialize",
		"Essentially the 'New Game' option."
	);
	gen["SVP"] = cmd(
		"Save Profile",
		"Saves the game to the default slot, Profile.dat."
	);
	gen["LDP"] = cmd(
		"Load Profile",
		"Loads the game, or if that fails, essentially performs New Game."
	);
	gen["FAC"] = cmd(
		"Face",
		"AAAA", "face",
		"Sets the face in the left side of the message box to A."
	);
	gen["GIT"] = cmd(
		"Show Item Image",
		"AAAA", "item",
		"Shows item A image."
	);
	gen["NUM"] = cmd(
		"Write Number",
		"AAAA", "numIndex",
		"Writes out the number in slot A."
	);
	gen["CRE"] = cmd(
		"Credits",
		"The end of the game. Note that Credits.tsc runs in parallel with whatever's going on here."
	);
	gen["SIL"] = cmd(
		"Set Credits Image",
		"AAAA", "creditsImage",
		"Sets credits image."
	);
	gen["CIL"] = cmd(
		"Cut Credits Image",
		"Removes credits image."
	);
	gen["XX1"] = cmd(
		"Island",
		"AAAA", "islandMode",
		"Performs island scene."
	);
	gen["ESC"] = cmd(
		"Title Screen",
		"Returns to the title screen."
	);
	// NON-VANILLA : CSE2EX
	gen["MIM"] = cmd(
		"[CSE2EX] Mimiga Mask (Player Skin)",
		"AAAA", "",
		"Changes the Mimiga Mask sub-mode to A."
	);
	gen["PHY"] = cmd(
		"[CSE2EX] Alter Player Physics Values",
		"AAAA:BBBB", "asmPhyKey", "",
		"Changes player physics value A to B."
	);
	// NON-VANILLA : CS+
	gen["ACH"] = cmd(
		"[CS+] Achievement",
		"AAAA", "",
		"Steam Achievements."
	);
	// NON-VANILLA : CS+ Switch
	gen["HM2"] = cmd(
		"[CS+ Switch] Hide Player 2",
		"Hides player 2."
	);
	gen["KE2"] = cmd(
		"[CS+ Switch] Unknown : KEY Player 2?",
		"More information is required."
	);
	gen["FR2"] = cmd(
		"[CS+ Switch] Unknown : FRE Player 2?",
		"More information is required."
	);
	// NON-VANILLA : CS+ Switch, doukutsu-rs
	gen["2MV"] = cmd(
		"[CS+ Switch, doukutsu-rs] Move Player 2",
		"AAAA", "direction",
		"Moves player 2 left or right of player 1 based on A."
	);
	gen["INJ"] = cmd(
		"[CS+ Switch, doukutsu-rs] Item Number Jump",
		"AAAA:BBBB:CCCC", "item", "", "LABEL",
		"Jumps to label C if the player has exactly amount B of item A."
	);
	gen["I+N"] = cmd(
		"[CS+ Switch, doukutsu-rs] Add Item X With Limit",
		"AAAA:BBBB", "item", "",
		"For each individual player, if they have less than B of item A, add one of it."
	);
	gen["FF-"] = cmd(
		"[CS+ Switch, doukutsu-rs] Clear Flag Range",
		"AAAA:BBBB", "flag", "flag",
		"Clears flags from A to B inclusive."
	);
	gen["PSH"] = cmd(
		"[CS+ Switch, doukutsu-rs] Push (Call)",
		"AAAA", "LABEL",
		"Calls label A - can return here with POP."
	);
	gen["POP"] = cmd(
		"[CS+ Switch, doukutsu-rs] Pop (Return)",
		"Returns from a PSH."
	);
	return gen;
}

const QJsonObject cdgtsclanguagemdl_tscbuiltin = cdgtsclanguagemdl_tscbuiltin_gen();
