#include <QLabel>
#include <QTextEdit>
#include <QJsonArray>
#include "cdgtsclanguagemdl.h"
#include "models/cdkgamemdl.h"
#include "pieces/cdptscnumerics.h"

CDKGAMEMDL_REGISTER_GLOBAL(tscLanguage) {
	return new CdgTSCLanguageMdl(mdl, data["commands"].toObject());
}

void CdgTSCCompletionNumberDatum::load(const QJsonObject & obj) {
	start = obj["start"].toInt();
	enumType = obj["enumType"].toString();
}
QJsonObject CdgTSCCompletionNumberDatum::save() const {
	QJsonObject obj;
	obj["start"] = start;
	obj["enumType"] = enumType;
	return obj;
}

void CdgTSCLanguageCommand::load(const QJsonObject & obj) {
	longName = obj["longName"].toString();
	parameterText = obj["parameterText"].toString();
	description = obj["description"].toString();
	for (QJsonValueRef val : obj["numbers"].toArray()) {
		CdgTSCCompletionNumberDatum datum;
		datum.load(val.toObject());
		numbers.append(datum);
	}
	isDefault = false;
}
QJsonObject CdgTSCLanguageCommand::save() const {
	QJsonObject obj;
	obj["longName"] = longName;
	obj["parameterText"] = parameterText;
	obj["description"] = description;
	QJsonArray numbs;
	for (const CdgTSCCompletionNumberDatum & num : numbers)
		numbs.append(num.save());
	obj["numbers"] = numbs;
	return obj;
}

CdgTSCLanguageMdl::CdgTSCLanguageMdl(CdkGameMdl * gameMdl, const QJsonObject & cmd) : CdkBaseLanguageMdl(gameMdl, "tsc") {
	for (const QString & ref : cdgtsclanguagemdl_tscbuiltin.keys()) {
		CdgTSCLanguageCommand cmdV;
		cmdV.name = ref;
		cmdV.load(cdgtsclanguagemdl_tscbuiltin[ref].toObject());
		cmdV.isDefault = true;
		commands[cmdV.name] = cmdV;
	}
	for (const QString & ref : cmd.keys()) {
		CdgTSCLanguageCommand cmdV;
		cmdV.name = ref;
		cmdV.load(cmd[ref].toObject());
		commands[cmdV.name] = cmdV;
	}
	maxCommandNameLength = 3;
	for (const CdgTSCLanguageCommand & mapCmd : commands.values())
		if (mapCmd.name.length() > maxCommandNameLength)
			maxCommandNameLength = mapCmd.name.length();
}

QJsonObject CdgTSCLanguageMdl::storeCfg() {
	QJsonObject obj;
	obj["type"] = "tscLanguage";
	QJsonObject cmds;
	for (const CdgTSCLanguageCommand & cmd : commands)
		if (!cmd.isDefault)
			cmds[cmd.name] = cmd.save();
	obj["commands"] = cmds;
	return obj;
}

QWidget * CdgTSCLanguageMdl::provideServices(QTextEdit * other) {
	new CdpTSCHighlighter(other->document(), this);
	return new CdpTSCNumerics(other, other, this);
}

#define HAS_AVAILABLE(N) ((i + N) <= (text.length()))

CdgTSCCompletionData CdgTSCLanguageMdl::provideCompletionData(const QString & text) {
	CdgTSCCompletionData datums;
	for (int i = 0; i < text.length(); i++) {
		if (text.at(i) == '#') {
			CdgTSCCompletionCommandDatum cmd(i, "#AAAA", "A label. This can be jumped to with, for example, &lt;EVE, or conditionally via &lt;FLJ or &lt;YNJ.");
			datums.commands.append(cmd);
			datums.numbers.append(CdgTSCCompletionNumberDatum(i + 1, "LABEL"));
			i += 4;
		} else if (text.at(i) == '<') {
			// ooo!
			if (HAS_AVAILABLE(4)) {
				QString cmdName = text.mid(i + 1, 3);
				// what command?
				if (commands.contains(cmdName)) {
					const CdgTSCLanguageCommand & cmdEnt = commands[cmdName];
					CdgTSCCompletionCommandDatum cmdDat(i, QLatin1String("<") + cmdEnt.name + cmdEnt.parameterText, cmdEnt.description);
					datums.commands.append(cmdDat);
					for (const CdgTSCCompletionNumberDatum & datum : cmdEnt.numbers)
						datums.numbers.append(datum.offset(i + 4));
					i += cmdEnt.parameterText.length() + 3;
				} else {
					CdgTSCCompletionCommandDatum cmd(i, "<???", "unknown command: " + cmdName);
					datums.commands.append(cmd);
				}
			}
		}
	}
	return datums;
}

CdpTSCHighlighter::CdpTSCHighlighter(QTextDocument * parent, CdgTSCLanguageMdl * mdl) : QSyntaxHighlighter(parent), model(mdl) {

}

void CdpTSCHighlighter::highlightBlock(const QString & text) {
	QTextCharFormat cmdFormat;
	cmdFormat.setForeground(Qt::blue);
	QTextCharFormat lblFormat;
	lblFormat.setFontWeight(QFont::Bold);
	QTextCharFormat numFormat;
	numFormat.setUnderlineStyle(QTextCharFormat::SingleUnderline);
	CdgTSCCompletionData cd = model->provideCompletionData(text);
	for (const CdgTSCCompletionCommandDatum & datum : cd.commands) {
		if (datum.fmtText.startsWith("#")) {
			setFormat(datum.start, datum.fmtText.length(), lblFormat);
		} else {
			setFormat(datum.start, datum.fmtText.length(), cmdFormat);
		}
	}
	for (const CdgTSCCompletionNumberDatum & datum : cd.numbers) {
		setFormat(datum.start, 4, numFormat);
	}
}
