#include "cdkbaselanguagemdl.h"
#include "models/cdkgamemdl.h"

CdkBaseLanguageMdl::CdkBaseLanguageMdl(CdkGameMdl * gameMdl, const QString & nm) : CdkBaseGlobalMdl(gameMdl), name(nm) {

}

CdkBaseLanguageMdl * CdkBaseLanguageMdl::attemptAcquireByName(CdkGameMdl * gameMdl, const QString & name) {
	for (CdkBaseGlobalMdl * mdl : gameMdl->gModels) {
		CdkBaseLanguageMdl * res = dynamic_cast<CdkBaseLanguageMdl *>(mdl);
		if (res)
			if (res->name == name)
				return res;
	}
	return nullptr;
}

void CdkBaseLanguageMdl::saveGlobal(CdExceptionTracker &) {

}
