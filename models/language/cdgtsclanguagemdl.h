#ifndef CDGTSCLANGUAGEMDL_H
#define CDGTSCLANGUAGEMDL_H

#include <QSyntaxHighlighter>
#include "cdkbaselanguagemdl.h"

// Represents a number.
class CdgTSCCompletionNumberDatum {
public:
	// note: enum strings "", "NPC", "EVENTID", "STAGE", "X", "Y", "TICKS" and "LABEL" are special
	CdgTSCCompletionNumberDatum() {}
	CdgTSCCompletionNumberDatum(int s, const QString & enu) : start(s), enumType(enu) {}
	int start;
	QString enumType;

	CdgTSCCompletionNumberDatum offset(int s) const {
		return CdgTSCCompletionNumberDatum(start + s, enumType);
	}

	void load(const QJsonObject & obj);
	QJsonObject save() const;
};

// Represents a command in the database.
class CdgTSCLanguageCommand {
public:
	// Name (not necessarily 3 characters!)
	QString name;
	// Long name for list
	QString longName;
	// full template parameter text (indicates length!)
	QString parameterText;
	QString description;
	bool isDefault = true;
	QList<CdgTSCCompletionNumberDatum> numbers;

	void load(const QJsonObject & obj);
	QJsonObject save() const;
};

class CdgTSCCompletionCommandDatum {
public:
	// note: enum strings "" and "LABEL" are special
	CdgTSCCompletionCommandDatum(int s, const QString & text, const QString & desc) : start(s), fmtText(text), description(desc) {}
	int start;
	QString fmtText;
	QString description;
};

class CdgTSCCompletionData {
public:
	QList<CdgTSCCompletionCommandDatum> commands;
	QList<CdgTSCCompletionNumberDatum> numbers;
};

extern const QJsonObject cdgtsclanguagemdl_tscbuiltin;

// TSC "half-parser"
class CdgTSCLanguageMdl : public CdkBaseLanguageMdl {
	Q_OBJECT
public:
	CdgTSCLanguageMdl(CdkGameMdl * gameMdl, const QJsonObject & cmd);
	QJsonObject storeCfg() override;
	QWidget * provideServices(QTextEdit * other) override;
	CdgTSCCompletionData provideCompletionData(const QString & line);

	int maxCommandNameLength;
	QMap<QString, CdgTSCLanguageCommand> commands;
};

class CdpTSCHighlighter : public QSyntaxHighlighter {
	Q_OBJECT

public:
	explicit CdpTSCHighlighter(QTextDocument * parent, CdgTSCLanguageMdl * mdl);
	void highlightBlock(const QString &text) override;
	CdgTSCLanguageMdl * model;
};

#endif // CDGTSCLANGUAGEMDL_H
