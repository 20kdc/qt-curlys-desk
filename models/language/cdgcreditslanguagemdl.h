#ifndef CDGCREDITSLANGUAGEMDL_H
#define CDGCREDITSLANGUAGEMDL_H

#include <QSyntaxHighlighter>
#include "cdkbaselanguagemdl.h"

class CdgCreditsLanguageMdl : public CdkBaseLanguageMdl {
	Q_OBJECT
public:
	CdgCreditsLanguageMdl(CdkGameMdl * mdl);
	QJsonObject storeCfg() override;
	QWidget * provideServices(QTextEdit * other) override;
};

class CdpCreditsHighlighter : public QSyntaxHighlighter {
	Q_OBJECT

public:
	explicit CdpCreditsHighlighter(QTextDocument * parent);
	void highlightBlock(const QString &text) override;
};

#endif // CDGCREDITSLANGUAGEMDL_H
