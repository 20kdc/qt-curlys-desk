#include <QLabel>
#include <QTextEdit>
#include "cdgcreditslanguagemdl.h"
#include "models/cdkgamemdl.h"

CDKGAMEMDL_REGISTER_GLOBAL(creditsLanguage) {
	return new CdgCreditsLanguageMdl(mdl);
}

CdgCreditsLanguageMdl::CdgCreditsLanguageMdl(CdkGameMdl * gameMdl) : CdkBaseLanguageMdl(gameMdl, "credits") {

}

QJsonObject CdgCreditsLanguageMdl::storeCfg() {
	QJsonObject obj;
	obj["type"] = "creditsLanguage";
	return obj;
}

QWidget * CdgCreditsLanguageMdl::provideServices(QTextEdit * other) {
	new CdpCreditsHighlighter(other->document());
	return new QLabel(
		"[X]YYYY - text X with cast image Y\n"
		"+XXXX - set horizontal margin\n"
		"-XXXX - wait / vertical space\n"
		"!XXXX - set music to X\n"
		"lXXXX - label\n"
		"jXXXX - jump to label X\n"
		"fXXXX:YYYY - if flag X, jump to label Y\n"
		"~ - fade music\n"
		"/ - terminate (all past this ignored)\n"
		"Do be aware that you can only jump forwards.\n"
		"Jumping backwards is not allowed.\n"
		"Further, the letter 'l' is always a label,\n"
		"and thus ordinary text may conflict.\n"
	);
}

CdpCreditsHighlighter::CdpCreditsHighlighter(QTextDocument * parent) : QSyntaxHighlighter(parent) {

}

#define HAS_AVAILABLE(N) ((i + N) <= (text.length()))

void CdpCreditsHighlighter::highlightBlock(const QString & text) {
	QTextCharFormat cmdFormat;
	cmdFormat.setForeground(Qt::blue);
	QTextCharFormat lblFormat;
	lblFormat.setFontWeight(QFont::Bold);
	QTextCharFormat imgFormat;
	imgFormat.setForeground(Qt::red);
	QTextCharFormat txtFormat;
	txtFormat.setFontItalic(true);
	int state = 0;
	int startText = 0;
	for (int i = 0; i < text.length(); i++) {
		QChar chr = text.at(i);
		if (state == 0) {
			if (chr == 'l') {
				if (HAS_AVAILABLE(5)) {
					setFormat(i, 5, lblFormat);
					i += 4;
				}
			} else if ((chr == 'j') || (chr == '+') || (chr == '-') || (chr == '!')) {
				if (HAS_AVAILABLE(5)) {
					setFormat(i, 5, cmdFormat);
					i += 4;
				}
			} else if (chr == 'f') {
				if (HAS_AVAILABLE(10)) {
					setFormat(i, 10, cmdFormat);
					i += 9;
				}
			} else if (chr == '[') {
				if (HAS_AVAILABLE(5)) {
					state = 1;
					startText = i + 1;
				}
			}
		} else if (state == 1) {
			if (chr == ']') {
				state = 0;
				setFormat(startText, i - startText, txtFormat);
				setFormat(i + 1, 4, imgFormat);
			}
		}
	}
}
