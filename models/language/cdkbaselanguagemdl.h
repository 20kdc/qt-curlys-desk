#ifndef CDKBASELANGUAGEMDL_H
#define CDKBASELANGUAGEMDL_H

#include "models/cdkbaseglobalmdl.h"

class QTextEdit;

// Provides completion, syntax highlighting, etc. support.
class CdkBaseLanguageMdl : public CdkBaseGlobalMdl {
	Q_OBJECT
public:
	CdkBaseLanguageMdl(CdkGameMdl * gameMdl, const QString & name);
	virtual QWidget * provideServices(QTextEdit * other) = 0;
	void saveGlobal(CdExceptionTracker & ex) override;
	static CdkBaseLanguageMdl * attemptAcquireByName(CdkGameMdl * gameMdl, const QString & name);
	const QString name;
};

#endif // CDKBASELANGUAGEMDL_H
