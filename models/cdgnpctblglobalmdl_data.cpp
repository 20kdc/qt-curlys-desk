#include "cdgnpctblglobalmdl.h"
#include <QJsonArray>
#include "cdcategory.h"

#define NO 0, 0, 0, 0

static QJsonObject cfg(int dl, int du, int dr, int dd, int il, int iu, int ir, int id) {
	QJsonObject main;
	main["frameL"] = dl;
	main["frameU"] = du;
	main["frameR"] = dr;
	main["frameD"] = dd;
	main["indicatorL"] = il;
	main["indicatorU"] = iu;
	main["indicatorR"] = ir;
	main["indicatorD"] = id;
	return main;
}

static QJsonObject cfgNudge(int dl, int du, int dr, int dd, int il, int iu, int ir, int id, int nudgeX, int nudgeY) {
	QJsonObject main;
	main["frameL"] = dl;
	main["frameU"] = du;
	main["frameR"] = dr;
	main["frameD"] = dd;
	main["indicatorL"] = il;
	main["indicatorU"] = iu;
	main["indicatorR"] = ir;
	main["indicatorD"] = id;
	main["nudgeX"] = nudgeX;
	main["nudgeY"] = nudgeY;
	return main;
}

static QJsonObject npc(const QString & name, const QString & sheet, int category, const QString & description, QJsonObject cfgA) {
	QJsonObject part;
	part["name"] = name;
	part["desc"] = description;
	part["category"] = category;
	part["sheet"] = sheet;
	part["cfgA"] = cfgA;
	part["cfgB"] = cfgA;
	return part;
}

static QJsonObject npc(const QString & name, const QString & sheet, int category, const QString & description, QJsonObject cfgA, QJsonObject cfgB) {
	QJsonObject part;
	part["name"] = name;
	part["desc"] = description;
	part["category"] = category;
	part["sheet"] = sheet;
	part["cfgA"] = cfgA;
	part["cfgB"] = cfgB;
	return part;
}

static QJsonObject cdgnpctbl_getConstantMetadata() {
	QJsonObject res;
	QJsonArray surfaces;
	surfaces.append("data/Title");       //  0
	surfaces.append("data/pixel");       //  1 doukutsu-rs extracted resource
	surfaces.append("MAP_TILESET");      //  2 fancy
	surfaces.append("NULL");             //  3
	surfaces.append("NULL");             //  4
	surfaces.append("NULL");             //  5
	surfaces.append("data/Fade");        //  6
	surfaces.append("NULL");             //  7
	surfaces.append("data/ItemImage");   //  8
	surfaces.append("NULL");             //  9  *
	surfaces.append("NULL");             //  10 *
	surfaces.append("data/Arms");        //  11
	surfaces.append("data/ArmsImage");   //  12
	surfaces.append("NULL");             //  13 *
	surfaces.append("data/StageImage");  //  14
	surfaces.append("data/Loading");     //  15 doukutsu-rs extracted resource?
	surfaces.append("data/MyChar");      //  16
	surfaces.append("data/Bullet");      //  17
	surfaces.append("NULL");             //  18
	surfaces.append("data/Caret");       //  19
	surfaces.append("data/Npc/NpcSym");  //  20
	surfaces.append("MAP_NPC1");         //  21
	surfaces.append("MAP_NPC2");         //  22
	surfaces.append("data/Npc/NpcRegu"); //  23
	surfaces.append("NULL");             //  24
	surfaces.append("NULL");             //  25
	surfaces.append("data/TextBox");     //  26
	surfaces.append("data/Face");        //  27
	surfaces.append("MAP_BACKGROUND");   //  28
	// there's more, but they're all best effectively null

	res["surfaces"] = surfaces;
	res["bkgPre"] = "data/";
	res["npcPre"] = "data/Npc/Npc";

#define ALT_DOWN_INDICATOR 2, 18, 14, 30
#define FURN1_TEXT "This is effectively furniture.\nAn interaction or collision script can add some functionality."
#define FURN2_TEXT "This is effectively furniture.\nOption 2 selects a different variant.\nAn interaction or collision script can add some functionality."
#define FURN3_TEXT "This is effectively furniture.\n"
#define COPY_PASTED_ITEM_BASE "\nAs usual for 'item' entities, it needs an interaction event attached to be of use."
#define COPY_PASTED_ITEM_CODE COPY_PASTED_ITEM_BASE "\nIf the direction is changed to 2, it will jump upwards and emit smoke."
#define NO_DESC "No description is available for this NPC."

	res["0"] = npc(
		"Null", "Npc/NpcSym", C_UTIL,
		"An invisible entity with no internal behaviour.\nCan act as a marker, as something interactable, etc.\nIf the direction flag is set, then this entity is automatically moved downwards by a tile.",
		cfg(0, 0, 16, 16, NO),
		cfg(0, 0, 16, 16, ALT_DOWN_INDICATOR)
	);
	res["1"] = npc(
		"Experience Point", "Npc/NpcSym", C_SPWN,
		"One of those bouncing triangles you get when you kill an enemy.\nThe amount of experience given by one of these when placed is determined by the experience field in npc.tbl.",
		cfg(0, 16, 16, 32, NO)
	);
	res["2"] = npc(
		"Behemoth", "Npc/NpcEggs1", C_EGGS,
		"Like elephants, but cuter.\nRelatively neutral. The player can stand on top of one and effectively ride it.\nIt can hurt the player, though.",
		cfg(32, 0, 64, 24, NO),
		cfg(32, 24, 64, 48, NO)
	);
	res["3"] = npc(
		"Dead Enemy", "Npc/NpcSym", C_INTN,
		"This entity deletes itself after 100 ticks.",
		cfg(0, 0, 0, 0, NO)
	);
	res["4"] = npc(
		"Smoke Particle", "Npc/NpcSym", C_SPWN,
		"A particle of smoke.",
		cfg(64, 0, 80, 16, NO),
		cfg(32, 128, 48, 144, NO)
	);
#define GREEN_CRITTER_JUMP_HEIGHT 32
#define GREEN_CRITTER_JUMP 6, 6 - GREEN_CRITTER_JUMP_HEIGHT, 10, 10
	res["5"] = npc(
		"Green Critter", "Npc/NpcEggs1", C_ECRT,
		"D'aww, it's so cute...\nJumps 3 tiles high when the player's nearby.",
		cfg(16, 48, 32, 64, GREEN_CRITTER_JUMP),
		cfg(16, 64, 32, 80, GREEN_CRITTER_JUMP)
	);
#define GREEN_BEETLE_FLY_RANGE 16
	res["6"] = npc(
		"Beetle (Green)", "Npc/NpcEggs1", C_EGGS,
		"Mostly harmless.\nFlies left and right in straight lines.",
		cfg(16, 80, 32, 96, NO),
		cfg(16, 96, 32, 112, NO)
	);
	res["7"] = npc(
		"Basil", "Npc/NpcEggs1", C_EGGS,
		"Instant death, but at least it's predictable.\nGoes left and right across the ground in a straight line.\nIf it hits the player, does 127 damage.",
		cfg(256, 64, 288, 80, NO),
		cfg(288, 64, 320, 80, NO)
	);
#define BLUE_BEETLE_FLY_RANGE 16
#define BLUE_BEETLE_FLY 7, 7 - BLUE_BEETLE_FLY_RANGE, 9, 9 + BLUE_BEETLE_FLY_RANGE
#define BALROG_LINE "\"Huzzah! There you are!\"\n"
#define BALROG_STANDARD_RECTS cfg(0, 0, 40, 24, NO), cfg(0, 24, 40, 48, NO)
	res["8"] = npc(
		"Beetle (Blue)", "Npc/NpcEggs1", C_EGGS,
		"Those things that deliberately fly into you.\nAfter the player gets in range, moves towards the player on the X axis, but hovers around it's present Y position.",
		cfg(80, 80, 96, 96, BLUE_BEETLE_FLY),
		cfg(80, 96, 96, 112, BLUE_BEETLE_FLY)
	);
	res["9"] = npc(
		"Balrog (Falls/Lands)", "Npc/NpcBllg", C_CHAR,
		BALROG_LINE "This iteration of Balrog is set to fall and crash into the ground on spawn.\nFor effective use, spawn aerially.",
		cfg(120, 0, 160, 24, NO),
		cfg(120, 24, 160, 48, NO)
	);
	res["10"] = npc(
		"Balrog (Shooting, Unused?)", "Npc/NpcBllg", C_CHAR,
		BALROG_LINE "This is supposedly unused (ref: CSE2EX)",
		BALROG_STANDARD_RECTS
	);
	res["11"] = npc(
		"Balrog Energy Bullet", "Npc/NpcSym", C_INTN,
		"This is supposedly spawned by 10 Balrog (Shooting, Unused?) (ref: CSE2EX).",
		cfg(208, 104, 224, 120, NO)
	);
	res["12"] = npc(
		"Balrog (Cutscene)", "Npc/NpcBllg", C_CHAR,
		BALROG_LINE "This is a highly configurable version of Balrog for cutscene use.\n"
		"Here is an untested listing of involved ANP numbers:\n"
		"0000: Idle\n"
		"0001, 0002: Internal (wait-for-blink/in-blink)\n"
		"0010: If direction is 4, turns towards player.\n"
		"      In any case, after 30 ticks, turns non-solid and goes flying upward.\n"
		"      Upon crossing map 'ceiling' (0), sound & quake occurs & NPC type changes to 0.\n"
		"0011, 0012: Internal (wait/in-flight)\n"
		"0020: If direction is 4, turns towards player.\n"
		"      In any case, emits smoke, then falls while shaking.\n"
		"      After 100 ticks if NPC flag 0x0008 is set, will switch to state 11.\n"
		"0021: Internal\n"
		"0030: Holds the :D sprite for ~100 ticks and then reverts to idle.\n"
		"      Despite being a 'major opcode', this doesn't reset the animation timer (CSE2EX bug?).\n"
		"      So in some cases you need to use other operations and waits to reset it.\n"
		"0040: If direction is 4, turns towards player.\n"
		"      In any case, rapidly begins switching between blank-eyes and dot-eyes (confirm?).\n"
		"0041: Internal\n"
		"0042: If direction is 4, turns towards player.\n"
		"      In any case, rapidly begins switching between dot-eyes and meh-face.\n"
		"0043: Internal\n"
		"0050: Faced away from the camera (towards BKG).\n"
		"0060: Faced away from the camera, doing something that makes noise, and moving.\n"
		"0061: Internal\n"
		"0070: Dot-eyes and makes a noise. Sets something to 0 after 64 ticks.\n"
		"0071: Internal\n"
		"0080: Shaking, blank-eyes.\n"
		"0081: Internal\n"
		"0100: Lowered-down. Does a lot of case-specific stuff\n"
		"0101, 0102: Internal\n",
		cfg(0, 0, 40, 24, NO)
	);
	res["13"] = npc(
		"Forcefield", "Npc/NpcSym", C_FURN,
		"The red force-field of impermeability.",
		cfg(128, 0, 144, 16, NO)
	);
	res["14"] = npc(
		"Key", "Npc/NpcSym", C_ITEM,
		"A key." COPY_PASTED_ITEM_CODE,
		cfg(224, 0, 240, 16, NO)
	);
	res["15"] = npc(
		"Chest (Closed)", "Npc/NpcSym", C_ITEM,
		"A closed chest." COPY_PASTED_ITEM_CODE "\nOther behaviour (open chest, etc.) must be completely manually created with TSC and conditional entities.",
		cfg(240, 0, 256, 16, NO)
	);
	res["16"] = npc(
		"Save Disk", "Npc/NpcSym", C_ITEM,
		"A spinning floppy disk." COPY_PASTED_ITEM_CODE "\nAssuming Head.tsc is intact, using event ID 0016 provides the standard behaviour.",
		cfg(96, 16, 112, 32, NO)
	);
	res["17"] = npc(
		"Health Refill", "Npc/NpcSym", C_ITEM,
		"A health refill." COPY_PASTED_ITEM_CODE "\nAssuming Head.tsc is intact, using event ID 0017 provides the standard behaviour.",
		cfg(288, 0, 304, 16, NO)
	);
	res["18"] = npc(
		"Door", "Npc/NpcSym", C_FURN,
		"It's a door. Note that on it's own, a door doesn't actually do anything.\n"
		"There are two things to consider with the use of ANP and directions with doors.\n"
		"If the door's direction is 2 (right, i.e. alt.dir is on, etc.) then the door appears opened.\n"
		"If the door is switched to ANP action 1, it emits some smoke.\n"
		"Another method you'll see of opening a door is to delete it.\n"
		"Example interaction TSC code for a door follows:\n"
		"#0100\n"
		"<PRI<ANP0100:0001:0002\n"
		"<SOU0011<FAO0004\n"
		"<TRA0001:0091:0030:0030"
		"Note that you need an 'entry script' in the other map for the target door.\n",
		cfg(224, 16, 240, 40, NO)
	);
	res["19"] = npc(
		"Balrog (Burst)", "Npc/NpcBllg", C_CHAR,
		BALROG_LINE "Upon spawning, emits smoke, makes noise, causes an earthquake, jumps.\nWill eventually settle down and idle.",
		BALROG_STANDARD_RECTS
	);
	res["20"] = npc(
		"Computer", "Npc/NpcSym", C_FURN,
		"Has an alternate direction.",
		cfg(288, 16, 320, 40, NO),
		cfg(288, 40, 320, 64, NO)
	);
	res["21"] = npc(
		"Chest (Open)", "Npc/NpcSym", C_ITEM,
		"An open chest.\nDoesn't have any behaviour.\nThe alternate direction pushes the chest down by a tile.",
		cfg(224, 40, 240, 48, NO),
		cfg(224, 40, 240, 48, ALT_DOWN_INDICATOR)
	);
	res["22"] = npc(
		"Teleporter", "Npc/NpcSym", C_FURN,
		"A teleporter.\nDoesn't have any behaviour.",
		cfg(240, 16, 264, 48, NO)
	);
	res["23"] = npc(
		"Teleporter Lights", "Npc/NpcSym", C_FURN,
		"The lights for the teleporter.\nDoesn't have any behaviour.",
		cfg(264, 16, 288, 20, NO)
	);
	res["24"] = npc(
		"Critter (Flying Grasstown)", "Npc/NpcWeed", C_ECRT,
		"Oh dear goodness IT CAN FLY PLEASE RUN AWAY.",
		cfg(0, 0, 24, 24, NO),
		cfg(0, 24, 24, 48, NO)
	);
	res["25"] = npc(
		"Lift", "Npc/NpcSym", C_FURN,
		"A lift. Pretty much runs itself. TODO: Show indicator!\nSee EggS (Egg Observation Room).",
		cfgNudge(256, 64, 288, 80, NO, 8, 0)
	);
	res["26"] = npc(
		"Bat (Flying)", "Npc/NpcWeed", C_EBAT,
		"Not another bat...",
		cfg(32, 80, 48, 96, NO),
		cfg(32, 96, 48, 112, NO)
	);
	res["27"] = npc(
		"Death Trap", "Npc/NpcSym", C_EVIL,
		"Because sometimes jumping puzzles need negative failure reinforcement.",
		cfg(96, 64, 128, 88, NO)
	);
	res["28"] = npc(
		"Critter (Grasstown)", "Npc/NpcWeed", C_ECRT,
		"At least it's not rainbow-coloured.",
		cfg(0, 48, 16, 64, NO),
		cfg(0, 64, 16, 80, NO)
	);
	res["29"] = npc(
		"Cthulhu", "Npc/NpcRegu", C_CHAR,
		"I shall give vague advice, yes.",
		cfg(0, 192, 16, 216, NO),
		cfg(0, 216, 16, 240, NO)
	);
	res["30"] = npc(
		"Hermit Gunsmith", "Npc/NpcGuest", C_CHAR,
		"\"I mean, you stole my gun, but you're also beating up all of that nasty stuff all over the place, so we're cool.\"",
		cfg(48, 0, 64, 16, NO)
	);
	res["31"] = npc(
		"Bat (Dark Green / Hanging)", "Npc/NpcWeed", C_EBAT,
		"This is what a \"drop bear\" looks and acts like.",
		cfg(0, 80, 16, 96, NO),
		cfg(0, 96, 16, 112, NO)
	);
	res["32"] = npc(
		"Life Capsule", "Npc/NpcSym", C_ITEM,
		"A life capsule." COPY_PASTED_ITEM_BASE,
		cfg(32, 96, 48, 112, NO)
	);
	res["33"] = npc(
		"Balrog Bouncing Projectile", "Npc/NpcBllg", C_INTN,
		"One of Balrog's projectiles.",
		cfg(240, 64, 256, 80, NO)
	);
	res["34"] = npc(
		"Bed", "Npc/NpcSym", C_FURN,
		"A bed. Keep in mind you need an interaction script to make it do anything.\n"
		"If Head.tsc is intact, event ID 0019 will provide the standard behaviour.\n"
		"There's an alternate direction for this, which looks more 'broken'.",
		cfg(192, 48, 224, 64, NO)
	);
	res["35"] = npc(
		"Mannan", "Npc/NpcWeed", C_GTWN,
		"Evil gravestone. Look at the visuals, you know what it is.",
		cfg(96, 64, 120, 96, NO),
		cfg(96, 96, 120, 128, NO)
	);
	res["36"] = npc(
		"Balrog 2 (25 : Malco : Hover)", "Npc/NpcBllg", C_CHAR,
		BALROG_LINE "That one you probably forgot about.",
		BALROG_STANDARD_RECTS
	);
	res["37"] = npc(
		"Sign", "Npc/NpcSym", C_FURN,
		FURN2_TEXT,
		cfg(192, 64, 208, 80, NO)
	);
	res["38"] = npc(
		"Fireplace", "Npc/NpcSym", C_FURN,
		"This is a little fire.\nANP 0000 turns the fire on, ANP 0010 turns it off with a little smoke effect.",
		cfg(128, 64, 144, 80, NO)
	);
	res["39"] = npc(
		"Save Sign", "Npc/NpcSym", C_FURN,
		FURN1_TEXT,
		cfg(224, 64, 240, 80, NO)
	);
#define NPC_BASE_ANPS \
		"ANP:\n" \
		"0000: Idle\n" \
		"0001, 0002: Internal\n" \
		"0003: Walk\n" \
		"0004: Internal\n" \
		"0005: Look away from camera\n"

	res["40"] = npc(
		"Santa", "Npc/NpcRegu", C_CHAR,
		"Not that one.\n"
		NPC_BASE_ANPS,
		cfg(0, 32, 16, 48, NO),
		cfg(0, 48, 16, 64, NO)
	);
	res["41"] = npc(
		"Door (Broken)", "Stage/PrtMimi", C_FURN,
		"Mostly furniture. But teleports itself up automatically.",
		// the logic behind this is a bit odd but you'll come to understand it
		cfgNudge(0, 80, 48, 112, -16, -32, 32, 0, 0, 16)
	);
	res["42"] = npc(
		"Sue", "Npc/NpcGuest", C_CHAR,
		"Like a puppy, but talks. D'awwww.\n"
		NPC_BASE_ANPS
		"0006: Oof\n"
		"0007: Internal\n"
		"0008: Jump\n"
		"0009, 0010: Internal\n"
		"0011: Flip between two states quickly\n"
		"0012: Internal\n"
		"0013: Find NPC with event ID 501, look in the opposite direction to them, positions self 4 pixels below them, with adjustments\n"
		"0014: Internal\n"
		"0015: Creates two Red Crystals and holds super position near self\n"
		"0016: Internal\n"
		"0017: Holds super position just above self\n"
		"0020: Generic walk code until she reaches just left of player.\n"
		"0021: Internal\n"
		"0030: Run\n"
		"0031: Internal\n"
		"0040: Jump\n",
		cfg(0, 0, 16, 16, NO),
		cfg(0, 16, 16, 32, NO)
	);
	res["43"] = npc(
		"Chalkboard", "Npc/NpcSym", C_FURN,
		FURN2_TEXT,
		cfgNudge(128, 80, 168, 112, NO, 0, -16),
		cfgNudge(168, 80, 208, 112, NO, 0, -16)
	);
	res["44"] = npc(
		"Polish", "Npc/NpcSand", C_SZON,
		"Spinny blades.",
		cfg(0, 0, 32, 32, NO)
	);
	res["45"] = npc(
		"Baby", "Npc/NpcSand", C_SZON,
		"Isn't it ccuuuuuu-AAAAAAAAAAAAAAAAA",
		cfg(0, 32, 16, 48,  NO)
	);
	res["46"] = npc(
		"H/V Trigger, Timer", "Npc/NpcSym", C_UTIL,
		"This is a bit of a weird one.\n"
		"Firstly, it always has the script-on-collide flag set.\n"
		"Secondly, it works by moving itself towards the player by a fixed amount on a given axis.\n"
		"If 'facing left', it moves on the X axis. If 'facing right' (opt 2), it moves on the Y axis.\n"
		"Something to keep in mind is that you can use this behaviour to setup a timer.\n"
		"If you move the trigger far away from the player, it takes a while for it to get back to the player.",
		cfg(0, 0, 16, 16, NO)
	);
	res["47"] = npc(
		"Sandcroc", "Npc/NpcSand", C_SZON,
		"Enemy in the Sand Zone. Invisible, moving left/right at 2 pixels per tick towards the player, then when the player gets too close, chomp!",
		cfg(48, 48, 96, 80, NO)
	);
	res["48"] = npc(
		"Omega Projectiles", "Npc/NpcSand", C_INTN,
		NO_DESC,
		cfg(288, 88, 304, 104, NO),
		cfg(288, 104, 304, 120, NO)
	);
	res["49"] = npc(
		"Skullhead", "Npc/NpcSand", C_SZON,
		"Enemy in the Sand Zone.",
		cfg(0, 80, 32, 104, NO),
		cfg(0, 104, 32, 128, NO)
	);
	res["50"] = npc(
		"Skeleton Projectile", "Npc/NpcSand", C_INTN,
		NO_DESC,
		cfg(48, 32, 64, 48, NO)
	);
	res["51"] = npc(
		"Crow And Skullhead", "Npc/NpcSand", C_INTN,
		"This is where the NPCs that heavily use parenting start.",
		cfg(96, 80, 128, 112, NO),
		cfg(96, 112, 128, 144, NO)
	);
	res["52"] = npc(
		"Blue Robot (Sitting)", "Npc/NpcRegu", C_CHAR,
		FURN1_TEXT,
		cfg(240, 96, 256, 112, NO)
	);
	res["53"] = npc(
		"Skullstep Leg", "Npc/NpcSand", C_INTN,
		NO_DESC,
		cfg(0, 128, 24, 144, NO),
		cfg(48, 128, 72, 144, NO)
	);
	res["54"] = npc(
		"Skullstep", "Npc/NpcSand", C_SZON,
		"Enemy in the Sand Zone. Oh, it can walk now.",
		cfg(0, 80, 32, 104, NO),
		cfg(0, 104, 32, 128, NO)
	);
	res["55"] = npc(
		"Kazuma", "Npc/NpcRegu", C_CHAR,
		NPC_BASE_ANPS,
		cfg(192, 192, 208, 216, NO),
		cfg(192, 216, 208, 240, NO)
	);
	res["56"] = npc(
		"Beetle (Tan)", "Npc/NpcSand", C_SZON,
		"Enemy in the Sand Zone.",
		cfg(0, 144, 16, 160, NO),
		cfg(0, 160, 16, 176, NO)
	);
	res["57"] = npc(
		"Crow", "Npc/NpcSand", C_SZON,
		"Enemy in the Sand Zone.",
		cfg(96, 80, 128, 112, NO),
		cfg(96, 112, 128, 144, NO)
	);
	res["58"] = npc(
		"Basu", "Npc/NpcEggs1", C_EGGS,
		"Bigger flies.",
		cfg(192, 0, 216, 24, NO),
		cfg(192, 24, 216, 48, NO)
	);
	res["59"] = npc(
		"Eye Door", "Npc/NpcSym", C_EVIL,
		"You thought living doors were safe. Then this one showed up.",
		cfg(208, 80, 224, 104, NO)
	);
	res["60"] = npc(
		"Toroko", "Npc/NpcRegu", C_CHAR,
		"ANP:\n"
		"0000: Idle\n"
		"0001, 0002: Internal\n"
		"0003: Walk\n"
		"0004: Internal\n"
		"0006: Jumps in a given direction."
		"0007: Internal"
		"0008: Jumps upwards."
		"0009: Internal"
		"0010: Jumps in a given direction. Becomes interactable once hitting the ground."
		"0011: Internal"
		"0012: Cancel X motion",
		cfg(0, 64, 16, 80, NO),
		cfg(0, 80, 16, 96, NO)
	);
	res["61"] = npc(
		"King", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(224, 32, 240, 48, NO),
		cfg(224, 48, 240, 64, NO)
	);
	res["62"] = npc(
		"Kazuma On Computer", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfgNudge(272, 192, 288, 216, NO, -4, 16)
	);
	res["63"] = npc(
		"Toroko w/ Stick", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(64, 64, 80, 80, NO),
		cfg(64, 80, 80, 96, NO)
	);
	res["64"] = npc(
		"Critter (Blue)", "Npc/NpcCemet", C_ECRT,
		NO_DESC,
		cfg(0, 0, 16, 16, NO),
		cfg(0, 16, 16, 32, NO)
	);
#define BLUE_BAT_FLY_RANGE 32
#define BLUE_BAT_FLY 7, 7 - BLUE_BAT_FLY_RANGE, 9, 9 + BLUE_BAT_FLY_RANGE
	res["65"] = npc(
		"Bat (Blue)", "Npc/NpcCemet", C_EBAT,
		NO_DESC,
		cfg(32, 32, 48, 48, BLUE_BAT_FLY),
		cfg(32, 48, 48, 64, BLUE_BAT_FLY)
	);
	res["66"] = npc(
		"Misery Bubble", "Npc/NpcRegu", C_SPWN,
		NO_DESC,
		cfg(32, 192, 56, 216, NO)
	);
	res["67"] = npc(
		"Misery (Floating)", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(80, 0, 96, 16, NO),
		cfg(80, 16, 96, 32, NO)
	);
	res["68"] = npc(
		"Balrog 1 (14 : Barr : Run, Run, Jump)", "Npc/NpcBllg", C_CHAR,
		BALROG_LINE "This iteration of Balrog runs left, right, and jumps at the player.",
		BALROG_STANDARD_RECTS
	);
	res["69"] = npc(
		"Pignon", "Npc/NpcCemet", C_GYRD,
		NO_DESC,
		cfg(48, 0, 64, 16, NO),
		cfg(48, 16, 64, 32, NO)
	);
	res["70"] = npc(
		"Sparkle", "Npc/NpcSym", C_ITEM,
		NO_DESC,
		cfg(96, 48, 112, 64, NO)
	);
	res["71"] = npc(
		"Chinfish", "Npc/NpcGuest", C_EVIL,
		NO_DESC,
		cfg(64, 32, 80, 48, NO),
		cfg(64, 48, 80, 64, NO)
	);
	res["72"] = npc(
		"Sprinkler", "Npc/NpcSym", C_FURN,
		NO_DESC,
		cfg(224, 48, 240, 64, NO)
	);
	res["73"] = npc(
		"Water Droplet", "Caret", C_SPWN,
		NO_DESC,
		cfg(72, 16, 74, 18, NO)
	);
	res["74"] = npc(
		"Jack", "Npc/NpcGuest", C_CHAR,
		NO_DESC,
		cfg(64, 0, 80, 16, NO),
		cfg(64, 16, 80, 32, NO)
	);
	res["75"] = npc(
		"Kanpachi", "Npc/NpcGuest", C_CHAR,
		"Not furniture! Really!",
		cfg(272, 32, 296, 56, NO)
	);
	res["76"] = npc(
		"Flowers", "Npc/NpcPlant", C_ITEM,
		"Has interesting behaviour: The event ID chooses the sprite.",
		cfg(0, 0, 16, 16, NO)
	);
	res["77"] = npc(
		"Yamashita", "Npc/NpcPlant", C_CHAR,
		"Not furniture! Really!",
		cfg(0, 16, 48, 48, NO)
	);
	res["78"] = npc(
		"Pot", "Npc/NpcSym", C_FURN,
		FURN2_TEXT,
		cfg(160, 48, 176, 64, NO)
	);
	res["79"] = npc(
		"Mahin", "Npc/NpcGuest", C_CHAR,
		NO_DESC,
		cfg(0, 0, 16, 16, NO),
		cfg(0, 16, 16, 32, NO)
	);
	res["80"] = npc(
		"Gravekeeper", "Npc/NpcCemet", C_GYRD,
		NO_DESC,
		cfg(0, 64, 24, 88, NO),
		cfg(0, 88, 24, 112, NO)
	);
	res["81"] = npc(
		"Giant Pignon", "Npc/NpcCemet", C_GYRD,
		NO_DESC,
		cfg(144, 64, 168, 88, NO),
		cfg(144, 88, 168, 112, NO)
	);
	res["82"] = npc(
		"Misery (Standing)", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(80, 0, 96, 16, NO),
		cfg(80, 16, 96, 32, NO)
	);
	res["83"] = npc(
		"Igor (Cutscene)", "Npc/NpcRavil", C_CHAR,
		NO_DESC,
		cfg(0, 0, 40, 40, NO),
		cfg(0, 40, 40, 80, NO)
	);
	res["84"] = npc(
		"Basu Projectile", "Npc/NpcEggs1", C_INTN,
		NO_DESC,
		cfg(48, 48, 64, 64, NO)
	);
	res["85"] = npc(
		"Terminal", "Npc/NpcSym", C_FURN,
		NO_DESC,
		cfg(256, 96, 272, 120, NO),
		cfg(256, 96, 272, 120, NO)
	);
	res["86"] = npc(
		"Missile Pickup", "Npc/NpcSym", C_SPWN,
		NO_DESC,
		cfg(0, 80, 16, 96, NO)
	);
	res["87"] = npc(
		"Heart Pickup", "Npc/NpcSym", C_SPWN,
		NO_DESC,
		cfg(32, 80, 48, 96, NO)
	);
	res["88"] = npc(
		"Igor (Boss)", "Npc/NpcRavil", C_CHAR,
		NO_DESC,
		cfg(0, 0, 40, 40, NO),
		cfg(0, 40, 40, 80, NO)
	);
	res["89"] = npc(
		"Igor (Dead)", "Npc/NpcRavil", C_CHAR,
		NO_DESC,
		cfg(80, 80, 120, 120, NO),
		cfg(200, 80, 240, 120, NO)
	);
	res["90"] = npc(
		"Background", "Npc/NpcWeed", C_FURN,
		NO_DESC,
		cfg(280, 80, 296, 104, NO)
	);
	res["91"] = npc(
		"Cage Bars (2x1.5, Mimiga Village)", "Npc/NpcSym", C_FURN,
		NO_DESC,
		cfg(96, 88, 128, 112, NO)
	);
	res["92"] = npc(
		"Sue At Computer", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(272, 216, 288, 240, NO)
	);
	res["93"] = npc(
		"Chaco", "Npc/NpcGuest", C_CHAR,
		NO_DESC,
		cfg(128, 0, 144, 16, NO),
		cfg(128, 16, 144, 32, NO)
	);
	res["94"] = npc(
		"Kulala", "Npc/NpcWeed", C_GTWN,
		NO_DESC,
		cfg(272, 0, 320, 24, NO)
	);
	res["95"] = npc(
		"Jelly", "Npc/NpcWeed", C_GTWN,
		NO_DESC,
		cfg(208, 64, 224, 80, NO),
		cfg(208, 80, 224, 96, NO)
	);

#define FAN_ANP \
	"One of the Grasstown fans.\n" \
	"ANP:" \
	"0000: Off\n" \
	"0002: On"

	res["96"] = npc(
		"Fan (Left)", "Npc/NpcSym", C_FURN,
		FAN_ANP,
		cfg(272, 120, 288, 136, NO)
	);
	res["97"] = npc(
		"Fan (Up)", "Npc/NpcSym", C_FURN,
		FAN_ANP,
		cfg(272, 136, 288, 152, NO)
	);
	res["98"] = npc(
		"Fan (Right)", "Npc/NpcSym", C_FURN,
		FAN_ANP,
		cfg(272, 152, 288, 168, NO)
	);
	res["99"] = npc(
		"Fan (Down)", "Npc/NpcSym", C_FURN,
		FAN_ANP,
		cfg(272, 168, 288, 184, NO)
	);
	res["100"] = npc(
		"Grate", "Npc/NpcSym", C_FURN,
		FURN1_TEXT,
		cfgNudge(272, 48, 288, 64, NO, 0, 16)
	);
	res["101"] = npc(
		"Malco Screen", "Npc/NpcSym", C_FURN,
		NO_DESC,
		cfg(240, 136, 256, 152, NO)
	);
	res["102"] = npc(
		"Malco Computer Wave", "Npc/NpcSym", C_FURN,
		NO_DESC,
		cfg(208, 120, 224, 136, NO)
	);
	res["103"] = npc(
		"Mannan Projectile", "Npc/NpcWeed", C_INTN,
		NO_DESC,
		cfg(192, 96, 208, 120, NO),
		cfg(192, 120, 208, 144, NO)
	);
	res["104"] = npc(
		"Frog", "Npc/NpcWeed", C_GTWN,
		NO_DESC,
		cfg(0, 112, 32, 144, NO),
		cfg(0, 144, 32, 176, NO)
	);
	res["105"] = npc(
		"Hey Bubble Low", "Npc/NpcSym", C_SPWN,
		NO_DESC,
		cfg(128, 32, 144, 48, NO)
	);
	res["106"] = npc(
		"Hey Bubble High", "Npc/NpcSym", C_SPWN,
		NO_DESC,
		cfg(0, 0, 0, 0, NO)
	);
	res["107"] = npc(
		"Malco Broken", "Npc/NpcWeed", C_CHAR,
		NO_DESC,
		cfg(144, 0, 160, 24, NO)
	);
	res["108"] = npc(
		"Balfrog Projectile", "Npc/NpcWeed", C_INTB,
		NO_DESC,
		cfg(96, 48, 112, 64, NO)
	);
	res["109"] = npc(
		"Malco Powered On", "Npc/NpcWeed", C_CHAR,
		NO_DESC,
		cfg(240, 0, 256, 24, NO),
		cfg(240, 24, 256, 48, NO)
	);
	res["110"] = npc(
		"Puchi", "Npc/NpcWeed", C_EVIL,
		NO_DESC,
		cfg(96, 128, 112, 144, NO),
		cfg(96, 144, 112, 160, NO)
	);
	res["111"] = npc(
		"Quote Teleport Out", "MyChar", C_SPWN,
		NO_DESC,
		cfg(0, 0, 16, 16, NO)
	);
	res["112"] = npc(
		"Quote Teleport In", "MyChar", C_SPWN,
		NO_DESC,
		cfg(0, 0, 16, 16, NO)
	);
	res["113"] = npc(
		"Professor Booster A", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(224, 0, 240, 16, NO),
		cfg(224, 16, 240, 32, NO)
	);
	res["114"] = npc(
		"Press", "Npc/NpcSym", C_HELL,
		NO_DESC,
		cfgNudge(144, 112, 160, 136, NO, 0, -4)
	);
	res["115"] = npc(
		"Ravil", "Npc/NpcRavil", C_EVIL,
		NO_DESC,
		cfg(0, 120, 24, 144, NO),
		cfg(0, 144, 24, 168, NO)
	);
	res["116"] = npc(
		"Red Petals", "Npc/NpcSym", C_FURN,
		NO_DESC,
		cfg(272, 184, 320, 200, NO)
	);
	res["117"] = npc(
		"Curly Brace", "Npc/NpcRegu", C_CHAR,
		"Not the drink. TODO THIS IS A DUMMY ENTRY",
		cfg(0, 96, 16, 112, NO),
		cfg(0, 112, 16, 128, NO)
	);
	res["118"] = npc(
		"Curly Brace (Boss)", "Npc/NpcCurly", C_CHAR,
		"Not the drink. TODO THIS IS A DUMMY ENTRY",
		cfg(0, 32, 32, 56, NO),
		cfg(0, 56, 32, 80, NO)
	);
	res["119"] = npc(
		"Table + Chair", "Npc/NpcSym", C_FURN,
		NO_DESC,
		cfg(248, 184, 272, 200, NO)
	);
	res["120"] = npc(
		"Colon A", "Npc/NpcCurly", C_CHAR,
		NO_DESC,
		cfg(64, 0, 80, 16, NO)
	);
	res["121"] = npc(
		"Colon B", "Npc/NpcCurly", C_CHAR,
		NO_DESC,
		cfg(0, 0, 16, 16, NO)
	);
	res["122"] = npc(
		"Colon (Attacking)", "Npc/NpcCurly", C_CHAR,
		NO_DESC,
		cfg(0, 0, 16, 16, NO),
		cfg(0, 16, 16, 32, NO)
	);
	res["123"] = npc(
		"Curly Projectile", "Npc/NpcCurly", C_INTN,
		NO_DESC,
		cfg(192, 0, 208, 16, NO)
	);
	res["124"] = npc(
		"Sunstone", "Npc/NpcSand", C_SZON,
		NO_DESC,
		cfgNudge(160, 0, 192, 32, NO, 8, 8)
	);
	res["125"] = npc(
		"Hidden Item", "Npc/NpcSym", C_ITEM,
		NO_DESC,
		cfg(0, 96, 16, 112, NO)
	);
	res["126"] = npc(
		"Puppy A", "Npc/NpcSand", C_CHAR,
		NO_DESC,
		cfg(48, 144, 64, 160, NO),
		cfg(48, 160, 64, 176, NO)
	);
	res["127"] = npc(
		"Machine Gun Trail 2", "Caret", C_INTN,
		NO_DESC,
		cfg(112, 48, 128, 64, NO)
	);
	res["128"] = npc(
		"Machine Gun Trail 3", "Caret", C_INTN,
		NO_DESC,
		cfg(176, 16, 184, 32, NO)
	);
	res["129"] = npc(
		"Fireball Trail", "Caret", C_INTN,
		NO_DESC,
		cfg(128, 48, 144, 64, NO)
	);
	res["130"] = npc(
		"Puppy B", "Npc/NpcSand", C_CHAR,
		NO_DESC,
		cfg(48, 144, 64, 160, NO),
		cfg(48, 160, 64, 176, NO)
	);
	res["131"] = npc(
		"Puppy C", "Npc/NpcSand", C_CHAR,
		NO_DESC,
		cfg(144, 144, 160, 160, NO),
		cfg(144, 160, 160, 176, NO)
	);
	res["132"] = npc(
		"Puppy D", "Npc/NpcSand", C_CHAR,
		NO_DESC,
		cfg(48, 144, 64, 160, NO),
		cfg(48, 160, 64, 176, NO)
	);
	res["133"] = npc(
		"Jenka", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(176, 32, 192, 48, NO),
		cfg(176, 48, 192, 64, NO)
	);
	res["134"] = npc(
		"Armadillo", "Npc/NpcSand", C_SZON,
		NO_DESC,
		cfg(224, 0, 256, 16, NO),
		cfg(224, 16, 256, 32, NO)
	);
	res["135"] = npc(
		"Skeleton", "Npc/NpcSand", C_SZON,
		NO_DESC,
		cfg(256, 32, 288, 64, NO),
		cfg(256, 64, 288, 96, NO)
	);
	res["136"] = npc(
		"Puppy E", "Npc/NpcSand", C_CHAR,
		NO_DESC,
		cfg(48, 144, 64, 160, NO),
		cfg(48, 160, 64, 176, NO)
	);
	res["137"] = npc(
		"Door (Large, Frame)", "Npc/NpcSym", C_FURN,
		NO_DESC,
		cfg(96, 136, 128, 188, NO)
	);
	res["138"] = npc(
		"Door (Large)", "Npc/NpcSym", C_FURN,
		NO_DESC,
		cfg(96, 112, 112, 136, NO),
		cfg(112, 112, 128, 136, NO)
	);
	res["139"] = npc(
		"Doctor", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(0, 128, 24, 160, NO),
		cfg(0, 160, 24, 192, NO)
	);
	res["140"] = npc(
		"Toroko (Frenzied)", "Npc/NpcToro", C_CHAR,
		NO_DESC,
		cfg(0, 0, 32, 32, NO),
		cfg(0, 32, 32, 64, NO)
	);
	res["141"] = npc(
		"Falling Block (Small)", "Npc/NpcToro", C_SPWN,
		NO_DESC,
		cfg(288, 32, 304, 48, NO)
	);
	res["142"] = npc(
		"Flower Cub", "Npc/NpcToro", C_INTB,
		NO_DESC,
		cfg(0, 128, 16, 144, NO)
	);
	res["143"] = npc(
		"Jenka (Collapsed)", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(208, 32, 224, 48, NO),
		cfg(208, 48, 224, 64, NO)
	);
	res["144"] = npc(
		"Toroko Teleport In", "Npc/NpcRegu", C_SPWN,
		NO_DESC,
		cfg(0, 64, 16, 80, NO),
		cfg(0, 80, 16, 96, NO)
	);
	res["145"] = npc(
		"King's Sword", "Npc/NpcSym", C_ITEM,
		NO_DESC,
		cfg(96, 32, 112, 48, NO),
		cfg(112, 32, 128, 48, NO)
	);
	res["146"] = npc(
		"Lightning", "Caret", C_INTN,
		NO_DESC,
		cfg(256, 0, 272, 240, NO)
	);
	res["147"] = npc(
		"Critter (Purple)", "Npc/NpcMaze", C_LABY,
		NO_DESC,
		cfg(0, 96, 16, 112, NO),
		cfg(0, 112, 16, 128, NO)
	);
	res["148"] = npc(
		"Critter (Purple) Projectile", "Npc/NpcMaze", C_INTN,
		NO_DESC,
		cfg(96, 96, 104, 104, NO)
	);
	res["149"] = npc(
		"Moving Block (Horizontal)", "Stage/PrtMaze", C_FURN,
		NO_DESC,
		cfgNudge(16, 0, 48, 32, NO, 8, 8)
	);
	res["150"] = npc(
		"Quote", "MyChar", C_CHAR,
		NO_DESC,
		cfg(0, 0, 16, 16, NO),
		cfg(0, 16, 16, 32, NO)
	);
	res["151"] = npc(
		"Blue Robot (Standing)", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(192, 0, 208, 16, NO),
		cfg(192, 16, 208, 32, NO)
	);
	res["152"] = npc(
		"Stuck Shutter", "Npc/NpcSym", C_UTIL,
		NO_DESC,
		cfg(0, 0, 0, 0, NO)
	);
	res["153"] = npc(
		"Gaudi", "Npc/NpcMaze", C_LABY,
		NO_DESC,
		cfg(0, 0, 24, 24, NO),
		cfg(0, 24, 24, 48, NO)
	);
	res["154"] = npc(
		"Gaudi (Dead)", "Npc/NpcMaze", C_LABY,
		NO_DESC,
		cfg(0, 0, 24, 24, NO),
		cfg(0, 24, 24, 48, NO)
	);
	res["155"] = npc(
		"Gaudi (Flying)", "Npc/NpcMaze", C_LABY,
		NO_DESC,
		cfg(0, 0, 24, 24, NO),
		cfg(0, 24, 24, 48, NO)
	);
	res["156"] = npc(
		"Gaudi Projectile", "Npc/NpcMaze", C_INTN,
		NO_DESC,
		cfg(96, 112, 112, 128, NO)
	);
	res["157"] = npc(
		"Moving Block (Vertical)", "Stage/PrtMaze", C_FURN,
		NO_DESC,
		cfgNudge(16, 0, 48, 32, NO, 8, 8)
	);
	res["158"] = npc(
		"Fish Missile", "Npc/NpcX", C_INTN,
		NO_DESC,
		cfg(0, 224, 16, 240, NO)
	);
	res["159"] = npc(
		"Monster X (Defeated)", "Npc/NpcX", C_INTB,
		NO_DESC,
		cfg(144, 128, 192, 200, NO)
	);
	res["160"] = npc(
		"Puu Black", "Npc/NpcDark", C_LABY,
		NO_DESC,
		cfg(0, 0, 40, 24, NO),
		cfg(0, 24, 40, 48, NO)
	);
	res["161"] = npc(
		"Puu Black Projectile", "Npc/NpcDark", C_INTN,
		NO_DESC,
		cfg(0, 48, 16, 64, NO)
	);
	res["162"] = npc(
		"Puu Black (Dead)", "Npc/NpcDark", C_INTN,
		NO_DESC,
		cfg(40, 0, 80, 24, NO),
		cfg(40, 24, 80, 48, NO)
	);
	res["163"] = npc(
		"Dr Gero", "Npc/NpcGuest", C_CHAR,
		NO_DESC,
		cfg(192, 0, 208, 16, NO),
		cfg(192, 16, 208, 32, NO)
	);
	res["164"] = npc(
		"Nurse Hasumi", "Npc/NpcGuest", C_CHAR,
		NO_DESC,
		cfg(224, 0, 240, 16, NO),
		cfg(224, 16, 240, 32, NO)
	);
	res["165"] = npc(
		"Curly (Collapsed)", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(192, 96, 208, 112, NO)
	);
	res["166"] = npc(
		"Chaba", "Npc/NpcMaze", C_CHAR,
		"Best bug. ^.^",
		cfg(144, 104, 184, 128, NO)
	);
	res["167"] = npc(
		"Professor Booster (Falling)", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(304, 0, 320, 16, NO)
	);
	res["168"] = npc(
		"Boulder", "Npc/NpcBllg", C_FURN,
		NO_DESC,
		cfg(264, 56, 320, 96, NO)
	);
	res["169"] = npc(
		"Balrog 4 (44 : MazeS : Missile)", "Npc/NpcBllg", C_CHAR,
		BALROG_LINE "Shooty-shooty bang bang.",
		BALROG_STANDARD_RECTS
	);
	res["170"] = npc(
		"Balrog 4 Projectile", "Npc/NpcMaze", C_INTN,
		NO_DESC,
		cfg(112, 96, 128, 104, NO),
		cfg(112, 104, 128, 112, NO)
	);
	res["171"] = npc(
		"Fire Whirrr", "Npc/NpcMaze", C_LABY,
		NO_DESC,
		cfg(120, 48, 152, 80, NO),
		cfg(184, 48, 216, 80, NO)
	);
	res["172"] = npc(
		"Fire Whirrr Projectile", "Npc/NpcMaze", C_INTN,
		NO_DESC,
		cfg(248, 48, 264, 80, NO)
	);
	res["173"] = npc(
		"Gaudi (Armoured)", "Npc/NpcMaze", C_LABY,
		NO_DESC,
		cfg(0, 128, 24, 152, NO),
		cfg(0, 152, 24, 176, NO)
	);
	res["174"] = npc(
		"Gaudi (Armoured) Projectile", "Npc/NpcMaze", C_INTN,
		NO_DESC,
		cfg(120, 80, 136, 96, NO)
	);
	res["175"] = npc(
		"Gaudi (Egg)", "Npc/NpcMaze", C_LABY,
		NO_DESC,
		cfg(168, 80, 192, 104, NO),
		cfg(216, 80, 240, 104, NO)
	);
	res["176"] = npc(
		"BuyoBuyo Base", "Npc/NpcMaze", C_LABY,
		NO_DESC,
		cfg(96, 128, 128, 144, NO),
		cfg(96, 144, 128, 160, NO)
	);
	res["177"] = npc(
		"BuyoBuyo", "Npc/NpcMaze", C_LABY,
		NO_DESC,
		cfg(192, 128, 208, 144, NO)
	);
	res["178"] = npc(
		"Core Blade Projectile", "Npc/NpcAlmo1", C_INTN,
		NO_DESC,
		cfg(0, 224, 16, 240, NO)
	);
	res["179"] = npc(
		"Core Wisp Projectile", "Npc/NpcAlmo2", C_INTN,
		NO_DESC,
		cfg(48, 224, 72, 240, NO)
	);
	res["180"] = npc(
		"Curly (AI)", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(0, 96, 16, 112, NO),
		cfg(0, 112, 16, 128, NO)
	);
	res["181"] = npc(
		"Curly (AI) Machine Gun", "Npc/NpcSym", C_CHAR,
		NO_DESC,
		cfg(216, 152, 232, 168, NO),
		cfg(216, 168, 232, 184, NO)
	);
	res["182"] = npc(
		"Curly (AI) Polar Star", "Npc/NpcSym", C_CHAR,
		NO_DESC,
		cfg(184, 152, 200, 168, NO),
		cfg(184, 168, 200, 184, NO)
	);
	res["183"] = npc(
		"Curly Air Tank Bubble", "Caret", C_UTIL,
		NO_DESC,
		cfg(56, 96, 80, 120, NO)
	);
	res["184"] = npc(
		"Big Shutter", "Stage/PrtAlmond", C_FURN,
		NO_DESC,
		cfgNudge(0, 64, 32, 96, NO, 8, 8)
	);
	res["185"] = npc(
		"Small Shutter", "Stage/PrtAlmond", C_FURN,
		NO_DESC,
		cfgNudge(96, 64, 112, 96, NO, 0, 8)
	);
	res["186"] = npc(
		"Lift Block", "Stage/PrtAlmond", C_UTIL,
		NO_DESC,
		cfg(48, 48, 64, 64, NO)
	);
	res["187"] = npc(
		"Fuzz Core", "Npc/NpcMaze", C_LABY,
		NO_DESC,
		cfg(224, 104, 256, 136, NO),
		cfg(224, 136, 256, 168, NO)
	);
	res["188"] = npc(
		"Fuzz", "Npc/NpcMaze", C_LABY,
		NO_DESC,
		cfg(288, 104, 304, 120, NO),
		cfg(288, 120, 304, 136, NO)
	);
	res["189"] = npc(
		"Unused Homing Flame", "Npc/NpcSym", C_SPWN,
		"Starts by throwing itself leftwards.\n"
		"This is programmed in such a way that one can assume this was intended to be spawned by another NPC.\n"
		"After 5 seconds, will start to home in on player.",
		cfg(224, 184, 232, 200, NO)
	);
	res["190"] = npc(
		"Broken Killer Robot", "Npc/NpcSym", C_CHAR,
		"The broken killer robot from Core room.\n"
		"This actually has some ANPs:\n"
		"0000: Default state, lights off.\n"
		"0010: Plays sound and emits smoke.\n"
		"0020: Lights blink.",
		cfg(192, 32, 208, 48, NO)
	);
	res["191"] = npc(
		"Water Level Controller", "Npc/NpcSym", C_UTIL,
		"This is a bit of an oddity.\n"
		"Essentially, the Core water level is always set to this NPC's Y position.\n"
		"This NPC is then controlled via ANPs:\n"
		"0000: Set target to current Y position and run sine motion. This is used\n"
		"0010: Internal\n"
		"0020: This is used when the Core is alive and sets up that particular motion.\n"
		"      In particular, it reads the boss Y position, which for Core is actually a flag for this purpose.\n"
		"0021, 0022: Internal\n"
		"0030: Core defeat - Flood.\nThis actually maintains a sine water level *around* 0.",
		cfg(0, 0, 0, 0, NO)
	);
	res["192"] = npc(
		"Scooter", "Npc/NpcRegu", C_FURN,
		NO_DESC,
		cfg(224, 64, 256, 80, NO),
		cfg(224, 80, 256, 96, NO)
	);
	res["193"] = npc(
		"Scooter (Broken)", "Npc/NpcRegu", C_FURN,
		NO_DESC,
		cfg(256, 96, 320, 112, NO)
	);
	res["194"] = npc(
		"Blue Robot (Broken)", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(192, 120, 224, 128, NO)
	);
	res["195"] = npc(
		"Grate (Dark Place)", "Stage/PrtCave", C_FURN,
		FURN1_TEXT "\nThis is the one in Dark Place.",
		cfg(112, 64, 128, 80, NO)
	);
	res["196"] = npc(
		"Ironhead: Wall-That-Goes-Left-And-Wraps", "Stage/PrtRiver", C_UTIL,
		"Attempts to go left by 6 pixels each frame.\nOnce it reaches <= tile X 19, moves itself forward by 22 tiles.\nTherefore, establishes the perspective of a horizontally moving wall.",
		cfg(112, 64, 144, 80, NO),
		cfg(112, 80, 144, 96, NO)
	);
	res["197"] = npc(
		"Ironhead: Ikachan Hari", "Npc/NpcStream", C_WWAY,
		"Recreation of an Ikachan enemy, but recontextualized for Main Artery.\nStarts by flying off to the right & up/down, but is continually receiving leftward force.\nHarmless in this state.\nOnce it starts going left, it activates, flashing like in Ikachan, and can thus cause damage.\nIn this state, once it goes past tile X 3 (far offscreen for Ironhead fight), it autodies.",
		cfg(0, 0, 16, 16, NO)
	);
	res["198"] = npc(
		"Ironhead Projectile", "Npc/NpcIronH", C_INTB,
		"Oh no, Ironhead fires projectiles now?",
		cfg(208, 48, 224, 72, NO)
	);
	res["199"] = npc(
		"Water/Wind Particles", "Caret", C_UTIL,
		NO_DESC,
		cfg(72, 16, 74, 18, NO)
	);
	res["200"] = npc(
		"Dragon Zombie", "Npc/NpcEggs2", C_EGGS,
		NO_DESC,
		cfg(0, 0, 40, 40, NO),
		cfg(0, 40, 40, 80, NO)
	);
	res["201"] = npc(
		"Dragon Zombie (Dead)", "Npc/NpcEggs2", C_INTN,
		NO_DESC,
		cfg(200, 0, 240, 40, NO),
		cfg(200, 40, 240, 80, NO)
	);
	res["202"] = npc(
		"Dragon Zombie Projectile", "Npc/NpcSym", C_INTN,
		NO_DESC,
		cfg(184, 216, 200, 240, NO)
	);
	res["203"] = npc(
		"Critter (Egg Corridor 2)", "Npc/NpcEggs2", C_ECRT,
		NO_DESC,
		cfg(0, 80, 16, 96, NO),
		cfg(0, 96, 16, 112, NO)
	);
	res["204"] = npc(
		"Falling Spike (Small)", "Stage/PrtJail", C_EVIL,
		NO_DESC,
		cfg(240, 80, 256, 96, NO)
	);
	res["205"] = npc(
		"Falling Spike (Large)", "Stage/PrtJail", C_EVIL,
		NO_DESC,
		cfg(112, 80, 128, 112, NO)
	);
	res["206"] = npc(
		"Counter Bomb", "Npc/NpcEggs2", C_EGGS,
		NO_DESC,
		cfg(80, 80, 120, 120, NO)
	);
	res["207"] = npc(
		"Counter Bomb Countdown", "Npc/NpcEggs2", C_EGGS,
		NO_DESC,
		cfg(0, 144, 16, 160, NO)
	);
	res["208"] = npc(
		"Basu (Egg Corridor 2)", "Npc/NpcEggs2", C_EGGS,
		NO_DESC,
		cfg(248, 80, 272, 104, NO),
		cfg(248, 104, 272, 128, NO)
	);
	res["209"] = npc(
		"Basu Projectile (Egg Corridor 2)", "Npc/NpcEggs2", C_INTN,
		NO_DESC,
		cfg(232, 96, 248, 112, NO)
	);
	res["210"] = npc(
		"Beetle (Egg Corridor 2)", "Npc/NpcEggs2", C_EGGS,
		NO_DESC,
		cfg(0, 112, 16, 128, NO),
		cfg(32, 112, 48, 128, NO)
	);
	res["211"] = npc(
		"Spikes (Small)", "Npc/NpcSym", C_EVIL,
		NO_DESC,
		cfg(256, 200, 272, 216, NO)
	);
	res["212"] = npc(
		"Sky Dragon", "Npc/NpcRegu", C_CHAR,
		"Dwagon! *pets* *hugs* UwU",
		cfg(160, 152, 200, 192, NO)
	);
	res["213"] = npc(
		"Night Spirit", "Npc/NpcMoon", C_OSDE,
		"Hug on sight.",
		cfg(0, 0, 48, 48, NO)
	);
	res["214"] = npc(
		"Night Spirit Projectile", "Npc/NpcMoon", C_INTN,
		NO_DESC,
		cfg(144, 48, 176, 64, NO)
	);
	res["215"] = npc(
		"Sandcroc (Outer Wall)", "Npc/NpcMoon", C_OSDE,
		NO_DESC,
		cfg(0, 96, 48, 128, NO)
	);
	res["216"] = npc(
		"Debug Cat", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(256, 192, 272, 216, NO)
	);
	res["217"] = npc(
		"Itoh", "Npc/NpcRegu", C_LABY,
		NO_DESC,
		cfg(144, 64, 160, 80, NO)
	);
	res["218"] = npc(
		"Core Giant Energy Ball Projectile", "Npc/NpcAlmo1", C_INTN,
		NO_DESC,
		cfg(256, 120, 288, 152, NO)
	);
	res["219"] = npc(
		"Smoke Generator", "Npc/NpcMaze", C_UTIL,
		NO_DESC,
		cfg(0, 0, 0, 0, NO)
	);
	res["220"] = npc(
		"Shovel Brigade", "Npc/NpcGuest", C_CHAR,
		NO_DESC,
		cfg(0, 64, 16, 80, NO),
		cfg(0, 80, 16, 96, NO)
	);
	res["221"] = npc(
		"Shovel Brigade (Walking)", "Npc/NpcGuest", C_CHAR,
		NO_DESC,
		cfg(0, 64, 16, 80, NO),
		cfg(0, 80, 16, 96, NO)
	);
	res["222"] = npc(
		"Cage Bars (1x2, Plantation Jail)", "Npc/NpcSym", C_FURN,
		FURN1_TEXT,
		cfg(96, 168, 112, 200, NO)
	);
	res["223"] = npc(
		"Momorin", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(80, 192, 96, 216, NO),
		cfg(80, 216, 96, 240, NO)
	);
	res["224"] = npc(
		"Chie", "Npc/NpcGuest", C_CHAR,
		NO_DESC,
		cfg(112, 32, 128, 48, NO),
		cfg(112, 48, 128, 64, NO)
	);
	res["225"] = npc(
		"Megane", "Npc/NpcGuest", C_CHAR,
		NO_DESC,
		cfg(64, 64, 80, 80, NO),
		cfg(64, 80, 80, 96, NO)
	);
	res["226"] = npc(
		"Kanpachi (Not Fishing)", "Npc/NpcGuest", C_CHAR,
		NO_DESC,
		cfg(256, 56, 272, 80, NO)
	);
	res["227"] = npc(
		"Bucket", "Npc/NpcGuest", C_FURN,
		NO_DESC,
		cfg(208, 32, 224, 48, NO)
	);
	res["228"] = npc(
		"Droll (Guard)", "Npc/NpcCent", C_PLNT,
		NO_DESC,
		cfg(0, 0, 32, 40, NO),
		cfg(0, 40, 32, 80, NO)
	);
	res["229"] = npc(
		"Red Flowers (Sprouts)", "Npc/NpcGuest", C_FURN,
		NO_DESC,
		cfg(0, 96, 48, 112, NO)
	);
	res["230"] = npc(
		"Red Flowers (Blooming)", "Npc/NpcGuest", C_FURN,
		NO_DESC,
		cfg(48, 96, 96, 128, NO)
	);
	res["231"] = npc(
		"Rocket", "Npc/NpcGuest", C_FURN,
		NO_DESC,
		cfg(176, 32, 208, 48, NO)
	);
	res["232"] = npc(
		"Orangebell", "Npc/NpcCent", C_LCAV,
		NO_DESC,
		cfg(128, 0, 160, 32, NO),
		cfg(128, 32, 160, 64, NO)
	);
	res["233"] = npc(
		"Orangebell Bat", "Npc/NpcCent", C_LCAV,
		NO_DESC,
		cfg(256, 0, 272, 16, NO),
		cfg(256, 16, 272, 32, NO)
	);
	res["234"] = npc(
		"Red Flowers (Picked)", "Npc/NpcGuest", C_FURN,
		NO_DESC,
		cfg(144, 96, 192, 112, NO)
	);
	res["235"] = npc(
		"Midorin", "Npc/NpcGuest", C_CHAR,
		NO_DESC,
		cfg(192, 96, 208, 112, NO),
		cfg(192, 112, 208, 128, NO)
	);
	res["236"] = npc(
		"Gunfish", "Npc/NpcCent", C_PLNT,
		NO_DESC,
		cfg(128, 64, 152, 88, NO),
		cfg(128, 88, 152, 112, NO)
	);
	res["237"] = npc(
		"Gunfish Projectile", "Npc/NpcCent", C_INTN,
		NO_DESC,
		cfg(312, 32, 320, 40, NO)
	);
	res["238"] = npc(
		"Press (Sideways)", "Npc/NpcSym", C_PLNT,
		NO_DESC,
		cfg(184, 200, 208, 216, NO)
	);
	res["239"] = npc(
		"Cage Bars (Large, Plantation / The King's Table)", "Stage/PrtStore", C_FURN,
		"Cage bars.\n"
		"The 'left' variant (Option 2) is for the Store tileset.\n"
		"The 'right' variant (Option 2) is for the White tileset.",
		cfgNudge(192, 48, 256, 80, NO, 8, 16),
		cfgNudge(96, 112, 144, 144, NO, 8, 16)
	);
	res["240"] = npc(
		"Mimiga (Jailed)", "Npc/NpcGuest", C_FURN,
		NO_DESC,
		cfg(160, 64, 176, 80, NO),
		cfg(160, 80, 176, 96, NO)
	);
	res["241"] = npc(
		"Critter (Last Cave)", "Npc/NpcRed", C_ECRT,
		NO_DESC,
		cfg(0, 0, 16, 16, NO),
		cfg(0, 16, 16, 32, NO)
	);
	res["242"] = npc(
		"Bat (Last Cave)", "Npc/NpcRed", C_EBAT,
		NO_DESC,
		cfg(32, 32, 48, 48, NO),
		cfg(32, 48, 48, 64, NO)
	);
	res["243"] = npc(
		"Bat Generator (Last Cave)", "Npc/NpcSym", C_UTIL,
		NO_DESC,
		cfg(0, 0, 0, 0, NO)
	);
	res["244"] = npc(
		"Lavadrop (Last Cave)", "Npc/NpcRed", C_SPWN,
		NO_DESC,
		cfg(96, 0, 104, 16, NO)
	);
	res["245"] = npc(
		"Lavadrop Generator (Last Cave)", "Npc/NpcRed", C_UTIL,
		NO_DESC,
		cfg(120, 0, 128, 16, NO)
	);
	res["246"] = npc(
		"Press (Proximity)", "Npc/NpcSym", C_LCAV,
		NO_DESC,
		cfg(144, 112, 160, 136, NO)
	);
	res["247"] = npc(
		"Misery (Boss)", "Npc/NpcMiza", C_CHAR,
		NO_DESC,
		cfg(0, 0, 16, 16, NO),
		cfg(0, 16, 16, 32, NO)
	);
	// if this seems swapped in CSE2EX comments, that's probably a CSE2EX issue
	// that said, if this turns out wrong...
	res["248"] = npc(
		"Misery (Boss) Energy Projectile", "Npc/NpcMiza", C_INTB,
		NO_DESC,
		cfg(0, 48, 16, 64, NO)
	);
	res["249"] = npc(
		"Misery (Boss, Vanishing)", "Npc/NpcMiza", C_CHAR,
		NO_DESC,
		cfg(48, 48, 64, 64, NO)
	);
	res["250"] = npc(
		"Misery (Boss) Lightning Projectile", "Npc/NpcMiza", C_INTB,
		NO_DESC,
		cfg(16, 32, 32, 48, NO)
	);
	res["251"] = npc(
		"Misery (Boss) Lightning", "Npc/NpcMiza", C_INTB,
		NO_DESC,
		cfg(96, 32, 112, 64, NO)
	);
	res["252"] = npc(
		"Misery (Boss) Bat", "Npc/NpcMiza", C_SPWN,
		NO_DESC,
		cfg(128, 32, 144, 48, NO),
		cfg(128, 48, 144, 64, NO)
	);
	res["253"] = npc(
		"EXP Capsule", "Npc/NpcSym", C_ITEM,
		NO_DESC,
		cfg(0, 64, 16, 80, NO)
	);
	res["254"] = npc(
		"Helicopter", "Npc/NpcHeri", C_FURN,
		NO_DESC,
		cfg(0, 0, 128, 64, NO)
	);
	res["255"] = npc(
		"Helicopter Blades", "Npc/NpcHeri", C_FURN,
		NO_DESC,
		cfg(128, 0, 240, 16, NO),
		cfg(240, 0, 320, 16, NO)
	);
	res["256"] = npc(
		"Doctor (Facing Away)", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(48, 160, 72, 192, NO)
	);
	res["257"] = npc(
		"Red Crystal", "Npc/NpcSym", C_FURN,
		NO_DESC,
		cfg(176, 32, 184, 48, NO)
	);
	res["258"] = npc(
		"Misery (Sleeping)", "Npc/NpcMiza", C_CHAR,
		NO_DESC,
		cfg(48, 32, 64, 48, NO)
	);
	res["259"] = npc(
		"Curly (Carried, Unconscious)", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(224, 96, 240, 112, NO),
		cfg(224, 112, 240, 128, NO)
	);
	res["260"] = npc(
		"Shovel Brigade (Caged)", "Npc/NpcGuest", C_CHAR,
		NO_DESC,
		cfg(128, 64, 144, 80, NO),
		cfg(128, 80, 144, 96, NO)
	);
	res["261"] = npc(
		"Chie (Caged)", "Npc/NpcGuest", C_CHAR,
		NO_DESC,
		cfg(112, 32, 128, 48, NO),
		cfg(112, 48, 128, 64, NO)
	);
	res["262"] = npc(
		"Chaco (Caged)", "Npc/NpcGuest", C_CHAR,
		NO_DESC,
		cfg(128, 0, 144, 16, NO),
		cfg(128, 16, 144, 32, NO)
	);
	res["263"] = npc(
		"Doctor (Boss)", "Npc/NpcDr", C_CHAR,
		NO_DESC,
		cfg(0, 0, 24, 32, NO),
		cfg(0, 32, 24, 64, NO)
	);
	res["264"] = npc(
		"Doctor Red Wave Projectile", "Npc/NpcDr", C_INTB,
		NO_DESC,
		cfg(288, 0, 304, 16, NO)
	);
	res["265"] = npc(
		"Doctor Red Ball Projectile", "Npc/NpcDr", C_INTB,
		NO_DESC,
		cfg(288, 16, 304, 32, NO)
	);
	res["266"] = npc(
		"Doctor Red Ball Projectile (Bouncing)", "Npc/NpcDr", C_INTB,
		NO_DESC,
		cfg(304, 16, 320, 32, NO)
	);
	res["267"] = npc(
		"Doctor (Frenzied)", "Npc/NpcDr", C_CHAR,
		NO_DESC,
		cfg(0, 64, 40, 112, NO),
		cfg(0, 112, 40, 160, NO)
	);
	res["268"] = npc(
		"Igor (Enemy)", "Npc/NpcRavil", C_CHAR,
		NO_DESC,
		cfg(0, 0, 40, 40, NO),
		cfg(0, 40, 40, 80, NO)
	);
	res["269"] = npc(
		"Bat (Doctor, Red, Bouncing)", "Npc/NpcDr", C_INTB,
		NO_DESC,
		cfg(232, 0, 248, 16, NO),
		cfg(232, 32, 248, 48, NO)
	);
	res["270"] = npc(
		"Red Crystal Particluate", "Npc/NpcSym", C_INTB,
		NO_DESC,
		cfg(170, 34, 174, 38, NO)
	);
	res["271"] = npc(
		"Ironhead Block", "Stage/PrtRiver", C_WWAY,
		NO_DESC,
		cfg(0, 0x40, 0x20, 0x60, NO)
	);
	res["272"] = npc(
		"Ironhead Block Generator", "Stage/PrtRiver", C_UTIL,
		NO_DESC,
		cfg(0, 0, 0, 0, NO)
	);
	res["273"] = npc(
		"Droll Projectile", "Npc/NpcCent", C_INTN,
		NO_DESC,
		cfg(248, 40, 272, 64, NO)
	);
	res["274"] = npc(
		"Droll", "Npc/NpcCent", C_PLNT,
		NO_DESC,
		cfg(0, 0, 32, 40, NO),
		cfg(0, 40, 32, 80, NO)
	);
	res["275"] = npc(
		"Puppy (Plantation)", "Npc/NpcGuest", C_CHAR,
		NO_DESC,
		cfg(272, 80, 288, 96, NO)
	);
	res["276"] = npc(
		"Red Demon", "Npc/NpcRed", C_LCAV,
		NO_DESC,
		cfg(0, 64, 32, 104, NO),
		cfg(0, 104, 32, 144, NO)
	);
	res["277"] = npc(
		"Red Demon Projectile", "Npc/NpcRed", C_INTN,
		NO_DESC,
		cfg(128, 0, 152, 24, NO)
	);
	res["278"] = npc(
		"Little Family", "data/ItemImage", C_CHAR,
		NO_DESC,
		cfg(0, 120, 8, 128, NO)
	);
	res["279"] = npc(
		"Falling Block (Large)", "Stage/PrtWhite", C_SPWN,
		NO_DESC,
		cfg(0, 16, 32, 48, NO)
	);
	res["280"] = npc(
		"Sue (Teleported By Misery)", "Npc/NpcRegu", C_LABY,
		NO_DESC,
		cfg(112, 32, 128, 48, NO),
		cfg(112, 48, 128, 64, NO)
	);
	res["281"] = npc(
		"Flying Red Particulate", "Npc/NpcRegu", C_SPWN,
		NO_DESC,
		cfg(0, 0, 0, 0, NO)
	);
	res["282"] = npc(
		"Mini Undead Core (Active)", "Npc/NpcAlmo2", C_INTB,
		NO_DESC,
		cfg(256, 80, 320, 120, NO)
	);
	res["283"] = npc(
		"Misery (Transformed)", "Npc/NpcMiza", C_CHAR,
		NO_DESC,
		cfg(0, 64, 32, 96, NO)
	);
	res["284"] = npc(
		"Sue (Transformed)", "Npc/NpcMiza", C_CHAR,
		NO_DESC,
		cfg(0, 128, 32, 160, NO),
		cfg(0, 160, 32, 192, NO)
	);
	res["285"] = npc(
		"Undead Core Spiral Projectile 1", "Npc/NpcAlmo2", C_INTB,
		NO_DESC,
		cfg(232, 104, 248, 120, NO)
	);
	res["286"] = npc(
		"Undead Core Spiral Projectile 2", "Npc/NpcAlmo2", C_INTB,
		NO_DESC,
		cfg(232, 120, 248, 136, NO)
	);
	res["287"] = npc(
		"Orange Smoke", "Npc/NpcAlmo2", C_UTIL,
		NO_DESC,
		cfg(0, 224, 16, 240, NO)
	);
	res["288"] = npc(
		"Undead Core Exploding Rock", "Npc/NpcAlmo2", C_INTB,
		NO_DESC,
		cfg(232, 72, 248, 88, NO)
	);
	res["289"] = npc(
		"Critter (Orange, Misery)", "Npc/NpcMiza", C_INTB,
		NO_DESC,
		cfg(160, 32, 176, 48, NO),
		cfg(160, 48, 176, 64, NO)
	);
	res["290"] = npc(
		"Bat (Misery)", "Npc/NpcMiza", C_INTB,
		NO_DESC,
		cfg(112, 32, 128, 48, NO),
		cfg(112, 48, 128, 64, NO)
	);
	res["291"] = npc(
		"Mini Undead Core (Inactive)", "Npc/NpcAlmo2", C_INTB,
		NO_DESC,
		cfg(256, 80, 320, 120, NO)
	);
	res["292"] = npc(
		"Earthquake Generator", "Npc/NpcSym", C_UTIL,
		NO_DESC,
		cfg(0, 0, 0, 0, NO)
	);
	res["293"] = npc(
		"Undead Core Giant Energy Projectile", "Npc/NpcAlmo2", C_INTB,
		NO_DESC,
		cfg(240, 200, 280, 240, NO)
	);
	res["294"] = npc(
		"Falling Block Generator", "Npc/NpcSym", C_UTIL,
		NO_DESC,
		cfg(0, 0, 0, 0, NO)
	);
	res["295"] = npc(
		"Cloud", "Stage/PrtWhite", C_INTN,
		NO_DESC,
		cfg(0, 0, 208, 64, NO)
	);
	res["296"] = npc(
		"Cloud Generator", "Npc/NpcSym", C_UTIL,
		NO_DESC,
		cfg(0, 0, 0, 0, NO)
	);
	res["297"] = npc(
		"Sue Held By Dragon", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(112, 48, 128, 64, NO)
	);
	res["298"] = npc(
		"Doctor (Intro)", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(72, 128, 88, 160, NO)
	);
	res["299"] = npc(
		"Balrog/Misery (Intro)", "Npc/NpcKings", C_CHAR,
		NO_DESC,
		cfg(0, 0, 48, 48, NO),
		cfg(48, 0, 96, 48, NO)
	);
	res["300"] = npc(
		"Demon Crown (Intro)", "Npc/NpcRegu", C_FURN,
		NO_DESC,
		cfg(192, 80, 208, 96, NO)
	);
	res["301"] = npc(
		"Fish Missile (Misery)", "Npc/NpcMiza", C_INTB,
		NO_DESC,
		cfg(144, 0, 160, 16, NO)
	);
	res["302"] = npc(
		"Camera Focus Marker", "Npc/NpcSym", C_UTIL,
		NO_DESC,
		cfg(0, 0, 0, 0, NO)
	);
	res["303"] = npc(
		"Curly's Machine Gun", "Npc/NpcSym", C_CHAR,
		NO_DESC,
		cfg(216, 152, 232, 168, NO),
		cfg(216, 168, 232, 184, NO)
	);
	res["304"] = npc(
		"Gaudi In Hospital", "Npc/NpcMaze", C_FURN,
		NO_DESC,
		cfg(0, 176, 24, 192, NO)
	);
	res["305"] = npc(
		"Small Puppy", "Npc/NpcSand", C_CHAR,
		NO_DESC,
		cfg(160, 144, 176, 160, NO),
		cfg(160, 160, 176, 176, NO)
	);
	res["306"] = npc(
		"Balrog (Nurse)", "Npc/NpcGuest", C_CHAR,
		NO_DESC,
		cfg(240, 96, 280, 128, NO),
		cfg(160, 152, 200, 184, NO)
	);
	res["307"] = npc(
		"Santa (Caged)", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(0, 32, 16, 48, NO),
		cfg(0, 48, 16, 64, NO)
	);
	res["308"] = npc(
		"Stumpy", "Npc/NpcGuest", C_LABY,
		NO_DESC,
		cfg(128, 112, 144, 128, NO),
		cfg(128, 128, 144, 144, NO)
	);
	res["309"] = npc(
		"Bute", "Npc/NpcHell", C_HELL,
		NO_DESC,
		cfg(0, 0, 16, 16, NO),
		cfg(0, 16, 16, 32, NO)
	);
	res["310"] = npc(
		"Bute (With Sword)", "Npc/NpcHell", C_HELL,
		NO_DESC,
		cfg(32, 0, 56, 16, NO),
		cfg(32, 16, 56, 32, NO)
	);
	res["311"] = npc(
		"Bute Archer", "Npc/NpcHell", C_HELL,
		NO_DESC,
		cfg(0, 32, 24, 56, NO),
		cfg(0, 56, 24, 80, NO)
	);
	res["312"] = npc(
		"Bute Archer Projectile", "Npc/NpcHell", C_INTN,
		NO_DESC,
		cfg(0, 160, 16, 176, NO),
		cfg(0, 176, 16, 192, NO)
	);
	res["313"] = npc(
		"Ma Pignon", "Npc/NpcCemet", C_GYRD,
		NO_DESC,
		cfg(128, 0, 144, 16, NO),
		cfg(128, 16, 144, 32, NO)
	);
	res["314"] = npc(
		"Ma Pignon Rock", "Stage/PrtCave", C_INTB,
		NO_DESC,
		cfg(64, 64, 80, 80, NO)
	);
	res["315"] = npc(
		"Ma Pignon Clone", "Npc/NpcCemet", C_INTB,
		NO_DESC,
		cfg(128, 0, 144, 16, NO),
		cfg(128, 16, 144, 32, NO)
	);
	res["316"] = npc(
		"Bute (Dead)", "Npc/NpcHell", C_INTN,
		NO_DESC,
		cfg(248, 32, 272, 56, NO),
		cfg(248, 56, 272, 80, NO)
	);
	res["317"] = npc(
		"Mesa", "Npc/NpcHell", C_HELL,
		NO_DESC,
		cfg(0, 80, 32, 120, NO),
		cfg(0, 120, 32, 160, NO)
	);
	res["318"] = npc(
		"Mesa (Dead)", "Npc/NpcHell", C_INTN,
		NO_DESC,
		cfg(224, 80, 256, 120, NO),
		cfg(224, 120, 256, 160, NO)
	);
	res["319"] = npc(
		"Mesa Block", "Stage/PrtHell", C_HELL,
		NO_DESC,
		cfg(16, 0, 32, 16, NO)
	);
	res["320"] = npc(
		"Curly (Carried, Shooting)", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(16, 96, 32, 112, NO),
		cfg(16, 112, 32, 128, NO)
	);
	res["321"] = npc(
		"Curly's Nemesis", "Npc/NpcSym", C_CHAR,
		NO_DESC,
		cfg(136, 152, 152, 168, NO),
		cfg(136, 168, 152, 184, NO)
	);
	res["322"] = npc(
		"Deleet", "Npc/NpcSym", C_HELL,
		NO_DESC,
		cfg(272, 216, 296, 240, NO)
	);
	res["323"] = npc(
		"Bute (Spinning)", "Npc/NpcHell", C_HELL,
		NO_DESC,
		cfg(216, 32, 232, 56, NO)
	);
	res["324"] = npc(
		"Bute Generator", "Npc/NpcHell", C_UTIL,
		NO_DESC,
		cfg(0, 0, 0, 0, NO)
	);
	res["325"] = npc(
		"Heavy Press Lightning", "Npc/NpcPress", C_INTB,
		NO_DESC,
		cfg(240, 96, 272, 128, NO)
	);
	res["326"] = npc(
		"Sue/Itoh Becoming Human", "Npc/NpcGuest", C_CHAR,
		NO_DESC,
		cfg(0, 128, 16, 152, NO),
		cfg(128, 128, 144, 152, NO)
	);
	res["327"] = npc(
		"Sneeze", "Npc/NpcGuest", C_FURN,
		NO_DESC,
		cfg(240, 80, 256, 96, NO)
	);
	res["328"] = npc(
		"Lab TF Device", "Stage/PrtLabo", C_FURN,
		NO_DESC,
		cfg(96, 0, 128, 48, NO)
	);
	res["329"] = npc(
		"Lab Fan", "Stage/PrtLabo", C_FURN,
		NO_DESC,
		cfg(48, 0, 64, 16, NO)
	);
	res["330"] = npc(
		"Rolling", "Npc/NpcSym", C_HELL,
		NO_DESC,
		cfg(144, 136, 160, 152, NO)
	);
	res["331"] = npc(
		"Ballos Bone Projectile", "Npc/NpcBallos", C_INTN,
		NO_DESC,
		cfg(288, 80, 304, 96, NO)
	);
	res["332"] = npc(
		"Ballos Shockwave", "Caret", C_INTB,
		NO_DESC,
		cfg(144, 96, 168, 120, NO)
	);
	res["333"] = npc(
		"Ballos Lightning", "Caret", C_INTB,
		NO_DESC,
		cfg(80, 120, 104, 144, NO)
	);
	res["334"] = npc(
		"Sweat", "Npc/NpcSym", C_INTB,
		NO_DESC,
		cfg(160, 184, 168, 200, NO),
		cfg(176, 184, 184, 200, NO)
	);
	res["335"] = npc(
		"Ikachan", "Npc/NpcStream", C_CHAR,
		NO_DESC,
		cfg(0, 16, 16, 32, NO)
	);
	res["336"] = npc(
		"Ikachan Generator", "Npc/NpcSym", C_UTIL,
		NO_DESC,
		cfg(0, 0, 0, 0, NO)
	);
	res["337"] = npc(
		"Numhachi", "Npc/NpcCent", C_CHAR,
		NO_DESC,
		cfg(256, 112, 288, 152, NO)
	);
	res["338"] = npc(
		"Green Devil", "Npc/NpcMiza", C_HELL,
		NO_DESC,
		cfg(288, 0, 304, 16, NO),
		cfg(288, 16, 304, 32, NO)
	);
	res["339"] = npc(
		"Green Devil Generator", "Npc/NpcSym", C_UTIL,
		NO_DESC,
		cfg(0, 0, 0, 0, NO)
	);
	res["340"] = npc(
		"Ballos", "Npc/NpcPriest", C_CHAR,
		NO_DESC,
		cfg(0, 0, 48, 40, NO),
		cfg(0, 40, 48, 80, NO)
	);
	res["341"] = npc(
		"Ballos Head", "Npc/NpcPriest", C_INTB,
		NO_DESC,
		cfg(288, 32, 320, 48, NO)
	);
	res["342"] = npc(
		"Ballos Eye", "Npc/NpcBallos", C_INTB,
		NO_DESC,
		cfg(240, 48, 280, 88, NO)
	);
	res["343"] = npc(
		"Ballos Cutscene", "Npc/NpcBallos", C_INTB,
		NO_DESC,
		cfg(0, 0, 120, 120, NO)
	);
	res["344"] = npc(
		"Ballos Eyes", "Npc/NpcBallos", C_INTB,
		NO_DESC,
		cfg(272, 0, 296, 16, NO)
	);
	res["345"] = npc(
		"Ballos Skull Projectile", "Stage/PrtHell", C_INTN,
		NO_DESC,
		cfg(128, 176, 144, 192, NO)
	);
	res["346"] = npc(
		"Ballos Orbiting Platform", "Npc/NpcBallos", C_INTB,
		NO_DESC,
		cfg(240, 0, 272, 16, NO)
	);
	res["347"] = npc(
		"Hoppy", "Npc/NpcBallos", C_INTB,
		NO_DESC,
		cfg(256, 48, 272, 64, NO)
	);
	res["348"] = npc(
		"Ballos Spikes", "Stage/PrtHell", C_INTB,
		NO_DESC,
		cfg(128, 152, 160, 176, NO)
	);
	res["349"] = npc(
		"Statue", "Npc/NpcSym", C_FURN,
		FURN2_TEXT,
		cfgNudge(0, 0, 16, 16, NO, 8, 0),
		cfgNudge(0, 0, 16, 16, NO, 0, 16)
	);
	res["350"] = npc(
		"Flying Bute Archer", "Npc/NpcHell", C_HELL,
		NO_DESC,
		cfg(24, 160, 48, 184, NO),
		cfg(24, 184, 48, 208, NO)
	);
	res["351"] = npc(
		"Statue (Shootable)", "Stage/PrtHell", C_FURN,
		"One of the shootable statues that start as Demon Crown owners and turn into main characters when shot.\nDirection divided by 10 (for some reason) controls which statue this is.",
		cfgNudge(0, 96, 32, 136, NO, 8, 12),
		cfgNudge(64, 96, 96, 136, NO, 8, 12)
	);
	res["352"] = npc(
		"Ending Characters", "Npc/NpcRegu", C_CHAR,
		NO_DESC,
		cfg(304, 48, 320, 64, NO)
	);
	res["353"] = npc(
		"Flying Bute w/ Sword", "Npc/NpcHell", C_HELL,
		NO_DESC,
		cfg(168, 160, 184, 184, NO)
	);
	res["354"] = npc(
		"Invisible Deathtrap Wall", "Npc/NpcSym", C_EVIL,
		NO_DESC,
		cfg(0, 0, 0, 0, NO)
	);
	res["355"] = npc(
		"Balrog w/ Quote and Curly", "Title", C_CHAR,
		NO_DESC,
		cfg(80, 16, 96, 32, NO)
	);
	res["356"] = npc(
		"Balrog (Rescuing Quote & Curly)", "Npc/NpcGuest", C_CHAR,
		NO_DESC,
		cfg(240, 128, 280, 152, NO)
	);
	res["357"] = npc(
		"Puppy (Ghost)", "Npc/NpcSym", C_CHAR,
		NO_DESC,
		cfg(224, 136, 240, 152, NO)
	);
	res["358"] = npc(
		"Misery (Credits)", "Npc/NpcMiza", C_CHAR,
		NO_DESC,
		cfg(208, 8, 224, 32, NO)
	);
	res["359"] = npc(
		"Water Droplet Generator", "Npc/NpcSym", C_UTIL,
		NO_DESC,
		cfg(0, 0, 0, 0, NO)
	);
	res["360"] = npc(
		"Thank You", "Npc/NpcGuest", C_UTIL,
		NO_DESC,
		cfgNudge(0, 176, 48, 184, NO, -8, -8)
	);
	return res;
}

QJsonObject cdgnpctbl_constantMetadata = cdgnpctbl_getConstantMetadata();
