#ifndef CDKBASEMDL_H
#define CDKBASEMDL_H

#include <QObject>

class CdkGameMdl;
class CdkBaseGlobalMdl;

class CdkBaseMdl : public QObject {
	Q_OBJECT
public:
	// This function and derivatives are allowed to throw.
	CdkBaseMdl(CdkGameMdl * gameMdl);
	CdkGameMdl * boundMdl;
};

const QList<CdkBaseGlobalMdl *> & cdkbasemdl_findPotentialDependencies();

template <class X> class CdkDependency {
public:
	CdkDependency() {
		for (CdkBaseGlobalMdl * mdl : cdkbasemdl_findPotentialDependencies()) {
			X * res = dynamic_cast<X *>(mdl);
			if (res != nullptr) {
				value = res;
				return;
			}
		}
		throw QString("A model was unable to find dependency ") + typeid(X).name() + QString(".");
	}
	X * value;

	X & operator *() {
		return *value;
	}

	X * operator ->() {
		return value;
	}
};

#endif // CDKBASEMDL_H
