#ifndef CDGTSCGLOBALMDL_H
#define CDGTSCGLOBALMDL_H

#include "cdkbasevisiblemdl.h"

class CdpTSCEditor;

class CdgTSCGlobalMdl : public CdkBaseVisibleMdl {
public:
	CdgTSCGlobalMdl(CdkGameMdl * mdl, const QString & filename, const QString & language);
	void saveGlobal(CdExceptionTracker & ex) override;
	QJsonObject storeCfg() override;
	void show() override;
	QString filename;
	QString language;
private:
	CdpTSCEditor * editor;
};

#endif // CDGTSCGLOBALMDL_H
