#include "cdgnpctblglobalmdl.h"
#include "cdkgamemdl.h"
#include "cdutils.h"
#include "pieces/cdpnpctbleditor.h"
#include "cdgenumglobalmdl.h"
#include <QJsonArray>

CDKGAMEMDL_REGISTER_GLOBAL(npctbl) {
	if (data["file"].type() != QJsonValue::String)
		throw QString("No NPCTBL file specified in entry.");
	if (data["metadata"].type() != QJsonValue::Object)
		throw QString("No metadata specified in entry.");
	return new CdgNPCTBLGlobalMdl(mdl, data["file"].toString(), data["metadata"].toObject());
}

static QRect npctbl_loadRect(const QJsonValue & rct, const QString & pfx) {
	return QRect(
		QPoint(rct[pfx + "L"].toInt(), rct[pfx + "U"].toInt()),
		// Qt rectangles are inclusive, Windows & Pixel rectangles aren't
		QPoint(rct[pfx + "R"].toInt() - 1, rct[pfx + "D"].toInt() - 1)
	);
}

static void npctbl_saveRect(QJsonObject & rct, const QString & pfx, const QRect & rect) {
	rct[pfx + "L"] = rect.x();
	rct[pfx + "U"] = rect.y();
	rct[pfx + "R"] = rect.right() + 1;
	rct[pfx + "D"] = rect.bottom() + 1;
}

CdgNPCTBLEntryConfig npctbl_loadConfig(const QJsonValue & cfg) {
	CdgNPCTBLEntryConfig cfgR;
	cfgR.spriteBounds = npctbl_loadRect(cfg, "frame");
	cfgR.indicatorBounds = npctbl_loadRect(cfg, "indicator");
	// these being optional is useful
	cfgR.nudge.setX(cfg["nudgeX"].toInt());
	cfgR.nudge.setY(cfg["nudgeY"].toInt());
	return cfgR;
}

QJsonObject npctbl_saveConfig(const CdgNPCTBLEntryConfig & cfg) {
	QJsonObject cfgR;
	npctbl_saveRect(cfgR, "frame", cfg.spriteBounds);
	npctbl_saveRect(cfgR, "indicator", cfg.indicatorBounds);
	cfgR["nudgeX"] = cfg.nudge.x();
	cfgR["nudgeY"] = cfg.nudge.y();
	return cfgR;
}

CdgNPCTBLGlobalMdl::CdgNPCTBLGlobalMdl(CdkGameMdl * gameMdl, const QString & file, const QJsonObject & meta) : CdkBaseVisibleMdl(gameMdl), file(file), editor(nullptr) {
	fileAccess = new QFile(gameMdl->getPath(file));
	setObjectName(file);

	QJsonObject patchedMetadata = meta;
	for (const QString & key : cdgnpctbl_constantMetadata.keys()) {
		if (!patchedMetadata.contains(key)) {
			// add in entries that we have but the metadata file doesn't
			// this includes (in particular) entries that have not been modified
			patchedMetadata[key] = cdgnpctbl_constantMetadata[key];
		}
	}

	// vars
	bkgPre = patchedMetadata["bkgPre"].toString();
	npcPre = patchedMetadata["npcPre"].toString();

	// surfaces
	for (const QJsonValue & surface : patchedMetadata["surfaces"].toArray())
		surfaces.append(surface.toString());

	// Actual load procedure uses npc.tbl size as ultimate reference for what to load
	if (fileAccess->open(QFile::ReadOnly)) {
		QByteArray qba = fileAccess->readAll();
		int entries = qba.length() / 0x18;
		int ptr = 0;
		for (int i = 0; i < entries; i++)
			npcs.append(CdgNPCTBLEntry());

		for (CdgNPCTBLEntry & ent : npcs) {
			ent.flags = cdutils_load_u16(qba.mid(ptr, 2));
			ptr += 2;
		}

		for (CdgNPCTBLEntry & ent : npcs) {
			ent.health = cdutils_load_u16(qba.mid(ptr, 2));
			ptr += 2;
		}

		for (CdgNPCTBLEntry & ent : npcs)
			ent.surfaceId = qba[ptr++];

		for (CdgNPCTBLEntry & ent : npcs)
			ent.deathSoundId = qba[ptr++];

		for (CdgNPCTBLEntry & ent : npcs)
			ent.hurtSoundId = qba[ptr++];

		for (CdgNPCTBLEntry & ent : npcs)
			ent.sizeId = qba[ptr++];

		for (CdgNPCTBLEntry & ent : npcs) {
			ent.exp = cdutils_load_u32(qba.mid(ptr, 4));
			ptr += 4;
		}

		for (CdgNPCTBLEntry & ent : npcs) {
			ent.damage = cdutils_load_u32(qba.mid(ptr, 4));
			ptr += 4;
		}

		for (CdgNPCTBLEntry & ent : npcs) {
			ent.hitL = qba[ptr++];
			ent.hitU = qba[ptr++];
			ent.hitR = qba[ptr++];
			ent.hitD = qba[ptr++];
		}

		for (CdgNPCTBLEntry & ent : npcs) {
			ent.displayL = qba[ptr++];
			ent.displayU = qba[ptr++];
			ent.displayR = qba[ptr++];
			ent.displayD = qba[ptr++];
		}

		for (int i = 0; i < npcs.length(); i++) {
			CdgNPCTBLEntry & ent = npcs[i];
			QString key = QString::number(i);
			if (!patchedMetadata.contains(key))
				continue;
			QJsonObject tjo = patchedMetadata[key].toObject();
			// this is as opposed to patchedMetadata
			ent.extModified = meta.contains(key);
			ent.name = tjo["name"].toString();
			ent.description = tjo["desc"].toString();
			ent.category = tjo["category"].toInt();
			ent.sheet = tjo["sheet"].toString();
			ent.cfgA = npctbl_loadConfig(tjo["cfgA"]);
			ent.cfgB = npctbl_loadConfig(tjo["cfgB"]);
		}

		fileAccess->close();
	} else {
		throw QString("Unable to load NPCTBL table.");
	}
}

CdgNPCTBLGlobalMdl::~CdgNPCTBLGlobalMdl() {
	delete fileAccess;
}

void CdgNPCTBLGlobalMdl::show() {
	if (!editor) {
		CdExceptionTracker ex("showing NPCTBLGlobalMdl");
		CdgEnumGlobalMdl * enums = boundMdl->findDependency<CdgEnumGlobalMdl>(ex, "CdgNPCTBLEditor");
		if (!boundMdl->terminateTracker(ex, boundMdl->theOneTrueRoot)) {
			editor = new CdpNPCTBLEditor(boundMdl->theOneTrueRoot, enums, this);
		} else {
			return;
		}
	}
	editor->show();
}

void CdgNPCTBLGlobalMdl::saveGlobal(CdExceptionTracker & ex) {
	if (!fileAccess->open(QFile::WriteOnly))
		CD_THROW("Unable to open npc.tbl");

	for (CdgNPCTBLEntry & ent : npcs)
		cdutils_save_u16(fileAccess, ent.flags);

	for (CdgNPCTBLEntry & ent : npcs)
		cdutils_save_i16(fileAccess, ent.health);

	for (CdgNPCTBLEntry & ent : npcs)
		cdutils_save_u8(fileAccess, ent.surfaceId);

	for (CdgNPCTBLEntry & ent : npcs)
		cdutils_save_u8(fileAccess, ent.deathSoundId);

	for (CdgNPCTBLEntry & ent : npcs)
		cdutils_save_u8(fileAccess, ent.hurtSoundId);

	for (CdgNPCTBLEntry & ent : npcs)
		cdutils_save_u8(fileAccess, ent.sizeId);

	for (CdgNPCTBLEntry & ent : npcs)
		cdutils_save_i32(fileAccess, ent.exp);

	for (CdgNPCTBLEntry & ent : npcs)
		cdutils_save_i32(fileAccess, ent.damage);

	for (CdgNPCTBLEntry & ent : npcs) {
		cdutils_save_i8(fileAccess, ent.hitL);
		cdutils_save_i8(fileAccess, ent.hitU);
		cdutils_save_i8(fileAccess, ent.hitR);
		cdutils_save_i8(fileAccess, ent.hitD);
	}

	for (CdgNPCTBLEntry & ent : npcs) {
		cdutils_save_i8(fileAccess, ent.displayL);
		cdutils_save_i8(fileAccess, ent.displayU);
		cdutils_save_i8(fileAccess, ent.displayR);
		cdutils_save_i8(fileAccess, ent.displayD);
	}
	fileAccess->close();
}

QJsonObject CdgNPCTBLGlobalMdl::storeCfg() {
	QJsonObject res;
	res["type"] = "npctbl";
	res["file"] = file;
	QJsonObject savedMetadata;
	// vars
	savedMetadata["bkgPre"] = bkgPre;
	savedMetadata["npcPre"] = npcPre;
	// surfaces
	QJsonArray surfacesArray;
	for (const QString & s : surfaces)
		surfacesArray.append(s);
	savedMetadata["surfaces"] = surfacesArray;
	// NPCs
	int idx = 0;
	for (const CdgNPCTBLEntry & npc : npcs) {
		if (npc.extModified) {
			QJsonObject qjo;
			qjo["name"] = npc.name;
			qjo["desc"] = npc.description;
			qjo["category"] = npc.category;
			qjo["sheet"] = npc.sheet;
			qjo["cfgA"] = npctbl_saveConfig(npc.cfgA);
			qjo["cfgB"] = npctbl_saveConfig(npc.cfgB);
			savedMetadata[QString::number(idx)] = qjo;
		}
		idx++;
	}
	// done
	res["metadata"] = savedMetadata;
	return res;
}
