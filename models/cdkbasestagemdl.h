#ifndef CDKBASESTAGEMDL_H
#define CDKBASESTAGEMDL_H

#include <QList>
#include <QString>
#include <QJsonObject>
#include "cdkbaseglobalmdl.h"
#include "formats/cdfstage.h"

class CdkBaseStageMdl : public CdkBaseGlobalMdl {
	Q_OBJECT
	friend CdkGameMdl;
public:
	CdkBaseStageMdl(CdkGameMdl * mdl);
	virtual ~CdkBaseStageMdl();
	virtual bool supportsMNA2() = 0;
	// -- Load/Save (protected as CdkGameMdl owns stage management) --
protected:
	virtual QList<CdfStage> load(CdExceptionTracker & ex) = 0;
};

#endif // CDKBASESTAGEMDL_H
