#include "cdkbasemdl.h"
#include "cdkgamemdl.h"

static CdkGameMdl * cdkbaseglobalmdl_currentlyBeingInitialized;

CdkBaseMdl::CdkBaseMdl(CdkGameMdl * gameMdl) : boundMdl(gameMdl) {
	cdkbaseglobalmdl_currentlyBeingInitialized = gameMdl;
}

const QList<CdkBaseGlobalMdl *> & cdkbasemdl_findPotentialDependencies() {
	return cdkbaseglobalmdl_currentlyBeingInitialized->gModels;
}
