#ifndef CDCATEGORY_H
#define CDCATEGORY_H

#include <QStringList>

inline QStringList cdcategory_names() {
	QStringList list;
#define C_ALLC 0
	list.append("No Category");
#define C_UTIL 1
	list.append("Utility & Spawners");
#define C_SPWN 2
	list.append("Spawnable");
#define C_CHAR 3
	list.append("Characters");
#define C_ITEM 4
	list.append("Items & Containers");
#define C_FURN 5
	list.append("Furniture");
#define C_EVIL 6
	list.append("Enemies/Evil Furniture");
#define C_EBAT 7
	list.append("Enemies/Bats");
#define C_ECRT 8
	list.append("Enemies/Critters");
#define C_GYRD 9
	list.append("Enemies/Graveyard");
#define C_EGGS 10
	list.append("Enemies/Egg Corridor");
#define C_GTWN 11
	list.append("Enemies/Grasstown");
#define C_SZON 12
	list.append("Enemies/Sand Zone");
#define C_LABY 13
	list.append("Enemies/Labyrinth");
#define C_WWAY 14
	list.append("Enemies/Waterway");
#define C_OSDE 15
	list.append("Enemies/Outer Wall");
#define C_PLNT 16
	list.append("Enemies/Plantation");
#define C_LCAV 17
	list.append("Enemies/Last Cave");
#define C_HELL 18
	list.append("Enemies/Sacred Grounds");
#define C_INTN 19
	list.append("Internal");
#define C_INTB 20
	list.append("Internal/Boss Parts");
	return list;
}

#endif // CDCATEGORY_H
