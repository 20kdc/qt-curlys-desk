#include <QApplication>
#include <QTextStream>
#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>

#include "uis/cdicurlysdesk.h"
#include "cdexcept.h"

int main(int argc, char *argv[]) {
	QApplication a(argc, argv);
	QTextStream qts(stderr);

	QStringList qsl = a.arguments();
	QString gameBase = "";
	QString gameExecutable = "";
	if (qsl.length() > 1) {
		gameExecutable = qsl[1] + QString("Doukutsu.exe");
		gameBase = qsl[1];
	} else {
		gameExecutable = QFileDialog::getOpenFileName(nullptr, "Target", "", "Doukutsu.exe / doukutsu-rs.x86_64 / other executables (*.*)");
		gameBase = QFileInfo(gameExecutable).dir().absolutePath() + "/";
	}
	qts << "Game base: " << gameBase << "\n";
	qts << "Game executable: " << gameExecutable << "\n";
	CdExceptionTracker tracker("init");
	CdkGameMdl gameMdl(tracker, gameBase, gameExecutable);
	if (gameMdl.terminateTracker(tracker, nullptr))
		return 1;
	CdiCurlysDesk ccd(&gameMdl);
	gameMdl.theOneTrueRoot = &ccd;
	ccd.setVisible(true);
	return a.exec();
}
