#include "cdutils.h"

void CduBinaryR::handle(char * ba, size_t len, size_t fileLength) {
	if (len == fileLength) {
		base->read(ba, len);
		return;
	}
	size_t maxLength = len;
	if (maxLength < fileLength)
		maxLength = fileLength;
	char tmp[maxLength];
	memset(tmp, 0, maxLength);
	// do the thing
	base->read(tmp, fileLength);
	memcpy(ba, tmp, len); // copy out of buffer
}

void CduBinaryR::fixstr(QString & str, int sz) {
	str = cdutils_load_fixstr(codec, base->read(sz));
}

void CduBinaryW::handle(char * ba, size_t len, size_t fileLength) {
	if (len == fileLength) {
		base->write(ba, len);
		return;
	}
	size_t maxLength = len;
	if (maxLength < fileLength)
		maxLength = fileLength;
	char tmp[maxLength];
	memset(tmp, 0, maxLength);
	memcpy(tmp, ba, len); // copy into buffer
	// do the thing
	base->write(ba, fileLength);
}

void CduBinaryW::fixstr(QString & str, int sz) {
	cdutils_save_fixstr(base, codec, str, sz);
}


void cdutils_save_fixstr(QIODevice * file, QTextCodec * cod, const QString & text, int sz) {
	// These strings may be in Latin-1 or Japanese dependent on various things.
	// For NOW, the project will assume Latin-1 always... :(
	QByteArray qba = cod->fromUnicode(text);
	int l = qba.length();
	if (l >= sz)
		l = sz - 1;
	qba = qba.left(l);
	while (l < sz) {
		qba.append((char) 0);
		l++;
	}
	file->write(qba);
}

QString cdutils_load_fixstr(QTextCodec * cod, const QByteArray & data) {
	// Qt seems to be trying really hard to optimize Latin1 constants,
	//  but I'm not sure if it is or isn't introducing issues.
	// Let's just say it's not as unlikely as you'd think.
	int l = data.length();
	for (int i = 0; i < l; i++)
		if (data[i] == (char) 0)
			return cod->toUnicode(data.left(i));
	return cod->toUnicode(data);
}

void cdutils_save_u32(QIODevice * file, uint32_t num) {
	uint8_t buf[4];
	buf[0] = num & 0xFF; num >>= 8;
	buf[1] = num & 0xFF; num >>= 8;
	buf[2] = num & 0xFF; num >>= 8;
	buf[3] = num & 0xFF;
	file->write((const char *) buf, 4);
}

void cdutils_save_u16(QIODevice * file, uint16_t num) {
	uint8_t buf[2];
	buf[0] = num & 0xFF; num >>= 8;
	buf[1] = num & 0xFF;
	file->write((const char *) buf, 2);
}

void cdutils_save_u8(QIODevice * file, uint8_t num) {
	file->write((char *) &num, 1);
}

uint32_t cdutils_load_u32(const QByteArray & data) {
	uint32_t buf = 0;
	buf |= (uint8_t) data[3]; buf <<= 8;
	buf |= (uint8_t) data[2]; buf <<= 8;
	buf |= (uint8_t) data[1]; buf <<= 8;
	buf |= (uint8_t) data[0];
	return buf;
}

uint16_t cdutils_load_u16(const QByteArray & data) {
	uint16_t buf = 0;
	buf |= (uint8_t) data[1]; buf <<= 8;
	buf |= (uint8_t) data[0];
	return buf;
}
