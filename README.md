# qt-curlys-desk (likely to be unmaintained)

# THE QT COMPANY ARE SELFISH

https://www.phoronix.com/scan.php?page=news_item&px=Qt-5.15-LTS-Commercial-Phase

If you are of The Qt Company, congratulations.

Selfishness has removed one of the only viable options for cross-platform native application development.

Do you want everything to be a memory-hungry Electron hell or just plain Windows-only?

Because this is how you make those things happen.

I daren't think how this is going to effect downstream projects like Krita.

## Credits

qt-curlys-desk by 20kdc

UI design and the addresses used for EXE patching are borrowed from Cave Editor by Wistil.

QExe by Leo40Story ( https://github.com/Leo40Git/QExe )

Qt by (many, many people!)

## License

The qt-curlys-desk license is CC0 (see COPYING).

QExe is Leo40Story's.

